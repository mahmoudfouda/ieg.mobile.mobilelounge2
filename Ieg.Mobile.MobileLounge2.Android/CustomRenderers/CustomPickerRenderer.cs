﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.Graphics;
using Android.Graphics.Drawables;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Ieg.Mobile.MobileLounge2.Droid.CustomRenderers;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly: ExportRenderer(typeof(Picker), typeof(CustomPickerRenderer))]
namespace Ieg.Mobile.MobileLounge2.Droid.CustomRenderers
{
    public class CustomPickerRenderer : PickerRenderer
    {

        public CustomPickerRenderer() : base(MainActivity.CurrentContext)
        {

        }

        protected override void OnElementChanged(ElementChangedEventArgs<Picker> e)
        {
            base.OnElementChanged(e);

            var picker = e.NewElement;
            if (Control != null)
            {
                Control.Focusable = false;//TODO: workaround for loop Picker's open 

                // picker.TextColor = bp.MyTextColor;
                // picker.BackgroundColor = bp.MyBackgroundColor;
                Control.SetHintTextColor(Android.Graphics.Color.Black);
                Control.SetSingleLine(false);
                // Control.SetTypeface(null, TypefaceStyle.Bold);
                // Control.Gravity = GravityFlags.Center;

                // Remove borders
                GradientDrawable gd = new GradientDrawable();
                gd.SetStroke(0, Android.Graphics.Color.Transparent);
                Control.SetBackgroundDrawable(gd);
            }

            SetControlStyle();
        }

        protected override void OnElementPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            base.OnElementPropertyChanged(sender, e);
            SetControlStyle();
        }

        private void SetControlStyle()
        {
            if (Control != null)
            {
                Drawable imgDropDownArrow = Resources.GetDrawable("pickerarrow");
                Bitmap bitmap = ((BitmapDrawable)imgDropDownArrow).Bitmap;
                imgDropDownArrow = new BitmapDrawable(Resources, Bitmap.CreateScaledBitmap(bitmap, 24, 12, true));//30 18
                Control.SetCompoundDrawablesRelativeWithIntrinsicBounds(null, null, imgDropDownArrow, null);
                Control.SetCursorVisible(false);
            }
        }
    }
}