﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.Graphics.Drawables;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Ieg.Mobile.MobileLounge2.Core.Controls;
using Ieg.Mobile.MobileLounge2.Droid.CustomRenderers;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly: ExportRenderer(typeof(CustomEntry), typeof(CustomEntryRenderer))]
namespace Ieg.Mobile.MobileLounge2.Droid.CustomRenderers
{
    public class CustomEntryRenderer : EntryRenderer
    {
        private GradientDrawable shape;

        public CustomEntryRenderer(Context context) : base(context) { }

        protected override void OnElementChanged(ElementChangedEventArgs<Entry> e)
        {
            base.OnElementChanged(e);

            if (Control != null)
            {
                shape = new GradientDrawable();
                shape.SetColor(e.NewElement.BackgroundColor.ToAndroid());
                shape.SetCornerRadius(4);
                shape.SetStroke(1, Android.Graphics.Color.LightGray);
                Control.SetBackground(shape);
                Control.Gravity = GravityFlags.CenterVertical;
                Control.SetPadding(5, 0, 5, 0);
            }

            UpdateBorders();
        }

        protected override void OnElementPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            base.OnElementPropertyChanged(sender, e);

            if (Control == null) return;

            if (e.PropertyName == CustomEntry.IsBorderErrorVisibleProperty.PropertyName)
                UpdateBorders();
        }

        void UpdateBorders()
        {
            if (((CustomEntry)this.Element).IsBorderErrorVisible)
            {
                shape.SetStroke(3, ((CustomEntry)this.Element).BorderErrorColor.ToAndroid());
            }
            else
            {
                shape.SetStroke(1, Android.Graphics.Color.LightGray);
            }
        }
    }
}