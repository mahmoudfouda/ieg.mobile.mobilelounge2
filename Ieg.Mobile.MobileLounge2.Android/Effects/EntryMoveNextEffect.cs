﻿using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Text;
using Android.Views;
using Android.Widget;
using Ieg.Mobile.MobileLounge2.Droid.Effects;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;
using System.Linq;
using Ieg.Mobile.MobileLounge2.Core.Controls;

[assembly: ExportEffect(typeof(EntryMoveNextEffect), nameof(EntryMoveNextEffect))]
namespace Ieg.Mobile.MobileLounge2.Droid.Effects
{
    [Preserve]
    public class EntryMoveNextEffect : PlatformEffect
    {
        protected override void OnAttached()
        {
            // Check if the attached element is of the expected type and has the NextEntry
            // property set. if so, configure the keyboard to indicate there is another entry
            // in the form and the dismiss action to focus on the next entry
            if (base.Element is CustomEntry xfControl && xfControl.NextEntry != null)
            {
                var entry = (Android.Widget.EditText)Control;

                entry.ImeOptions = Android.Views.InputMethods.ImeAction.Next;
                entry.EditorAction += (sender, args) =>
                {
                    xfControl.OnNext();

                    ((IEntryController)Element).SendCompleted();
                };
            }
        }

        protected override void OnDetached()
        {
            // Intentionally empty
        }
    }
}