﻿using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Text;
using Android.Views;
using Android.Widget;
using Ieg.Mobile.MobileLounge2.Droid.Effects;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;
using System.Linq;

[assembly: ResolutionGroupName(nameof(Ieg.Mobile.MobileLounge2))]
[assembly: ExportEffect(typeof(CapitalizeKeyboardEffect), nameof(CapitalizeKeyboardEffect))]
namespace Ieg.Mobile.MobileLounge2.Droid.Effects
{
    [Preserve]
    public class CapitalizeKeyboardEffect : PlatformEffect
    {
        InputTypes old;
        IInputFilter[] oldFilters;

        protected override void OnAttached()
        {
            var editText = Control as EditText;
            if (editText != null)
            {
                old = editText.InputType;
                oldFilters = editText.GetFilters().ToArray();

                editText.SetRawInputType(InputTypes.ClassText | InputTypes.TextFlagCapCharacters);

                var newFilters = oldFilters.ToList();
                newFilters.Add(new InputFilterAllCaps());
                editText.SetFilters(newFilters.ToArray());
            }
        }

        protected override void OnDetached()
        {
            var editText = Control as EditText;
            if (editText != null)
            {
                editText.SetRawInputType(old);
                editText.SetFilters(oldFilters);
            }
        }
    }
}