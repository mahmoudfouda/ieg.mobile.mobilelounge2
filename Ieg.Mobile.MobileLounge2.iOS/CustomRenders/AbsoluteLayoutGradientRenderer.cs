﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CoreAnimation;
using CoreGraphics;
using Foundation;
using Ieg.Mobile.MobileLounge2.Core.Controls;
using Ieg.Mobile.MobileLounge2.iOS.CustomRenders;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer(typeof(AbsoluteLayoutGradient), typeof(AbsoluteLayoutGradientRenderer))]
namespace Ieg.Mobile.MobileLounge2.iOS.CustomRenders
{
    public class AbsoluteLayoutGradientRenderer : VisualElementRenderer<AbsoluteLayoutGradient>
    {
        public override void Draw(CGRect rect)
        {
            base.Draw(rect);
            AbsoluteLayoutGradient stack = (AbsoluteLayoutGradient)this.Element;
            CGColor startColor = stack.StartColor.ToCGColor();
            CGColor endColor = stack.EndColor.ToCGColor();
            #region for Vertical Gradient  
            var gradientLayer = new CAGradientLayer();     
            #endregion
            #region for Horizontal Gradient  
          /*  var gradientLayer = new CAGradientLayer()
            {
                StartPoint = new CGPoint(0, 0.5),
                EndPoint = new CGPoint(1, 0.5)
            };*/
            #endregion
            gradientLayer.Frame = rect;
            gradientLayer.Colors = new CGColor[] {
                startColor,
                endColor
            };
            NativeView.Layer.InsertSublayer(gradientLayer, 0);
        }
    }
}