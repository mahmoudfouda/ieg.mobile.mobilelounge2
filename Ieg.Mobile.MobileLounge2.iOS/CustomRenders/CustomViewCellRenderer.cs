﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Foundation;
using Ieg.Mobile.MobileLounge2.iOS.CustomRenders;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;
using Xamarin.Forms.PlatformConfiguration.iOSSpecific;

[assembly: ExportRenderer(typeof(ViewCell), typeof(CustomViewCellRenderer))]
namespace Ieg.Mobile.MobileLounge2.iOS.CustomRenders
{
    public class CustomViewCellRenderer : ViewCellRenderer
    {
        public override UITableViewCell GetCell(Xamarin.Forms.Cell item, UITableViewCell reusableCell, UITableView tv)
        {
            var cell = base.GetCell(item, reusableCell, tv);

            cell.SelectedBackgroundView = new UIView
            {
                BackgroundColor = Color.Transparent.ToUIColor(),
            };

            return cell;
        }
    }
}