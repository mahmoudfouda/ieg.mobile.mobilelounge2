﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CoreAnimation;
using Foundation;
using Ieg.Mobile.MobileLounge2.iOS.CustomRenders;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer(typeof(NavigationPage), typeof(TransitionNavigationPageRenderer))]
namespace Ieg.Mobile.MobileLounge2.iOS.CustomRenders
{
    public class TransitionNavigationPageRenderer : NavigationRenderer
    {
        public override void PushViewController(UIViewController viewController, bool animated)
        {
            var transition = CATransition.CreateAnimation();
            transition.Duration = 0.5f;
            transition.Type = CAAnimation.TransitionPush;
            transition.Subtype = CAAnimation.TransitionFromRight;

            View.Layer.AddAnimation(transition, null);

            base.PushViewController(viewController, true);
        }

        public override UIViewController PopViewController(bool animated)
        {
            var transition = CATransition.CreateAnimation();
            transition.Duration = 0.5f;
            transition.Type = CAAnimation.TransitionPush;
            transition.Subtype = CAAnimation.TransitionFromLeft;

            View.Layer.AddAnimation(transition, null);

            return base.PopViewController(true);
        }
    }
}