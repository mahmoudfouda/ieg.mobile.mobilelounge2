﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CoreGraphics;
using Foundation;
using Ieg.Mobile.MobileLounge2.Core.Controls;
using Ieg.Mobile.MobileLounge2.iOS.CustomRenders;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer(typeof(CustomEntry), typeof(CustomEntryRenderer))]
namespace Ieg.Mobile.MobileLounge2.iOS.CustomRenders
{
    public class CustomEntryRenderer : EntryRenderer
    {
        private CGColor defaultBorderColor;
        private nfloat defaultCornerDarius;
        private nfloat defaultBorderWidth;

        protected override void OnElementChanged(ElementChangedEventArgs<Entry> e)
        {
            base.OnElementChanged(e);

            if (this.Control != null)
            {
                defaultBorderColor = this.Control.Layer.BorderColor;
                defaultCornerDarius = this.Control.Layer.CornerRadius;
                defaultBorderWidth = this.Control.Layer.BorderWidth;
            }
        }


        protected override void OnElementPropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            base.OnElementPropertyChanged(sender, e);

            if (Control == null || this.Element == null) return;

            if (e.PropertyName == CustomEntry.IsBorderErrorVisibleProperty.PropertyName)
            {
                if (((CustomEntry)this.Element).IsBorderErrorVisible)
                {
                    this.Control.Layer.BorderColor = ((CustomEntry)this.Element).BorderErrorColor.ToCGColor();
                    this.Control.Layer.BorderWidth = new nfloat(0.8);
                    this.Control.Layer.CornerRadius = 5;
                }
                else
                {
                    this.Control.Layer.BorderColor = defaultBorderColor;
                    this.Control.Layer.CornerRadius = defaultCornerDarius;
                    this.Control.Layer.BorderWidth = defaultBorderWidth;
                }
            }
        }
    }
}