﻿//using System;
//using System.Collections.Generic;
//using System.ComponentModel;
//using System.Linq;
//using System.Text;

//using Foundation;
//using Ieg.Mobile.MobileLounge2.Core.Controls;
//using Ieg.Mobile.MobileLounge2.iOS.CustomRenders;
//using MapKit;
//using ObjCRuntime;
//using UIKit;
//using Xamarin.Forms;
//using Xamarin.Forms.Maps.iOS;
//using Xamarin.Forms.Platform.iOS;

//[assembly: ExportRenderer(typeof(CustomMap), typeof(CustomMapRenderer))]
//namespace Ieg.Mobile.MobileLounge2.iOS.CustomRenders
//{
//    public class CustomMapRenderer : MapRenderer
//    {
//      //  MKCircleRenderer circleRenderer;

//        protected override void OnElementChanged(ElementChangedEventArgs<View> e)
//        {
//            base.OnElementChanged(e);

//            if (e.OldElement != null)
//            {
//                var nativeMap = Control as MKMapView;
//                if (nativeMap != null)
//                {
//                    nativeMap.RemoveOverlays(nativeMap.Overlays);
//                    nativeMap.OverlayRenderer = null;
//                    //circleRenderer = null;
//                }
//            }

//            if (e.NewElement != null)
//            {
//                var formsMap = (CustomMap)e.NewElement;
//                var nativeMap = Control as MKMapView;

//                nativeMap.ZoomEnabled = true;

//                var circles = formsMap.Circle;

//                foreach (var circle in circles)
//                {
//                    nativeMap.OverlayRenderer = GetOverlayRenderer;

//                    var circleOverlay = MKCircle.Circle(new CoreLocation.CLLocationCoordinate2D(circle.Position.Latitude, circle.Position.Longitude), circle.Radius);
//                    nativeMap.AddOverlay(circleOverlay);
//                }
//            }
//        }

//        MKOverlayRenderer GetOverlayRenderer(MKMapView mapView, IMKOverlay overlayWrapper)
//        {
//           MKCircleRenderer circleRenderer = null;

//            if (!Equals(overlayWrapper, null))
//            {
//                var overlay = Runtime.GetNSObject(overlayWrapper.Handle) as IMKOverlay;
//                circleRenderer = new MKCircleRenderer(overlay as MKCircle)
//                {
//                    FillColor = Color.FromHex("#660099ee").ToUIColor(),
//                    StrokeColor = Color.FromHex("#FFFFFFFF").ToUIColor(),
//                    Alpha = 0.4f,
//                    LineWidth = 1,
//                };
//            }
//            return circleRenderer;
//        }
        
//    }
//}