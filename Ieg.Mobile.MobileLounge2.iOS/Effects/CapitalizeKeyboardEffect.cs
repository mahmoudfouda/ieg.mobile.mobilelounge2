﻿using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;
using UIKit;
using Foundation;
using Ieg.Mobile.MobileLounge2.iOS.Effects;

[assembly: ResolutionGroupName(nameof(Ieg.Mobile.MobileLounge2))]
[assembly: ExportEffect(typeof(CapitalizeKeyboardEffect), nameof(CapitalizeKeyboardEffect))]
namespace Ieg.Mobile.MobileLounge2.iOS.Effects
{
    [Preserve(AllMembers = true)]
    public class CapitalizeKeyboardEffect : PlatformEffect
    {
        UITextAutocapitalizationType old;

        protected override void OnAttached()
        {
            var editText = Control as UITextField;
            if (editText != null)
            {
                old = editText.AutocapitalizationType;
                editText.AutocapitalizationType = UITextAutocapitalizationType.AllCharacters;
            }
        }

        protected override void OnDetached()
        {
            var editText = Control as UITextField;
            if (editText != null)
                editText.AutocapitalizationType = old;
        }
    }
}