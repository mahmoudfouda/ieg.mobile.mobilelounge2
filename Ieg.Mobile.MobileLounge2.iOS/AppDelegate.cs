﻿using System;
using System.Collections.Generic;
using System.Linq;

using Foundation;
using Ieg.Mobile.MobileLounge2.Core;
using Ieg.Mobile.MobileLounge2.Core.Behavior;
using UIKit;
using Xamarin.Forms.RadialMenu.iOSCore;

namespace Ieg.Mobile.MobileLounge2.iOS
{
    // The UIApplicationDelegate for the application. This class is responsible for launching the 
    // User Interface of the application, as well as listening (and optionally responding) to 
    // application events from iOS.
    [Register("AppDelegate")]
    public partial class AppDelegate : global::Xamarin.Forms.Platform.iOS.FormsApplicationDelegate
    {
        //
        // This method is invoked when the application has loaded and is ready to run. In this 
        // method you should instantiate the window, load the UI into it and then make the window
        // visible.
        //
        // You have 17 seconds to return from this method, or iOS will terminate your application.
        //
        public override bool FinishedLaunching(UIApplication app, NSDictionary options)
        {
            //Xamarin.Forms.Forms.SetFlags("CollectionView_Experimental");

            global::Xamarin.Forms.Forms.Init();

            FFImageLoading.Forms.Platform.CachedImageRenderer.Init();

            new Ieg.Mobile.Common.iOS.Controls.CustomWebViewRenderer();

            Abstractions.Init();

            Effects.Effects.Init();

            new EmptyEntryValidatorBehavior();

            //global::Xamarin.FormsMaps.Init();

            Xamarin.FormsGoogleMaps.Init("AIzaSyB1SrsCeYd-6jB8lBTq2WmacLXLltINOiM");

            LoadApplication(new App());

            return base.FinishedLaunching(app, options);
        }
    }
}
