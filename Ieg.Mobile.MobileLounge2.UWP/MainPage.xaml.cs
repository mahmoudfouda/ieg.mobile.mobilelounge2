﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Services.Maps;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

namespace Ieg.Mobile.MobileLounge2.UWP
{
    public sealed partial class MainPage
    {
        public MainPage()
        {
            this.InitializeComponent();

            MapService.ServiceToken = "Ap2EVA65RUi59GFyS43ufjek5WUE242Sw7OSwuMbJffaNgEhkOa3dMf54D073tNo";

            LoadApplication(new Ieg.Mobile.MobileLounge2.Core.App());
        }
    }
}
