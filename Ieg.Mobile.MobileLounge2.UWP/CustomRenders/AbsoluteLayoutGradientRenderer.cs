﻿using Ieg.Mobile.MobileLounge2.Core.Controls;
using Ieg.Mobile.MobileLounge2.UWP.CustomRenders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Foundation;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Media;
using Xamarin.Forms.Platform.UWP;

[assembly: ExportRenderer(typeof(AbsoluteLayoutGradient), typeof(AbsoluteLayoutGradientRenderer))]
namespace Ieg.Mobile.MobileLounge2.UWP.CustomRenders
{
    public class AbsoluteLayoutGradientRenderer : VisualElementRenderer<AbsoluteLayoutGradient, Panel>
    {
        private Xamarin.Forms.Color[] Colors { get; set; }

        protected override void OnElementChanged(ElementChangedEventArgs<AbsoluteLayoutGradient> e)
        {
            base.OnElementChanged(e);

            if (e.OldElement != null || Element == null)
                return;

            try
            {
                var stack = e.NewElement as AbsoluteLayoutGradient;

                Colors = new Xamarin.Forms.Color[] { stack.StartColor, Xamarin.Forms.Color.FromHex("#56afe2"),  Xamarin.Forms.Color.FromHex("#81c3e9"), Xamarin.Forms.Color.FromHex("#abd7f0"),  stack.EndColor };

                UpdateBackgroundColor();
            }
            catch { }
        }

        protected override void UpdateBackgroundColor()
        {
            base.UpdateBackgroundColor();

            LinearGradientBrush gradient;

            GradientStopCollection stopCollection = new GradientStopCollection();

            for (int i = 0, l = Colors.Length; i < l; i++)
            {
                stopCollection.Add(new GradientStop
                {
                    Color =  Windows.UI.Color.FromArgb((byte)(Colors[i].A * byte.MaxValue), (byte)(Colors[i].R * byte.MaxValue), (byte)(Colors[i].G * byte.MaxValue), (byte)(Colors[i].B * byte.MaxValue)),
                    Offset = (double)i / Colors.Length
                });
            }

            gradient = new LinearGradientBrush
            {
                GradientStops = stopCollection,
                StartPoint = new Point(0.5, 0),
                EndPoint = new Point(0.5, 1)
            };

            Background = gradient;
        }
    }
}
