﻿using Ieg.Mobile.MobileLounge2.Core.Controls;
using Ieg.Mobile.MobileLounge2.UWP.CustomRenders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Platform.UWP;

[assembly: ExportRenderer(typeof(CustomEntry), typeof(CustomEntryRenderer))]
namespace Ieg.Mobile.MobileLounge2.UWP.CustomRenders
{
    public class CustomEntryRenderer : EntryRenderer
    {
        protected override void OnElementChanged(ElementChangedEventArgs<Entry> e)
        {
            base.OnElementChanged(e);

            if (this.Control != null)
            {
                Control.Style = (Windows.UI.Xaml.Style)App.Current.Resources["CustomTextBoxStyle"];
            }
        }
    }
}