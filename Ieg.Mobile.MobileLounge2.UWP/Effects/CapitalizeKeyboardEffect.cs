﻿using Ieg.Mobile.MobileLounge2.UWP.Effects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml.Controls;
using Xamarin.Forms;
using Xamarin.Forms.Internals;
using Xamarin.Forms.Platform.UWP;

[assembly: ResolutionGroupName(nameof(Ieg.Mobile.MobileLounge2))]
[assembly: ExportEffect(typeof(CapitalizeKeyboardEffect), nameof(CapitalizeKeyboardEffect))]
namespace Ieg.Mobile.MobileLounge2.UWP.Effects
{
    [Preserve]
    public class CapitalizeKeyboardEffect : PlatformEffect
    {
        protected override void OnAttached()
        {
            var textbox = Control as TextBox;

            if (textbox == null)
                return;


            textbox.TextChanging -= TextboxOnTextChanging;
            textbox.TextChanging += TextboxOnTextChanging;
        }

        private static void TextboxOnTextChanging(TextBox sender, TextBoxTextChangingEventArgs args)
        {
            var selectionStart = sender.SelectionStart;
            sender.Text = sender.Text.ToUpper();
            sender.SelectionStart = selectionStart;
            sender.SelectionLength = 0;
        }

        protected override void OnDetached()
        {
            var textbox = Control as TextBox;
            if (textbox != null)
            {
                textbox.TextChanging -= TextboxOnTextChanging;
            }
        }
    }
}