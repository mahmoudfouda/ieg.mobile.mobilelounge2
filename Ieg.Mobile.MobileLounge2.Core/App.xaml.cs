﻿using AIMS;
using AIMS.Models;
using Ieg.Mobile.DataContracts.MobileLounge.ServiceModel;
using Ieg.Mobile.Localization;
using Ieg.Mobile.MobileLounge2.Core.Common;
using Ieg.Mobile.MobileLounge2.Core.Library;
using Ieg.Mobile.MobileLounge2.Core.Views;
using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

[assembly: XamlCompilation(XamlCompilationOptions.Compile)]
namespace Ieg.Mobile.MobileLounge2.Core
{
    public partial class App : Application
    {
        #region Static Properties
        public static MobileDevice MobileDevice { get { return AIMSApplication?.CurrentDevice; } }

        public static DefaultMobileApplication AIMSApplication { get; } = DefaultMobileApplication.Create();

        public static LanguageRepository TextProvider
        {
            get { return AIMSApplication.TextProvider; }
        }

        public static bool IsDeveloperModeEnabled { get; internal set; }
        public static bool DevMode { get; internal set; }
        #endregion

        #region Events
        public event VoidEventHandler OnLocationDetected;
        #endregion

        public App()
        {
            InitializeComponent();

            MainPage = new NavigationPage(new LoginPage());
        }

        protected override void OnStart()
        {
            var gpsAdapter = LocationService.Current;
            if (gpsAdapter != null)
            {
                gpsAdapter.GpsAdapter_OnLocationChanged += (sender, position) =>
                {
                    if(position != null && OnLocationDetected != null)
                        OnLocationDetected.Invoke();
                };
                gpsAdapter.OnLog += (sender, exception) =>
                {
                    //if(exception.InnerException != null)
                    //    AIMS.LogsRepository.AddInfo(exception.Message, exception.InnerException.Message);
                    //else AIMS.LogsRepository.AddInfo(exception.Message);
#if DEBUG
                    Console.WriteLine($"\n{exception.Message}");
                    if(exception.InnerException != null)
                        Console.WriteLine($"More Details:{exception.InnerException?.Message}\n\n");
#endif
                };
            }

#if DEBUG
            //string serialNumber = "d79d0451";
            string serialNumber = "abd5465d4b";//Uncomment this for the developer serial number
                                               //string serialNumber = "FakeDeviceUIDFakeDeviceUIDFakeDeviceUIDFakeDeviceUIDFakeDeviceUID";//Uncomment this for the registeration test
                                               //string serialNumber = "LGH812a752ba55";//Uncomment this for Olivier's serial number
#else
            string serialNumber = Plugin.DeviceInfo.CrossDeviceInfo.Current.Id;
#endif

            App.AIMSApplication.TriggerApplicationInit(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), gpsAdapter, new AIMS.Models.MobileDevice()
            {
                DeviceModel = Plugin.DeviceInfo.CrossDeviceInfo.Current.Model,
                DeviceName = Plugin.DeviceInfo.CrossDeviceInfo.Current.DeviceName,
                DeviceUniqueId = serialNumber,
                DeviceVersion = Plugin.DeviceInfo.CrossDeviceInfo.Current.Version,
                Type = Device.Idiom == TargetIdiom.Tablet || Device.Idiom == TargetIdiom.Desktop ? AIMS.DeviceType.Tablet : AIMS.DeviceType.Phone
            });

            LogsRepository.AddClientInfo("Application started");
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }
    }
}
