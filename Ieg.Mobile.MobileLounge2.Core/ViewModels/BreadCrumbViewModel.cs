﻿using Ieg.Mobile.Common.Controls;
using Ieg.Mobile.MobileLounge2.Core.Common;
using Ieg.Mobile.MobileLounge2.Core.Common.BindableModels;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;

namespace Ieg.Mobile.MobileLounge2.Core.ViewModels
{
    public class BreadCrumbViewModel : ViewModelBase
    {
        public BreadCrumbViewModel():base()
        {
            this.Navigation.OnHistoryUpdated += Navigation_OnHistoryUpdated;
        }

        private static ObservableCollection<PageBindable> _pages;
        public ObservableCollection<PageBindable> Pages
        {
            get
            {
                if (_pages == null)
                    _pages = new ObservableCollection<PageBindable>();

                return _pages;
            }
        }

        protected void Navigation_OnHistoryUpdated(object sender, System.Collections.Generic.IReadOnlyList<CustomContentView> e)
        {

            //Navigation.OnHistoryUpdated -= Navigation_OnHistoryUpdated;
            Pages.Clear();

            //if (MembershipProvider.Current != null && MembershipProvider.Current.SelectedLounge != null)
            //{
            //    if (Navigation.History.Count > 0)
            //        Pages.Add(new PageBindable() { Position = 0, Name = MembershipProvider.Current.SelectedLounge.LoungeName, BreadCrumberType = Navigation.History.Count > 1 ? BreadCrumberTypes.Common : BreadCrumberTypes.BeforeLast });
            //    else
            //        Pages.Add(new PageBindable() { Position = 0, Name = MembershipProvider.Current.SelectedLounge.LoungeName, BreadCrumberType = BreadCrumberTypes.Last });
            //}

            if (Navigation.History.Count > 0)
            {
                for (int i = 0; i < Navigation.History.Count; i++)
                {
                    if (i + 1 == Navigation.History.Count)
                        Pages.Add(new PageBindable() { Position = i + 1, Name = Navigation.History[i].Title, BreadCrumberType = BreadCrumberTypes.Last });
                    else
                    {
                        if (Navigation.History.Count - (i + 1) == 1)
                            Pages.Add(new PageBindable() { Position = i + 1, Name = Navigation.History[i].Title, BreadCrumberType = BreadCrumberTypes.BeforeLast });
                        else
                            Pages.Add(new PageBindable() { Position = i + 1, Name = Navigation.History[i].Title, BreadCrumberType = BreadCrumberTypes.Common });
                    }
                }
            }


            //Pages.Last().IsLastPage = true;

            //NotifyPropertyChange("Pages");
        }

        public override void Dispose()
        {
            base.Dispose();
            this.Navigation.OnHistoryUpdated -= Navigation_OnHistoryUpdated;
        }
    }
}
