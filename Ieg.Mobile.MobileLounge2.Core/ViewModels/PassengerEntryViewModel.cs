﻿using AIMS;
using AIMS.Models;
using Ieg.Mobile.MobileLounge2.Core.Common.BindableModels;
using Ieg.Mobile.MobileLounge2.Core.Library;
using Ieg.Mobile.MobileLounge2.Core.Views.ContentView;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace Ieg.Mobile.MobileLounge2.Core.ViewModels
{
    public class PassengerEntryViewModel : ViewModelBase
    {
        #region Fields and Properties
        private CancellationTokenSource throttleCts = new CancellationTokenSource();

        private PassengerBindable _selectedPassenger;

        private PassengersRepository _passengersRepository = new PassengersRepository();

        private ImageRepository _imageRepository = new ImageRepository();

        private ObservableCollection<AirportBindable> _airports = new ObservableCollection<AirportBindable>();

        public string ActualDate
        {
            get
            {
                return DateTime.Now.ToString("MMMM dd, yyyy");
            }
        }

        public PassengerBindable SelectedPassenger
        {
            get { return _selectedPassenger; }
            set
            {
                _selectedPassenger = value;
                NotifyPropertyChange("SelectedPassenger");
            }
        }

        public Passenger SelectedPassengerAIMS
        {
            get
            {
                if (MembershipProvider.Current != null)
                {
                    if (DataService.Current.IsGuestMode)
                        return MembershipProvider.Current.SelectedGuest;
                    else return MembershipProvider.Current.SelectedPassenger;
                }
                return null;
            }
            set
            {
                if (MembershipProvider.Current != null)
                {
                    if (DataService.Current.IsGuestMode)
                        MembershipProvider.Current.SelectedGuest = value;
                    else MembershipProvider.Current.SelectedPassenger = value;
                }
            }
        }

        public ObservableCollection<AirportBindable> Airports
        {
            get { return _airports; }
            set
            {
                _airports = value;
                NotifyPropertyChange("Airports");
            }
        }

        private bool _isListVisible;
        public bool IsListVisible
        {
            get { return _isListVisible; }
            set
            {
                _isListVisible = value;
                NotifyPropertyChange("IsListVisible");
            }
        }

        public string Title
        {
            get
            {
                if (MembershipProvider.Current != null && MembershipProvider.Current.SelectedCard != null)
                    return MembershipProvider.Current.SelectedCard.Name;
                else
                    return "Cards";
            }
        }

        private bool _fullNameIsNotValid;
        public bool FullNameIsNotValid
        {
            get { return _fullNameIsNotValid; }
            set
            {
                _fullNameIsNotValid = value;
                NotifyPropertyChange("FullNameIsNotValid");
            }
        }

        private bool _ffnIsNotValid;
        public bool FFNIsNotValid
        {
            get { return _ffnIsNotValid; }
            set
            {
                _ffnIsNotValid = value;
                NotifyPropertyChange("FFNIsNotValid");
            }
        }

        private bool _flightCarrierIsNotValid;
        public bool FlightCarrierIsNotValid
        {
            get { return _flightCarrierIsNotValid; }
            set
            {
                _flightCarrierIsNotValid = value;
                NotifyPropertyChange("FlightCarrierIsNotValid");
            }
        }

        private bool _flightNumberIsNotValid;
        public bool FlightNumberIsNotValid
        {
            get { return _flightNumberIsNotValid; }
            set
            {
                _flightNumberIsNotValid = value;
                NotifyPropertyChange("FlightNumberIsNotValid");
            }
        }

        private bool _trackingClassOfServiceIsNotValid;
        public bool TrackingClassOfServiceIsNotValid
        {
            get { return _trackingClassOfServiceIsNotValid; }
            set
            {
                _trackingClassOfServiceIsNotValid = value;
                NotifyPropertyChange("TrackingClassOfServiceIsNotValid");
            }
        }

        private bool _notesIsNotValid;
        public bool NotesIsNotValid
        {
            get { return _notesIsNotValid; }
            set
            {
                _notesIsNotValid = value;
                NotifyPropertyChange("NotesIsNotValid");
            }
        }

        private bool _pnrIsNotValid;
        public bool PNRIsNotValid
        {
            get { return _pnrIsNotValid; }
            set
            {
                _pnrIsNotValid = value;
                NotifyPropertyChange("PNRIsNotValid");
            }
        }

        private bool _fromIsNotValid;
        public bool FromIsNotValid
        {
            get { return _fromIsNotValid; }
            set
            {
                _fromIsNotValid = value;
                NotifyPropertyChange("FromIsNotValid");
            }
        }

        private bool _toIsNotValid;
        public bool ToIsNotValid
        {
            get { return _toIsNotValid; }
            set
            {
                _toIsNotValid = value;
                NotifyPropertyChange("ToIsNotValid");
            }
        }

        private bool _fullNameRequired;
        public bool FullNameRequired
        {
            get { return _fullNameRequired; }
            set
            {
                _fullNameRequired = value;
                NotifyPropertyChange("FullNameRequired");
            }
        }

        private bool _ffnRequired;
        public bool FFNRequired
        {
            get { return _ffnRequired; }
            set
            {
                _ffnRequired = value;
                NotifyPropertyChange("FFNRequired");
            }
        }

        private bool _flightCarrierRequired;
        public bool FlightCarrierRequired
        {
            get { return _flightCarrierRequired; }
            set
            {
                _flightCarrierRequired = value;
                NotifyPropertyChange("FlightCarrierRequired");
            }
        }

        private bool _flightNumberRequired;
        public bool FlightNumberRequired
        {
            get { return _flightNumberRequired; }
            set
            {
                _flightNumberRequired = value;
                NotifyPropertyChange("FlightNumberRequired");
            }
        }

        private bool _trackingClassOfServiceRequired;
        public bool TrackingClassOfServiceRequired
        {
            get { return _trackingClassOfServiceRequired; }
            set
            {
                _trackingClassOfServiceRequired = value;
                NotifyPropertyChange("TrackingClassOfServiceRequired");
            }
        }

        private bool _notesRequired;
        public bool NotesRequired
        {
            get { return _notesRequired; }
            set
            {
                _notesRequired = value;
                NotifyPropertyChange("NotesRequired");
            }
        }

        private bool _pnrRequired;
        public bool PNRRequired
        {
            get { return _pnrRequired; }
            set
            {
                _pnrRequired = value;
                NotifyPropertyChange("PNRRequired");
            }
        }

        private bool _fromRequired;
        public bool FromRequired
        {
            get { return _fromRequired; }
            set
            {
                _fromRequired = value;
                NotifyPropertyChange("FromRequired");
            }
        }

        private bool _toRequired;
        public bool ToRequired
        {
            get { return _toRequired; }
            set
            {
                _toRequired = value;
                NotifyPropertyChange("ToRequired");
            }
        }

        public Color BackGroundColor
        {
            get
            {
                return DataService.Current.IsGuestMode ? Color.FromHex("#9370db") : Color.FromHex("#0e79d8");
            }
        }



        #endregion

        public ICommand SearchAirportCommand { get; private set; }

        public ICommand ConfirmCommand { get; private set; }

        public PassengerEntryViewModel()
        {
            ConfirmCommand = new Command(() =>
            {
                if (MembershipProvider.Current == null) return;//in case of session timeout (which logs out the user)

                StartProcess(ConfirmAction);

            });

            _selectedPassenger = new PassengerBindable(SelectedPassengerAIMS);

            LoadPassenger();
        }

        private void ConfirmAction()
        {
            SelectedPassengerAIMS = SelectedPassenger.Passenger;

            if (DataService.Current.IsGuestMode)
            {
                _passengersRepository.OnPassengerChecked += OnGuestChecked;
                ValidateGuestInfo(SelectedPassenger.Passenger, (PassengerServiceType)MembershipProvider.Current.SelectedPassengerService.Id);
            }
            else
            {
                _passengersRepository.OnPassengerChecked += OnPassengerChecked;
                ValidatePassengerInfo();
            }
        }

        private void OnPassengerChecked(bool isSucceeded, int? errorCode, string errorMetadata, string message, Passenger passenger)
        {
            _passengersRepository.OnPassengerChecked -= OnPassengerChecked;

            if (MembershipProvider.Current == null)
            {
                //AppDelegate.ReturnToLoginView();
                return;
            }

            Device.BeginInvokeOnMainThread(() =>
            {
                EndProcess();

                passenger.ToAirport = SelectedPassenger.AirportTo;
                SelectedPassengerAIMS = passenger;
                if (isSucceeded)
                {
                    if (errorCode.HasValue && (ErrorCode)errorCode.Value == ErrorCode.PassengerNameMismatch)
                    {
                        errorCode = (int)ErrorCode.PassengerNameMismatch;

                        #region Passenger name mismatch (agent decision)

                        MessageButtonCancel = App.TextProvider.GetText(2541);
                        MessageButtonOk = App.TextProvider.GetText(2542);
                        MessageTitle = App.TextProvider.GetText(1104);
                        MessageQuestion = string.Format("{0}{1}", (errorCode.HasValue ? string.Format("{0}\n", App.TextProvider.GetText((int)errorCode)) : ""), message);
                        ShowQuestion = true;

                        PopupCancelQuestionCommand = new Command(() =>
                        {
                            //Focus on the name text box
                            // InvokeOnMainThread(() =>
                            // {
                            //     txtFullNameSPMI2.BecomeFirstResponder();
                            // });

                            HideAllPopup();
                        });

                        ConfirmQuestionPopupCommand = new Command(() =>
                        {
                            if (passenger != null)
                            {
                                PassengersRepository.AddFailedPassenger(passenger, message);
                            }

                            Device.BeginInvokeOnMainThread(() =>
                            {
                                HideAllPopup();
                                RejectPassenger();
                            });
                        });

                        NotifyPropertyChange("PopupCancelQuestionCommand");
                        NotifyPropertyChange("ConfirmQuestionPopupCommand");

                        Navigation.RootMasterDetailPage.ShowOverlayPage();
                        #endregion
                    }
                    else
                    {
                        #region Passenger check success
                        Navigation.Push(new ValidationResultContentView());
                        #endregion
                    }
                }
                else
                {
                    DataService.Current.ValidationResult = string.IsNullOrWhiteSpace(errorMetadata) ? new PassengerValidation() : new PassengerValidation(errorMetadata);
                    if (passenger != null)
                    {
                        PassengersRepository.AddFailedPassenger(passenger, message);
                        //LoadPassenger();
                    }

                    // TODO: Show failed and after 3 seconds return back to this screen (manual input)
                    //if (!(errorCode.HasValue && errorCode.Value == (int)ErrorCode.NeedToSwipeBoardingPass))//only in case of Incomplete Info we don't close the form
                    //    AppDelegate.ManualPassengerView.Close();//Closing the MI form in case of passenger validation failed (isSucceeded = false)
                    if (errorCode.HasValue && errorCode.Value == (int)ErrorCode.NeedToSwipeBoardingPass)//only in case of Incomplete Info we don't close the form
                    {
                        SelectedPassenger.FlightCarrier = passenger.FlightCarrier;
                        SelectedPassenger.FlightNumber = passenger.FlightNumber;
                        SelectedPassenger.TrackingClassOfService = passenger.TrackingClassOfService;
                        SelectedPassenger.PNR = passenger.PNR;
                        SelectedPassenger.FullName = passenger.FullName;
                        SelectedPassenger.FFN = passenger.FFN;

                        RefreshPassenger();
                    }
                    else
                    {
                        //   Navigation.Pop();//Closing the MI form in case of passenger validation failed (isSucceeded = false)
                        ShouldPopPage = true;
                    }

                    MessageTitle = App.TextProvider.GetText(1104);
                    MessageError = message;
                    ShowMessageError = true;

                    Navigation.RootMasterDetailPage.ShowOverlayPage();
                }
            });
        }

        public override void HideAllPopup()
        {
            base.HideAllPopup();

            if (ShouldPopPage)
                Navigation.Pop();

            ShouldPopPage = false;
        }

        private void OnGuestChecked(bool isSucceeded, int? errorCode, string errorMetadata, string message, Passenger passenger)
        {
            _passengersRepository.OnPassengerChecked -= OnGuestChecked;

            //InvokeOnMainThread(() => {
            if (MembershipProvider.Current == null)
            {
                //        AppDelegate.ReturnToLoginView();
                return;
            }

            //if (IsGuestMode)
            //     AppDelegate.GuestServicesView.OnGuestChecked(isSucceeded, errorCode, errorMetadata, message, passenger);

            //});
        }

        private void ValidateGuestInfo(Passenger guestPassengerInfo, PassengerServiceType guestTypeId)
        {
            if (guestPassengerInfo == null) return;
            try
            {
                _passengersRepository.ValidateGuestInfo(MembershipProvider.Current.SelectedPassenger, guestPassengerInfo, guestTypeId, MembershipProvider.Current.SelectedLounge);
            }
            catch (Exception ex)
            {
                Device.BeginInvokeOnMainThread(() =>
                {
                    MessageTitle = App.TextProvider.GetText(1104);

                    MessageError = App.TextProvider.GetText(1051);

                    LogsRepository.AddError("Guest Validation call error", ex);

                    EndProcess();

                    ShowMessageError = true;

                    Navigation.RootMasterDetailPage.ShowOverlayPage();
                });
            }
        }

        private void ValidatePassengerInfo(string barcode = "")
        {
            try
            {

                if (string.IsNullOrEmpty(barcode))
                    _passengersRepository.ValidatePassengerInfo(MembershipProvider.Current.SelectedLounge);
                else _passengersRepository.GetPassengerInfo(barcode, MembershipProvider.Current.SelectedLounge);
            }
            catch (Exception ex)
            {
                Device.BeginInvokeOnMainThread(() =>
                {
                    MessageTitle = App.TextProvider.GetText(1104);

                    MessageError = App.TextProvider.GetText(1051);

                    LogsRepository.AddError("Passenger Validation call error", ex);

                    EndProcess();

                    ShowMessageError = true;

                    Navigation.RootMasterDetailPage.ShowOverlayPage();
                });

            }

        }

        private void LoadImages()
        {
            #region Filling Images
            if (MembershipProvider.Current.SelectedAirline != null)
            {
                #region Filling the Airline
                if (MembershipProvider.Current.SelectedAirline.AirlineLogoBytes != null && MembershipProvider.Current.SelectedAirline.AirlineLogoBytes.Length > 0)
                {
                    SelectedPassenger.ImageAirline = ImageSource.FromStream(() => new MemoryStream(MembershipProvider.Current.SelectedAirline.AirlineLogoBytes));
                }
                else
                {
                    _imageRepository.LoadImage(MembershipProvider.Current.SelectedAirline.ImageHandle, (imageResult) =>
                    {
                        if (imageResult == null)
                        {
                            LogsRepository.AddError("Error in ImageAdapter.LoadImage()", "The result is null");
                            return;
                        }

                        if (imageResult.IsSucceeded)
                        {
                            var bytes = imageResult.ReturnParam;

                            if (bytes == null || bytes.Length == 0)
                                return;

                            Device.BeginInvokeOnMainThread(() =>
                            {
                                try
                                {
                                    SelectedPassenger.ImageAirline = ImageSource.FromStream(() => new MemoryStream(bytes));
                                }
                                catch (Exception eex)
                                {
                                    var m = eex.Message; //TODO: log
                                }
                            });
                        }

                    });
                }
                #endregion
            }

            if (MembershipProvider.Current.SelectedCard != null)
            {
                #region Filling the Card
                if (MembershipProvider.Current.SelectedCard.CardPictureBytes != null && MembershipProvider.Current.SelectedCard.CardPictureBytes.Length > 0)
                {
                    SelectedPassenger.ImageCard = ImageSource.FromStream(() => new MemoryStream(MembershipProvider.Current.SelectedCard.CardPictureBytes));
                }
                else
                {
                    _imageRepository.LoadImage(MembershipProvider.Current.SelectedCard.ImageHandle, (imageResult) =>
                    {
                        if (imageResult == null)
                        {
                            LogsRepository.AddError("Error in ImageAdapter.LoadImage()", "The result is null");
                            return;
                        }

                        if (imageResult.IsSucceeded)
                        {
                            var bytes = imageResult.ReturnParam;

                            if (bytes == null || bytes.Length == 0)
                                return;

                            Device.BeginInvokeOnMainThread(() =>
                            {
                                try
                                {
                                    SelectedPassenger.ImageCard = ImageSource.FromStream(() => new MemoryStream(bytes));
                                }
                                catch (Exception eex)
                                {
                                    var m = eex.Message; //TODO: log
                                }
                            });
                        }

                    });
                    #endregion
                }
            }
            #endregion
        }

        public void SearchAirports(string keyword)
        {
            Interlocked.Exchange(ref throttleCts, new CancellationTokenSource()).Cancel();

            Task.Delay(TimeSpan.FromMilliseconds(1000), this.throttleCts.Token)
                .ContinueWith(delegate
                {
                    this.PerformSearch(keyword);
                },
                CancellationToken.None, TaskContinuationOptions.OnlyOnRanToCompletion,
                TaskScheduler.FromCurrentSynchronizationContext());
        }

        private void PerformSearch(string keyword)
        {
            Device.BeginInvokeOnMainThread(() =>
            {
                IsListVisible = false;
            });

            using (var ar = new AirportsRepository())
            {
                ar.OnRepositoryChanged += (data) =>
                {
                    Device.BeginInvokeOnMainThread(() =>
                    {
                        Airports.Clear();
                        var items = (List<Airport>)data;
                        if (items != null && items.Count > 0)
                        {
                            foreach (var aipr in items)
                            {
                                Airports.Add(new AirportBindable(aipr));
                            }

                            IsListVisible = true;
                        }
                        else
                            IsListVisible = false;
                    });
                };
                ar.SearchAirports(keyword, true);
            }
        }

        private void RetrieveMainPassengerOf(bool isSucceeded, int? errorCode, string errorMetadata, string message, Passenger passenger)
        {
            _passengersRepository.OnPassengerChecked -= RetrieveMainPassengerOf;
            MembershipProvider.Current.SelectedPassenger = passenger;

            //SetContents();
        }

        private void RetrieveGuestPassenger(bool isSucceeded, int? errorCode, string errorMetadata, string message, Passenger passenger)
        {
            _passengersRepository.OnPassengerChecked -= RetrieveGuestPassenger;

            MembershipProvider.Current.SelectedPassenger = passenger;

            //SetContents();
        }

        private void LoadPassenger()
        {
            if (SelectedPassengerAIMS == null) return;

            LoadImages();

            var names = new List<string>();
            if (DataService.Current.ValidationResult.PassengerNames.Count > 0)
            {
                names = DataService.Current.ValidationResult.PassengerNames;
            }
            else if (!string.IsNullOrEmpty(SelectedPassenger.FullName))
                names.Add(SelectedPassenger.FullName);

            if (names.Count > 0)
                SelectedPassenger.FullName = names.Last();

            FullNameRequired =  FullNameIsNotValid = (DataService.Current.ValidationResult.IsNameRequired || 
                                                      DataService.Current.ValidationResult.PassengerNames.Count > 0);
            FFNRequired = FFNIsNotValid = DataService.Current.ValidationResult.IsFFNRequired;
            FlightCarrierRequired =  FlightCarrierIsNotValid = DataService.Current.ValidationResult.IsFlightInfoRequired;
            FlightNumberRequired = FlightNumberIsNotValid = DataService.Current.ValidationResult.IsFlightInfoRequired;
            TrackingClassOfServiceRequired = TrackingClassOfServiceIsNotValid = DataService.Current.ValidationResult.IsFlightInfoRequired;
            NotesRequired = NotesIsNotValid = DataService.Current.ValidationResult.IsNotesRequired;
            PNRRequired = PNRIsNotValid = DataService.Current.ValidationResult.IsPNRRequired;
            FromRequired = FromIsNotValid = DataService.Current.ValidationResult.IsDestinationOriginRequired;
            ToRequired = ToIsNotValid = DataService.Current.ValidationResult.IsDestinationOriginRequired;

            #region TODO: show Passenger Flight Carrier
            if (string.IsNullOrWhiteSpace(SelectedPassenger.FlightCarrier))
            {
                if (MembershipProvider.Current.SelectedAirline != null)//TODO: (Siavash )????
                {
                    SelectedPassenger.FlightCarrier = MembershipProvider.Current.SelectedAirline.Code;
                }
            }
            #endregion

            SelectedPassenger.FlightNumber = string.IsNullOrWhiteSpace(SelectedPassenger.FlightNumber) ? string.Empty : SelectedPassenger.FlightNumber.TrimStart('0');
            SelectedPassenger.AirportFrom = MembershipProvider.Current.SelectedLounge.AirportCode;
            LoadFullAirportName(MembershipProvider.Current.SelectedLounge.AirportCode, true);

        }

        public void LoadFullAirportName(string IATA_Code, bool isFrom)
        {

            using (var ar = new AirportsRepository())
            {
                ar.OnRepositoryChanged += (data) =>
                {
                    var airport = (Airport)data;
                    if (airport != null)
                        Device.BeginInvokeOnMainThread(() =>
                        {
                            if (isFrom)
                                SelectedPassenger.FromAirportName = airport.Name;
                            else
                                SelectedPassenger.ToAirportName = airport.Name;
                        });
                };
                ar.GetAirport(IATA_Code, true);
            }
        }

        public void RefreshPassenger()
        {
            ResetAllGradient();
            LoadPassenger();
        }

        private void ResetAllGradient()
        {
            FullNameIsNotValid = false;
            FFNIsNotValid = false;
            FlightCarrierIsNotValid = false;
            FlightNumberIsNotValid = false;
            TrackingClassOfServiceIsNotValid = false;
            NotesIsNotValid = false;
            PNRIsNotValid = false;
            FromIsNotValid = false;
            ToIsNotValid = false;
        }
    }
}
