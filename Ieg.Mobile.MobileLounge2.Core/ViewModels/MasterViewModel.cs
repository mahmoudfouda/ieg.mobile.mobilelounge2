﻿using System.Windows.Input;

namespace Ieg.Mobile.MobileLounge2.Core.ViewModels
{
    public class MasterViewModel : ViewModelBase
    {
        public ICommand SettingsCommand { get; private set; }
        public ICommand AboutCommand { get; private set; }

        public LoungesViewModel LoungesViewModel { get; private set; }

        private string _clientLogo = string.Empty;
        public string ClientLogo
        {
            get { return _clientLogo; }
            set {
                _clientLogo = value;
                NotifyPropertyChange("ClientLogo");
            }
        }

        private string _userPicture = string.Empty;
        public string UserPicture
        {
            get { return _userPicture; }
            set {
                _userPicture = value;
                NotifyPropertyChange("UserPicture");
            }
        }

        private string _title = string.Empty;
        public string Title
        {
            get { return _title; }
            private set {
                _title = value;
                NotifyPropertyChange("Title");
            }
        }

        private string _subtitle = string.Empty;
        public string Subtitle
        {
            get { return _subtitle; }
            set {
                _subtitle = value;
                NotifyPropertyChange("Subtitle");
            }
        }
    }
}
