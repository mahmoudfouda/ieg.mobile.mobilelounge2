﻿using AIMS;
using AIMS.Models;
using Ieg.Mobile.MobileLounge2.Core.Common.BindableModels;
using Ieg.Mobile.MobileLounge2.Core.Library;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Text;
using Xamarin.Forms;

namespace Ieg.Mobile.MobileLounge2.Core.ViewModels
{
    public class ValidationResultViewModel : ViewModelBase
    {
        private PassengerBindable _selectedPassenger;

        private PassengersRepository _passengersRepository = new PassengersRepository();

        private ImageRepository _imageRepository = new ImageRepository();

        private ObservableCollection<ServiceBindable> _services = new ObservableCollection<ServiceBindable>();

        private ObservableCollection<ServiceBindable> _availableServices = new ObservableCollection<ServiceBindable>();

        public PassengerBindable SelectedPassenger
        {
            get { return _selectedPassenger; }
            set
            {
                _selectedPassenger = value;
                NotifyPropertyChange("SelectedPassenger");
            }
        }

        public Passenger SelectedPassengerAIMS
        {
            get
            {
                if (MembershipProvider.Current != null)
                {
                    if (DataService.Current.IsGuestMode)
                        return MembershipProvider.Current.SelectedGuest;
                    else return MembershipProvider.Current.SelectedPassenger;
                }
                return null;
            }
            set
            {
                if (MembershipProvider.Current != null)
                {
                    if (DataService.Current.IsGuestMode)
                        MembershipProvider.Current.SelectedGuest = value;
                    else MembershipProvider.Current.SelectedPassenger = value;
                }
            }
        }

        public ObservableCollection<ServiceBindable> Services
        {
            get { return _services; }
            set
            {
                _services = value;
                NotifyPropertyChange("Services");
            }
        }

        public ObservableCollection<ServiceBindable> AvailableServices
        {
            get { return _availableServices; }
            set
            {
                _availableServices = value;
                NotifyPropertyChange("AvailableServices");
            }
        }

        private string _actualDate;

        public string ActualDate
        {
            get { return _actualDate; }
            set
            {
                _actualDate = value;
                NotifyPropertyChange("ActualDate");
            }
        }

        public string Title
        {
            get
            {
                if (MembershipProvider.Current.SelectedGuest != null)
                    return MembershipProvider.Current.SelectedGuest.FullName;
                else if (MembershipProvider.Current.SelectedPassenger != null)
                    return MembershipProvider.Current.SelectedPassenger.FullName;

                return "Validation Results";
            }
        }

        public Color BackGroundColor
        {
            get
            {
                return DataService.Current.IsGuestMode ? Color.FromHex("#9370db") : Color.FromHex("#2D9CDB"); // 0e79d8
            }
        }

        public ValidationResultViewModel()
        {
            RefreshPassenger();
        }

        public void RefreshPassenger()
        {
            //ClearContents();
            if (MembershipProvider.Current == null || MembershipProvider.Current.SelectedPassenger == null)
                return;

            //RefreshColors();

            if (MembershipProvider.Current.SelectedGuest != null)
            {
                SetContents();
            }
            else if (!MembershipProvider.Current.SelectedPassenger.IsGuest)//TODO: Avoid loading the host when validation happened (only when selected from list)
            {
                _passengersRepository.OnPassengerChecked += RetrieveMainPassengerOf;

                try
                {
                    _passengersRepository.RetrieveMainPassenger(MembershipProvider.Current.SelectedPassenger, MembershipProvider.Current.SelectedLounge);
                }
                catch (Exception ex)
                {
                    Device.BeginInvokeOnMainThread(() =>
                    {
                        MessageTitle = App.TextProvider.GetText(1104);

                        MessageError = App.TextProvider.GetText(1051);

                        ShowMessageError = true;

                        Navigation.RootMasterDetailPage.ShowOverlayPage();

                        LogsRepository.AddError("Passenger Retrieval call error", ex);
                    });
                }
            }
            else
            {
                _passengersRepository.OnPassengerChecked += RetrieveGuestPassenger;

                _passengersRepository.RetrieveGuestPassenger(MembershipProvider.Current.SelectedPassenger, MembershipProvider.Current.SelectedLounge);
            }
        }

        private void RetrieveMainPassengerOf(bool isSucceeded, int? errorCode, string errorMetadata, string message, Passenger passenger)
        {
            _passengersRepository.OnPassengerChecked -= RetrieveMainPassengerOf;
            MembershipProvider.Current.SelectedPassenger = passenger;
            SetContents();
        }

        private void RetrieveGuestPassenger(bool isSucceeded, int? errorCode, string errorMetadata, string message, Passenger passenger)
        {
            _passengersRepository.OnPassengerChecked -= RetrieveGuestPassenger;

            MembershipProvider.Current.SelectedPassenger = passenger;

            SetContents();
        }

        private void SetContents()
        {
            try
            {
                if (MembershipProvider.Current == null || MembershipProvider.Current.SelectedPassenger == null)
                    return;

                if (!DataService.Current.IsGuestMode)
                    LoadMainPassenger();
                else if (MembershipProvider.Current.SelectedGuest != null)
                    LoadGuestPassenger();

                #region Loading the returned Passenger Services (TODO: extract it to a method - as we need to refresh the quantities later)
                if (MembershipProvider.Current.SelectedPassenger.PassengerServices == null)//TODO: move this into Client Core Library inside (MembershipProvider.Current.SelectedPassenger.Set{} very last line)
                    MembershipProvider.Current.SelectedPassenger.PassengerServices = new Dictionary<PassengerService, List<Passenger>>();

                foreach (var guestCatKeyVal in MembershipProvider.Current.SelectedPassenger.PassengerServices)
                {
                    var passengerService = guestCatKeyVal.Key;
                    passengerService.Quantity = guestCatKeyVal.Value.Count;
                    var service = new ServiceBindable(passengerService);

                    foreach (var item in guestCatKeyVal.Value)
                        service.Add(item);

                    Services.Add(service);
                }
                #endregion




#if DEBUG//TODO: remove in case the service has been implemented

                SelectedPassenger.BoardingTime = "08:45AM";
                SelectedPassenger.Terminal = "3";
                SelectedPassenger.Gate = "16";
                SelectedPassenger.SeatNumber = "56E";
                SelectedPassenger.Time = "+10hrs";
#endif




            }
            catch(Exception ef)
            {
                LogsRepository.AddError("Error in SetContents()", ef);
            }


        }

        private void LoadGuestPassenger()
        {
            SelectedPassenger = new PassengerBindable(MembershipProvider.Current.SelectedGuest);

            var bpValdationDate =
           MembershipProvider.Current.SelectedPassenger.BoardingPassFlightDate != null &&
           MembershipProvider.Current.SelectedPassenger.BoardingPassFlightDate != DateTime.MinValue ?
           MembershipProvider.Current.SelectedPassenger.BoardingPassFlightDate : MembershipProvider.Current.SelectedPassenger.TrackingTimestamp;
            ActualDate = bpValdationDate.ToString("d MMM yyyy");
            SelectedPassenger.BoardingTime = bpValdationDate.ToString("d MMM yyyy");

            var guestAirline = AirlinesRepository.GetAirlineFromDB(MembershipProvider.Current.SelectedGuest.AirlineId);
            if (guestAirline != null)
            {
                if (guestAirline.AirlineLogoBytes != null && guestAirline.AirlineLogoBytes.Length > 0)
                {
                    SelectedPassenger.ImageAirline = ImageSource.FromStream(() => new MemoryStream(guestAirline.AirlineLogoBytes));
                }
                else
                {
                    _imageRepository.LoadImage(guestAirline.ImageHandle, (imageResult) =>
                    {
                        if (imageResult == null)
                        {
                            LogsRepository.AddError("Error in ImageAdapter.LoadImage()", "The result is null");
                            return;
                        }

                        if (imageResult.IsSucceeded)
                        {
                            var bytes = imageResult.ReturnParam;

                            if (bytes == null || bytes.Length == 0)
                                return;

                            Device.BeginInvokeOnMainThread(() =>
                            {
                                try
                                {
                                    SelectedPassenger.ImageAirline = ImageSource.FromStream(() => new MemoryStream(bytes));
                                }
                                catch (Exception eex)
                                {
                                    var m = eex.Message; //TODO: log
                                }
                            });
                        }

                    });
                }
            }

            SelectedPassenger.FromAirport = MembershipProvider.Current.SelectedGuest.FromAirport;
            LoadFullAirportName(MembershipProvider.Current.SelectedGuest.FromAirport, true);
            SelectedPassenger.ToAirport = string.IsNullOrWhiteSpace(MembershipProvider.Current.SelectedGuest.ToAirport) ? "..." : MembershipProvider.Current.SelectedGuest.ToAirport;
            LoadFullAirportName(SelectedPassenger.ToAirport, false);
        }

        private void LoadMainPassenger()
        {
            SelectedPassenger = new PassengerBindable(MembershipProvider.Current.SelectedPassenger);

            if (MembershipProvider.Current.SelectedCard != null)
            {
                if (MembershipProvider.Current.SelectedCard.CardPictureBytes != null && MembershipProvider.Current.SelectedCard.CardPictureBytes.Length > 0)
                {
                    SelectedPassenger.ImageCard = ImageSource.FromStream(() => new MemoryStream(MembershipProvider.Current.SelectedCard.CardPictureBytes));
                }
                else
                {
                    _imageRepository.LoadImage(MembershipProvider.Current.SelectedCard.ImageHandle, (imageResult) =>
                    {
                        if (imageResult == null)
                        {
                            LogsRepository.AddError("Error in ImageAdapter.LoadImage()", "The result is null");
                            return;
                        }

                        if (imageResult.IsSucceeded)
                        {
                            var bytes = imageResult.ReturnParam;

                            if (bytes == null || bytes.Length == 0)
                                return;

                            Device.BeginInvokeOnMainThread(() =>
                            {
                                try
                                {
                                    SelectedPassenger.ImageCard = ImageSource.FromStream(() => new MemoryStream(bytes));
                                }
                                catch (Exception eex)
                                {
                                    var m = eex.Message; //TODO: log
                                }
                            });
                        }

                    });
                }
            }

            if (MembershipProvider.Current.SelectedPassenger.OtherCardID > 0)
            {
                var otherCard = CardsRepository.GetCardFromDB(MembershipProvider.Current.SelectedPassenger.OtherCardID);

                if (otherCard != null)
                {
                    if (otherCard.CardPictureBytes != null && otherCard.CardPictureBytes.Length > 0)
                    {
                        SelectedPassenger.OtherCard = ImageSource.FromStream(() => new MemoryStream(otherCard.CardPictureBytes));
                    }
                    else
                    {
                        _imageRepository.LoadImage(otherCard.ImageHandle, (imageResult) =>
                        {
                            if (imageResult == null)
                            {
                                LogsRepository.AddError("Error in otherCard.LoadImage()", "The result is null");
                                return;
                            }

                            if (imageResult.IsSucceeded)
                            {
                                var bytes = imageResult.ReturnParam;

                                if (bytes == null || bytes.Length == 0)
                                    return;

                                Device.BeginInvokeOnMainThread(() =>
                                {
                                    try
                                    {
                                        SelectedPassenger.OtherCard = ImageSource.FromStream(() => new MemoryStream(bytes));
                                    }
                                    catch (Exception eex)
                                    {
                                        var m = eex.Message; //TODO: log
                                    }
                                });
                            }
                        });
                    }
                }
            }

            var bpValdationDate =
                MembershipProvider.Current.SelectedPassenger.BoardingPassFlightDate != null &&
                MembershipProvider.Current.SelectedPassenger.BoardingPassFlightDate != DateTime.MinValue ?
                MembershipProvider.Current.SelectedPassenger.BoardingPassFlightDate : MembershipProvider.Current.SelectedPassenger.TrackingTimestamp;

            ActualDate = bpValdationDate.ToString("d MMM yyyy");
            SelectedPassenger.BoardingTime = bpValdationDate.ToString("d MMM yyyy");

            if (MembershipProvider.Current.SelectedAirline != null)
            {
                #region Filling the Airline
                if (MembershipProvider.Current.SelectedAirline.AirlineLogoBytes != null && MembershipProvider.Current.SelectedAirline.AirlineLogoBytes.Length > 0)
                {
                    SelectedPassenger.ImageAirline = ImageSource.FromStream(() => new MemoryStream(MembershipProvider.Current.SelectedAirline.AirlineLogoBytes));
                }
                else
                {
                    _imageRepository.LoadImage(MembershipProvider.Current.SelectedAirline.ImageHandle, (imageResult) =>
                    {
                        if (imageResult == null)
                        {
                            LogsRepository.AddError("Error in ImageAdapter.LoadImage()", "The result is null");
                            return;
                        }

                        if (imageResult.IsSucceeded)
                        {
                            var bytes = imageResult.ReturnParam;

                            if (bytes == null || bytes.Length == 0)
                                return;

                            Device.BeginInvokeOnMainThread(() =>
                            {
                                try
                                {
                                    SelectedPassenger.ImageAirline = ImageSource.FromStream(() => new MemoryStream(bytes));
                                }
                                catch (Exception eex)
                                {
                                    var m = eex.Message; //TODO: log
                                }
                            });
                        }

                    });
                }
                #endregion
            }

            SelectedPassenger.AirportFrom = MembershipProvider.Current.SelectedLounge.AirportCode;
            LoadFullAirportName(MembershipProvider.Current.SelectedLounge.AirportCode, true);
            SelectedPassenger.AirportTo = string.IsNullOrWhiteSpace(MembershipProvider.Current.SelectedPassenger.ToAirport) ? "..." : MembershipProvider.Current.SelectedPassenger.ToAirport;
            LoadFullAirportName(SelectedPassenger.AirportTo, false);
        }

        private void LoadFullAirportName(string IATA_Code, bool isFrom)
        {

            using (var ar = new AirportsRepository())
            {
                ar.OnRepositoryChanged += (data) =>
                {
                    var airport = (Airport)data;
                    if (airport != null)
                        Device.BeginInvokeOnMainThread(() =>
                        {
                            if (isFrom)
                                SelectedPassenger.FromAirportName = airport.Name;
                            else
                                SelectedPassenger.ToAirportName = airport.Name;
                        });
                };
                ar.GetAirport(IATA_Code, true);
            }
        }

    }
}
