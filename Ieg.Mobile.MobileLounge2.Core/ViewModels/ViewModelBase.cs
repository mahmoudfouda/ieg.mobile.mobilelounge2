﻿using AIMS;
using Ieg.Mobile.MobileLounge2.Core.Common;
using Ieg.Mobile.MobileLounge2.Core.Common.BindableModels;
using Ieg.Mobile.MobileLounge2.Core.Library;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Windows.Input;
using System.Linq;
using Xamarin.Forms;
using Ieg.Mobile.MobileLounge2.Core.Controls;
using Xamarin.Forms.RadialMenu.Models;
using Xamarin.Forms.RadialMenu.Enumerations;
using Ieg.Mobile.MobileLounge2.Core.Common.Services;
using System;
using Ieg.Mobile.Common.Controls;
using System.Diagnostics;

namespace Ieg.Mobile.MobileLounge2.Core.ViewModels
{
    public class ViewModelBase : INotifyPropertyChanged, IDisposable
    {
        public event PropertyChangedEventHandler PropertyChanged;
        public event VoidEventHandler GoBackRequested;

        public event ActionEventHandler OnStartProcess = null;

        public event VoidEventHandler OnEndProcess = null;

        private bool _isLoading = false;
        public bool IsLoading
        {
            get { return _isLoading; }
            set
            {
                _isLoading = value;
                NotifyPropertyChange("IsLoading");
            }
        }

        private string _message = "Detecting your position. Please wait..."; // string.Empty;
        public string Message
        {
            get { return _message; }
            set
            {
                _message = value;
                NotifyPropertyChange("Message");
            }
        }

        public string SelectedLoungeName
        {
            get
            {
                return (IsLoggedIn && MembershipProvider.Current.SelectedLounge != null) ? MembershipProvider.Current.SelectedLounge.LoungeName : string.Empty;
            }

        }

        private string _messageTitle;
        public string MessageTitle
        {
            get { return _messageTitle; }
            set
            {
                _messageTitle = value;
                NotifyPropertyChange("MessageTitle");
            }
        }

        private string _messageError;
        public string MessageError
        {
            get { return _messageError; }
            set
            {
                _messageError = value;
                NotifyPropertyChange("MessageError");
            }
        }

        private string _messageQuestion;
        public string MessageQuestion
        {
            get { return _messageQuestion; }
            set
            {
                _messageQuestion = value;
                NotifyPropertyChange("MessageQuestion");
            }
        }

        private string _messageButtonOk;
        public string MessageButtonOk
        {
            get { return _messageButtonOk; }
            set
            {
                _messageButtonOk = value;
                NotifyPropertyChange("MessageButtonOk");
            }
        }

        private string _messageCancel;
        public string MessageButtonCancel
        {
            get { return _messageCancel; }
            set
            {
                _messageCancel = value;
                NotifyPropertyChange("MessageButtonCancel");
            }
        }

        public string LoggedUserName
        {
            get
            {
                return (IsLoggedIn && MembershipProvider.Current.UserFullName != null) ? MembershipProvider.Current.UserFullName : string.Empty;
            }
        }

        public string LoggedUserDescription
        {
            get
            {
                return (IsLoggedIn && MembershipProvider.Current.UserHeaderText != null) ? MembershipProvider.Current.UserHeaderText : string.Empty;
            }
        }

        private bool _showLastHourConfirmationPopup;
        public bool ShowLastHourConfirmationPopup
        {
            get { return _showLastHourConfirmationPopup; }
            set
            {
                _showLastHourConfirmationPopup = value;
                NotifyPropertyChange("ShowLastHourConfirmationPopup");
            }
        }

        private bool _showDeveloperLoginPopup;
        public bool ShowDeveloperLoginPopup
        {
            get { return _showDeveloperLoginPopup; }
            set
            {
                _showDeveloperLoginPopup = value;
                NotifyPropertyChange("ShowDeveloperLoginPopup");
            }
        }

        private bool _showMessageError;
        public bool ShowMessageError
        {
            get { return _showMessageError; }
            set
            {
                _showMessageError = value;
                NotifyPropertyChange("ShowMessageError");
            }
        }

        private bool _showQuestion;
        public bool ShowQuestion
        {
            get { return _showQuestion; }
            set
            {
                _showQuestion = value;
                NotifyPropertyChange("ShowQuestion");
            }
        }

        public bool ShouldPopPage { get; set; }

        private string _devUserName = "dev";
        public string DevUserName
        {
            get { return _devUserName; }
            set
            {
                _devUserName = value;
                NotifyPropertyChange("DevUserName");
            }
        }

        private string _devPassword;
        public string DevPassword
        {
            get { return _devPassword; }
            set
            {
                _devPassword = value;
                NotifyPropertyChange("DevPassword");
            }
        }

        public ICommand PopupCancelCommand { get; set; }

        public ICommand ConfirmQuestionPopupCommand { get; set; }

        public ICommand PopupCancelQuestionCommand { get; set; }

        protected void RequestClose()
        {
            Xamarin.Forms.Device.BeginInvokeOnMainThread(() =>
            {
                GoBackRequested?.Invoke();
            });
        }

        public virtual void LoadData(bool forceGet = false)
        {

        }

        public bool IsMenuOpenned
        {
            get
            {
                return Navigation.IsMenuOpenned;
            }
        }

        public bool IsLoggedIn
        {
            get
            {
                return MembershipProvider.Current != null && MembershipProvider.Current.LoggedinUser != null && MembershipProvider.Current.IsAuthenticated;
            }
        }

        public bool IsDeveloperModeEnabled
        {
            get
            {
                return App.IsDeveloperModeEnabled;
            }
        }

        public static LoungeBindable SelectedLoungeBindable { get;  set; }

        public INavigationService Navigation
        {
            get
            {
                return NavigatorService.Current;
            }
        }

        public ICommand LogoutCommand { get; private set; }

        public ViewModelBase()
        {
            LogoutCommand = new Xamarin.Forms.Command(async () =>
            {
                await MockLoginController.Current.Logout(true);
            });


            PopupCancelCommand = new Xamarin.Forms.Command(() =>
            {
                HideAllPopup();
            });


            NotifyPropertyChange("MenuItems");
        }

        protected void NotifyPropertyChange(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        public void CloseMenu()
        {
            Navigation.CloseMenu();
        }

        public void OpenMenu()
        {
            Navigation.OpenMenu();
        }

        public virtual void HideAllPopup()
        {
            ShowMessageError =
            ShowDeveloperLoginPopup =
            ShowQuestion = 
            ShowLastHourConfirmationPopup = false;

            Navigation.RootMasterDetailPage.HideOverlayPage();
        }

        protected void StartProcess(Action actionToCall)
        {
            Device.BeginInvokeOnMainThread(() =>
            {
                Navigation.RootMasterDetailPage.ShowOverlayPage();

                if (OnStartProcess != null)
                    OnStartProcess(actionToCall);
            });
        }

        protected void EndProcess()
        {
            if (OnEndProcess != null)
                OnEndProcess();
        }

        protected void DoDevLogin()
        {
            if (string.IsNullOrWhiteSpace(DevUserName) || string.IsNullOrEmpty(DevPassword))
            {
                Device.BeginInvokeOnMainThread(() =>
                {
                    MessageTitle = App.TextProvider.GetText(2047);

                    MessageError = App.TextProvider.GetText(1001);

                    EndProcess();

                    ShowMessageError = true;

                    Navigation.RootMasterDetailPage.ShowOverlayPage();
                });
            }
            else
            {
                MembershipProvider.Current.ValidateDeveloper(DevUserName, DevPassword, DefaultMobileApplication.Current.Position, (ss, result) =>
                {
                    if (result.Item1)
                    {
                        App.IsDeveloperModeEnabled = true;
                        App.DevMode = true;

                        Device.BeginInvokeOnMainThread(() =>
                        {
                            NotifyPropertyChange("IsDeveloperModeEnabled");

                            EndProcess();
                        });
                    }
                    else
                    {
                        string errorMessage = App.TextProvider.GetText((int)result.Item2);

                        Device.BeginInvokeOnMainThread(() =>
                        {
                            LogsRepository.AddWarning("Developer login failed",
                                string.Format(
                                    "A user with username '{0}' failed to authenticate as developer with username '{1}',\nError Code: {2}\nError Message: {3}",
                                    MembershipProvider.Current.LoggedinUser.Username, DevUserName, result.Item2,
                                    errorMessage));
                        });

                        Device.BeginInvokeOnMainThread(() =>
                        {
                            MessageTitle = App.TextProvider.GetText(2047);

                            MessageError = errorMessage;

                            EndProcess();

                            ShowMessageError = true;

                            Navigation.RootMasterDetailPage.ShowOverlayPage();
                        });
                    }

                });
            }
        }

        public virtual void Dispose()
        {
            
        }

        protected void RejectPassenger()
        {
            try
            {
                using (var pr = new PassengersRepository())
                {
                    pr.AgentRejectPassenger(MembershipProvider.Current.SelectedLounge);
                }

                Navigation.Pop();
            }
            catch (Exception ex)
            {
                HideAllPopup();
                MessageTitle = App.TextProvider.GetText(1104);
                MessageError = App.TextProvider.GetText(1051);
                ShouldPopPage = true;
                ShowMessageError = true;
                Navigation.RootMasterDetailPage.ShowOverlayPage();
                LogsRepository.AddError("Passenger deny call error", ex);
            }
        }
    }
}
