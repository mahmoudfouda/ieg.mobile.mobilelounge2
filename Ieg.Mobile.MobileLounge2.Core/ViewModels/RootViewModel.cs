﻿using AIMS;
using Ieg.Mobile.MobileLounge2.Core.Common.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Ieg.Mobile.MobileLounge2.Core.ViewModels
{
    public class RootViewModel : ViewModelBase
    {

        public LoungesViewModel LoungesViewModel { get; private set; }

        public RootViewModel()
        {
            LoungesViewModel = new LoungesViewModel();

            LoadData();
        }

        public void LoadData()
        {
            if (!IsLoggedIn)
                return;

            LoungesViewModel.LoadData();
        }
    }
}
