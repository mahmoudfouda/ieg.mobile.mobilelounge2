﻿using AIMS;
using AIMS.Models;
using Ieg.Mobile.MobileLounge2.Core.Common.BindableModels;
using Ieg.Mobile.MobileLounge2.Core.Library;
using Ieg.Mobile.MobileLounge2.Core.Views.ContentView;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Windows.Input;
using Xamarin.Forms;

namespace Ieg.Mobile.MobileLounge2.Core.ViewModels
{
    public class CardsViewModel : ViewModelBase
    {
        private const int ITEM_SIZE_LIST = 100;

        private CardsRepository _cardsRepository;
        private ImageRepository _imageRepository = new ImageRepository();

        private List<CardBindable> _sourceReadOnly = new List<CardBindable>();

        private ObservableCollection<CardBindable> _cardReadOnly = new ObservableCollection<CardBindable>();

        public ObservableCollection<CardBindable> CardsReadOnly
        {
            get { return _cardReadOnly; }
            set
            {
                _cardReadOnly = value;
                NotifyPropertyChange("CardsReadOnly");
            }
        }

        private string _keyword;

        public string Keyword
        {
            get { return _keyword; }
            set
            {
                _keyword = value;

                if (!string.IsNullOrWhiteSpace(_keyword))
                    FilterItems(_keyword);
                else
                    ShowReadOnlyList();

                NotifyPropertyChange("Keyword");
                NotifyPropertyChange("IsFiltered");
            }
        }

        public bool IsFiltered
        {
            get
            {
                return !string.IsNullOrWhiteSpace(Keyword) && Keyword.Length > 0;
            }
        }

        public ICommand FilterCommand { get; set; }

        public ICommand CardSelectedCommand { get; set; }

        private int _heightPrimaryList = 0;
        public int HeightPrimaryList
        {
            get
            {
                return _heightPrimaryList;
            }
            set
            {
                _heightPrimaryList = value;
                NotifyPropertyChange("HeightPrimaryList");
            }
        }

        private int _heightReadOnlyList = 0;
        public int HeightReadOnlyList
        {
            get
            {
                return _heightReadOnlyList;
            }
            set
            {
                _heightReadOnlyList = value;
                NotifyPropertyChange("HeightReadOnlyList");
            }
        }


        private CardBindable _selectedCard;

        public string Title
        {
            get
            {
                if (MembershipProvider.Current != null && MembershipProvider.Current.SelectedAirline != null)
                    return MembershipProvider.Current.SelectedAirline.Name;
                else
                    return "Cards";
            }
        }

        public CardBindable SelectedCard
        {
            get { return _selectedCard; }
            set
            {
                _selectedCard = value;
                NotifyPropertyChange("SelectedCard");
            }
        }

        #region Methods

        public CardsViewModel()
        {
            _cardsRepository = new CardsRepository();

            FilterCommand = new Command<string>(FilterItems);

            CardSelectedCommand = new Command(async (model) =>
            {
                if (model != null)
                {
                    SelectedCard = model as CardBindable;

                    MembershipProvider.Current.SelectedCard = SelectedCard.AimsCard;
                    //this.Title = SelectedCard.Name;

                    DataService.Current.ValidationResult = new PassengerValidation();

                    MembershipProvider.Current.SelectedPassenger = new Passenger
                    {
                        CardID = SelectedCard.AimsCard.ID,
                        AirlineId = SelectedCard.AimsCard.AirlineID
                    };


                    await Navigation.Push(new PassengerEntryContentView());
                }
            });

            LoadData();
        }

        public override void LoadData(bool forceGet = false)
        {
            if (!IsLoggedIn)
                return;

            if (MembershipProvider.Current.SelectedAirline == null)
                return;

            _keyword = string.Empty;
            IsLoading = true;

            if (CardsReadOnly.Count == 0)
            {
                _cardsRepository.OnRepositoryChanged += FirstCardLoad;
                _cardsRepository.LoadCards(false);
            }
            else
            {
                ShowReadOnlyList();
            }
        }

        private void ShowReadOnlyList()
        {
            IsLoading = true;

            CardsReadOnly.Clear();

            foreach (var card in _sourceReadOnly)
            {
                CardsReadOnly.Add(card);
            }

            var rowsCount = (int)Math.Ceiling((decimal)CardsReadOnly.Count / 3);
            HeightReadOnlyList = rowsCount * ITEM_SIZE_LIST;
            HeightPrimaryList = 0;
            IsLoading = false;
        }

        private void FirstCardLoad(object items)
        {
            _cardsRepository.OnRepositoryChanged -= FirstCardLoad;
            var newItems = ((IEnumerable<AIMS.Models.Card>)items).ToList();
            FinishedLoadCards(CardsReadOnly, newItems);

            var rowsCount = (int)Math.Ceiling((decimal)newItems.Count / 3);
            HeightReadOnlyList = rowsCount * ITEM_SIZE_LIST;
            HeightPrimaryList = 0;
        }

        private void FinishedLoadCards(ObservableCollection<CardBindable> listCards, List<AIMS.Models.Card> returnedItems)
        {
            if (!IsLoggedIn)
            {
                IsLoading = false;
                return;
            }

            Device.BeginInvokeOnMainThread(() =>
            {
                try
                {
                    listCards.Clear();

                    _sourceReadOnly.Clear();

                    if (returnedItems is IEnumerable<AIMS.Models.Card>)
                    {
                        foreach (var crReturned in returnedItems)
                        {
                            var card = new CardBindable(crReturned);
                            listCards.Add(card);

                            _sourceReadOnly.Add(card);

                            LoadImage(card);
                        }
                    }

                    NotifyPropertyChange("CardsReadOnly");
                }
                catch
                {
                    //TODO: Log
                }
                finally
                {
                    IsLoading = false;
                }
            });
        }

        public void FilterItems(string filter)
        {
            IsLoading = true;

            CardsReadOnly.Clear();

            var filteredCards = (from item in _sourceReadOnly
                                        where item.Name.ToUpper().Contains(filter.ToUpper())
                                        select item).ToList();

            foreach (var card in filteredCards)
            {
                CardsReadOnly.Add(card);
            }


            IsLoading = false;
        }

        private void CardsLoad(object item)
        {
            _cardsRepository.OnRepositoryChanged -= CardsLoad;
        }

        private void LoadImage(CardBindable cardBindable)
        {
            _imageRepository.LoadImage(cardBindable.ImageUrl, (imageResult) =>
            {
                if (imageResult == null)
                {
                    LogsRepository.AddError("Error in ImageAdapter.LoadCardImage()", "The result is null");
                    return;
                }

                if (imageResult.IsSucceeded)
                {
                    var bytes = imageResult.ReturnParam;

                    if (bytes == null || bytes.Length == 0)
                        return;

                    Device.BeginInvokeOnMainThread(() =>
                    {
                        try
                        {
                            cardBindable.Image = ImageSource.FromStream(() => new MemoryStream(bytes));
                        }
                        catch (Exception eex)
                        {
                            var m = eex.Message; //TODO: log
                        }
                    });
                }
            });
        }

        #endregion


    }
}
