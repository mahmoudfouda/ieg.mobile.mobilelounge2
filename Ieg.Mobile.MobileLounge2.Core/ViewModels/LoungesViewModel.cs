﻿using AIMS;
using Ieg.Mobile.MobileLounge2.Core.Common.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.Threading.Tasks;
using System.Linq;
using Ieg.Mobile.MobileLounge2.Core.Common.BindableModels;

namespace Ieg.Mobile.MobileLounge2.Core.ViewModels
{
    public class LoungesViewModel : ViewModelBase
    {
        private LoungeBindable _selectedLounge;

        private ObservableCollection<LoungeBindable> _availableLounges = new ObservableCollection<LoungeBindable>();
        public ObservableCollection<LoungeBindable> AvailableLounges
        {
            get { return _availableLounges; }
            set
            {
                _availableLounges = value;
                NotifyPropertyChange("AvailableLounges");
            }
        }

        public LoungeBindable SelectedLounge
        {
            get { return _selectedLounge; }
            set
            {
                if (value == null) return;
                if (_selectedLounge != null && _selectedLounge.Name.Equals(value.Name)) return;
                if (_selectedLounge != null) _selectedLounge.IsSelected = false;

                value.IsSelected = true;

                _selectedLounge = value;

                if (MembershipProvider.Current != null)
                {
                    MembershipProvider.Current.SelectedLounge = value.LoungeMenuItem.AimsWorkstation;

                    SelectedLoungeBindable = value;

                    LogsRepository.AddClientInfo("User selected a lounge", string.Format("Lounge detail: {0} | WSID: {1}", MembershipProvider.Current.SelectedLounge.LoungeName, MembershipProvider.Current.SelectedLounge.WorkstationID));
                }

                NotifyPropertyChange("SelectedLounge");

            }
        }

        public LoungesViewModel()
        {
            LoadData();
        }

        public void LoadData()
        {
            IsLoading = true;

            try
            {
                if (!IsLoggedIn)
                    return;

                AvailableLounges.Clear();

                var result = MembershipProvider.Current.UserLounges.Select(x => new Workstation(x)).ToList();

                //for (int i = 0; i < 8; i++)//TODO: Remove
                {
                    foreach (var item in result)
                    {
                        AvailableLounges.Add(new LoungeBindable(new LoungeMenuItem(item)));
                    }
                }

                SelectedLounge = AvailableLounges.FirstOrDefault();
            }
            catch
            {
                //TODO: Log
            }
            finally
            {
                IsLoading = false;
            }
        }
    }
}
