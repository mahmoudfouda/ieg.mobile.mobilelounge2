﻿using AIMS;
using Ieg.Mobile.MobileLounge2.Core.Views.ContentView;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Windows.Input;
using Xamarin.Forms;

namespace Ieg.Mobile.MobileLounge2.Core.ViewModels
{
    public class ProfileViewModel : ViewModelBase
    {
        private ImageRepository _imageRepository = new ImageRepository();

        private ImageSource _userAirlineTopIcon;
        public ImageSource UserAirlineTopIcon
        {
            get
            {
                return _userAirlineTopIcon;
            }
            set
            {
                _userAirlineTopIcon = value;
                NotifyPropertyChange("UserAirlineTopIcon");
            }
        }

        public string AirportName
        {
            get
            {
                if (!IsLoggedIn)
                    return string.Empty;

                return MembershipProvider.Current.LoggedinUser.AirportName;
            }
        }

        public string WorkstationName
        {
            get
            {
                if (!IsLoggedIn || MembershipProvider.Current.SelectedLounge == null)
                    return string.Empty;

                return MembershipProvider.Current.SelectedLounge.LoungeName;
            }
        }

        public int OccupanyPercentage
        {
            get
            {
                if (SelectedLoungeBindable == null)
                    return 0;

                return SelectedLoungeBindable.OccupanyPercentage;
            }
        }

        private int _selectedIndexLanguage = 0;
        public int SelectedIndexLanguage
        {
            get { return _selectedIndexLanguage; }
            set
            {
                _selectedIndexLanguage = value;
                NotifyPropertyChange("SelectedIndexLanguage");
            }
        }

        public string CircleColor
        {
            get
            {
                return OccupanyPercentage >= 90 ? "#eb5656" : OccupanyPercentage >= 50 ? "#f2c84b" : "#6ecf97";
            }
        }

        public ICommand LastHourCommand { get; set; }

        public ICommand AdvancedSettingsCommand { get; set; }

        public ICommand ConfirmLoginPopupCommand { get; set; }

        public ICommand MessagesCommand { get; set; }

        public ICommand LogsCommand { get; set; }


        public ProfileViewModel()
        {
            LastHourCommand = new Command(async () =>
            {
                Title = App.TextProvider.GetText(2010);
                await Navigation.Push(new LastHourContentView());
            });

            MessagesCommand = new Command(async () =>
            {
                Title = App.TextProvider.GetText(2041);
                await Navigation.Push(new MessagesContentView());
            });

            LogsCommand = new Command(async () =>
            {
                Title = App.TextProvider.GetText(2040);
                await Navigation.Push(new LogsContentView());
            });

            ConfirmLoginPopupCommand = new Command(() =>
            {
                StartProcess(DoDevLogin);
            });

            AdvancedSettingsCommand = new Command(() =>
            {
#if DEBUG
                DevUserName = "dev";
                var date = string.Format("{0}{1}{2}dev", DateTime.UtcNow.Month, DateTime.UtcNow.Day, DateTime.UtcNow.Year);
                date = date.Replace("/", "").Replace("-", "").Replace("0", "").Replace(" ", "");//2016/07/15-> 715216
                DevPassword = date;
#endif
                Device.BeginInvokeOnMainThread(() =>
                {
                    ShowDeveloperLoginPopup = true;
                    Navigation.RootMasterDetailPage.ShowOverlayPage();
                });
            });

            LoadImage();
            Title = App.TextProvider.GetText(2129);
        }

        private void LoadImage()
        {
            var usersAirline = AirlinesRepository.GetAirlineFromDB(MembershipProvider.Current.LoggedinUser.AirlineId);
            if (usersAirline != null && usersAirline.AirlineLogoBytes != null && usersAirline.AirlineLogoBytes.Length > 0)
            {
                UserAirlineTopIcon = ImageSource.FromStream(() => new MemoryStream(usersAirline.AirlineLogoBytes));
            }
            else if (usersAirline != null)
            {
                _imageRepository.LoadImage(usersAirline.ImageHandle, (imageResult) =>
                    {
                        if (imageResult == null)
                        {
                            LogsRepository.AddError("Error in ImageAdapter.LoadCardImage()", "The result is null");
                            return;
                        }

                        if (imageResult.IsSucceeded)
                        {
                            var bytes = imageResult.ReturnParam;

                            if (bytes == null || bytes.Length == 0)
                                return;

                            Device.BeginInvokeOnMainThread(() =>
                            {
                                try
                                {
                                    UserAirlineTopIcon = ImageSource.FromStream(() => new MemoryStream(bytes));
                                }
                                catch (Exception eex)
                                {
                                    var m = eex.Message; //TODO: log
                                }
                            });
                        }
                    });
            }
        }

        private string _title = "My Profile";
        public string Title
        {
            set
            {
                _title = value;
                NotifyPropertyChange("Title");
            }
            get
            {
                return _title;
            }
        }
    }
}
