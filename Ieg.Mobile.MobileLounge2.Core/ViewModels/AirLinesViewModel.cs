﻿using AIMS;
using Ieg.Mobile.MobileLounge2.Core.Common.BindableModels;
using Ieg.Mobile.MobileLounge2.Core.Common.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;
using System.Linq;
using Xamarin.Forms.RadialMenu.Models;
using Xamarin.Forms.RadialMenu.Enumerations;
using Ieg.Mobile.MobileLounge2.Core.Controls;
using Ieg.Mobile.MobileLounge2.Core.Views;
using Ieg.Mobile.MobileLounge2.Core.Views.ContentView;

namespace Ieg.Mobile.MobileLounge2.Core.ViewModels
{
    public class AirLinesViewModel : ViewModelBase
    {
        #region Fields and Properties

        private const int ITEM_SIZE_LIST = 150;

        private AirlinesRepository _airlinesRepository;

        private ObservableCollection<AirlineBindable> _airlinesReadOnly = new ObservableCollection<AirlineBindable>();

        private ObservableCollection<AirlineBindable> _firstListAirlines = new ObservableCollection<AirlineBindable>();

        private ObservableCollection<AirlineBindable> _secondListAirlines = new ObservableCollection<AirlineBindable>();

        private AirlineBindable _selectedAirline;

        private ImageRepository _imageRepository = new ImageRepository();

        public ObservableCollection<AirlineBindable> AirlinesReadOnly
        {
            get { return _airlinesReadOnly; }
            set
            {
                _airlinesReadOnly = value;
                NotifyPropertyChange("AirlinesReadOnly");
            }
        }

        public ObservableCollection<AirlineBindable> FirstListAirlines
        {
            get { return _firstListAirlines; }
            set
            {
                _firstListAirlines = value;
                NotifyPropertyChange("FirstListAirlines");
            }
        }

        public ObservableCollection<AirlineBindable> SecondListAirlines
        {
            get { return _secondListAirlines; }
            set
            {
                _secondListAirlines = value;
                NotifyPropertyChange("SecondListAirlines");
            }
        }

        public AirlineBindable SelectedAirline
        {
            get { return _selectedAirline; }
            set
            {
                _selectedAirline = value;
                NotifyPropertyChange("SelectedAirline");
            }
        }

        private ImageSource _userAirlineTopIcon;
        public ImageSource UserAirlineTopIcon
        {
            get { return _userAirlineTopIcon; }
            set
            {
                _userAirlineTopIcon = value;
                NotifyPropertyChange("UserAirlineTopIcon");
            }
        }
        public ICommand FilterCommand { get; set; }

        public ICommand AirLineSelectedCommand { get; set; }

        private int _heightSecondList = 0;
        public int HeightSecondList
        {
            get
            {
                return _heightSecondList;
            }
            set
            {
                _heightSecondList = value;
                NotifyPropertyChange("HeightSecondList");
            }
        }

        private int _heightPrimaryList = 0;
        public int HeightPrimaryList
        {
            get
            {
                return _heightPrimaryList;
            }
            set
            {
                _heightPrimaryList = value;
                NotifyPropertyChange("HeightPrimaryList");
            }
        }

        private int _heightReadOnlyList = 0;
        public int HeightReadOnlyList
        {
            get
            {
                return _heightReadOnlyList;
            }
            set
            {
                _heightReadOnlyList = value;
                NotifyPropertyChange("HeightReadOnlyList");
            }
        }

        private string _keyword;

        public string Keyword
        {
            get { return _keyword; }
            set
            {
                _keyword = value;

                if (!string.IsNullOrWhiteSpace(_keyword))
                    FilterItems(_keyword);
                else ShowReadOnlyList();
                //LoadData();

                NotifyPropertyChange("Keyword");
                NotifyPropertyChange("IsFiltered");
            }
        }

        public bool IsFiltered
        {
            get
            {
                return !string.IsNullOrWhiteSpace(Keyword) && Keyword.Length > 0;
            }
        }

        private string _title;
        public string Title
        {
            set
            {
                _title = value;
                NotifyPropertyChange("Title");
            }
            get
            {
                if (string.IsNullOrEmpty(_title))
                {
                    if (MembershipProvider.Current != null && MembershipProvider.Current.SelectedLounge != null)
                        return MembershipProvider.Current.SelectedLounge.LoungeName + " Lounge";
                    else
                        return LoggedUserDescription;
                }

                return _title;
            }
        }

        #endregion

        public AirLinesViewModel()
        {
            _airlinesRepository = new AirlinesRepository();

            FilterCommand = new Command<string>(FilterItems);
            LoadData(true);

            AirLineSelectedCommand = new Command(async (model) =>
            {
                if (model != null)
                {
                    SelectedAirline = model as AirlineBindable;

                    MembershipProvider.Current.SelectedAirline = SelectedAirline.Airline.AimsAirline;
                    //this.Title = SelectedAirline.Airline.AimsAirline.Name;

                    //await Navigation.Push(typeof(CardsPage));

                    await Navigation.Push(new CardsContentView());
                }
            });
        }

        #region Methods
        public void FilterItems(string filter)
        {
            IsLoading = true;

            #region Using AIMS core
            _airlinesRepository.OnRepositoryChanged += AirlineSearch;

            _airlinesRepository.SearchAirlines(filter);
            #endregion
        }

        public override void LoadData(bool forceGet = false)
        {
            if (!IsLoggedIn)
                return;

            if (MembershipProvider.Current.SelectedLounge == null)
            {
                if (MembershipProvider.Current.UserLounges == null || MembershipProvider.Current.UserLounges.Count() == 0)
                    return;

                //TODO: Check it
                MembershipProvider.Current.SelectedLounge = MembershipProvider.Current.UserLounges.FirstOrDefault();
                //Navigation_OnHistoryUpdated(this, null);//TODO Maybe we need to update breadcrumb manually
            }

            _keyword = string.Empty;

            Device.BeginInvokeOnMainThread(() =>
            {
                IsLoading = true;
            });

            if (AirlinesReadOnly.Count == 0)
            {
                _airlinesRepository.OnRepositoryChanged += FirstAirlineLoad;

                Task.Run(() =>
                {
                    _airlinesRepository.LoadAirlines(forceGet);
                });

            }
            else
            {
                ShowReadOnlyList();
            }
        }

        private void ShowReadOnlyList()
        {
            var rowsCount = (int)Math.Ceiling((decimal)AirlinesReadOnly.Count / 3);
            HeightReadOnlyList = rowsCount * ITEM_SIZE_LIST;
            HeightPrimaryList = 0;
            HeightSecondList = 0;

            Device.BeginInvokeOnMainThread(() =>
            {
                IsLoading = false;
            });
        }

        private void FirstAirlineLoad(object items)
        {
            _airlinesRepository.OnRepositoryChanged -= FirstAirlineLoad;

            var newItems = ((IEnumerable<AIMS.Models.Airline>)items).ToList();
            FinishedLoadAirlines(AirlinesReadOnly, newItems);

            var rowsCount = (int)Math.Ceiling((decimal)newItems.Count / 3);
            HeightReadOnlyList = rowsCount * ITEM_SIZE_LIST;
            HeightPrimaryList = 0;
            HeightSecondList = 0;
        }

        private void AirlineSearch(object items)
        {
            HeightReadOnlyList = 0;

            _airlinesRepository.OnRepositoryChanged -= AirlineSearch;
            var tempAirLines = ((IEnumerable<AIMS.Models.Airline>)items).ToList();

            var separatorIndex = GetSeparatorIndex(tempAirLines);

            if (separatorIndex != -1)
            {
                var tempFirstLine = tempAirLines.Take(separatorIndex).ToList();
                FinishedLoadAirlines(FirstListAirlines, tempFirstLine);

                var rowsCount = (int)Math.Ceiling((decimal)tempFirstLine.Count / 3);
                HeightPrimaryList = rowsCount * ITEM_SIZE_LIST;

                var tempSecondLine = tempAirLines.Skip(separatorIndex + 1).ToList();
                FinishedLoadAirlines(SecondListAirlines, tempSecondLine);

                rowsCount = (int)Math.Ceiling((decimal)tempSecondLine.Count / 3);
                HeightSecondList = rowsCount * ITEM_SIZE_LIST;
            }
            else
            {
                FinishedLoadAirlines(FirstListAirlines, tempAirLines);
                var rowsCount = (int)Math.Ceiling((decimal)tempAirLines.Count / 3);
                HeightPrimaryList = rowsCount * ITEM_SIZE_LIST;
                HeightSecondList = 0;
            }
        }

        private void FinishedLoadAirlines(ObservableCollection<AirlineBindable> listAirLines, List<AIMS.Models.Airline> returnedItems)
        {
            if (!IsLoggedIn)
            {
                IsLoading = false;
                return;
            }

            Device.BeginInvokeOnMainThread(() =>
            {
                try
                {
                    listAirLines.Clear();

                    if (returnedItems is IEnumerable<AIMS.Models.Airline>)
                    {
                        #region To show the icon of default Airline from current User
                        //var usersAirline = AirlinesRepository.GetAirlineFromDB(MembershipProvider.Current.LoggedinUser.AirlineId);
                        //if (usersAirline != null && usersAirline.AirlineLogoBytes != null && usersAirline.AirlineLogoBytes.Length > 0)
                        //{
                        //    UserAirlineTopIcon = ImageSource.FromStream(() => new MemoryStream(usersAirline.AirlineLogoBytes));
                        //}
                        //else if (usersAirline != null)
                        //{
                        //    _imageRepository.LoadImage(usersAirline.ImageHandle, (imageResult) =>
                        //        {
                        //            if (imageResult == null)
                        //            {
                        //                LogsRepository.AddError("Error in ImageAdapter.LoadCardImage()", "The result is null");
                        //                return;
                        //            }

                        //            if (imageResult.IsSucceeded)
                        //            {
                        //                var bytes = imageResult.ReturnParam;

                        //                if (bytes == null || bytes.Length == 0)
                        //                    return;

                        //                Device.BeginInvokeOnMainThread(() =>
                        //                {
                        //                    try
                        //                    {
                        //                        UserAirlineTopIcon = ImageSource.FromStream(() => new MemoryStream(bytes));
                        //                    }
                        //                    catch (Exception eex)
                        //                    {
                        //                        var m = eex.Message; //TODO: log
                        //                    }
                        //                });
                        //            }
                        //        });
                        //}
                        #endregion

                        foreach (var arReturned in returnedItems)
                        {
                            var airLine = new AirlineBindable(new Airline(arReturned)
                            {
                                Descriptions = arReturned.Description,
                                IATACode = arReturned.Code,
                                Id = arReturned.ID,
                                ImageUrl = arReturned.ImageHandle,
                                Name = arReturned.Name,
                            });

                            listAirLines.Add(airLine);
                            LoadImage(airLine);
                        }
                    }

                    NotifyPropertyChange("FirstListAirlines");
                    NotifyPropertyChange("SecondListAirlines");
                    NotifyPropertyChange("AirlinesReadOnly");

                }
                catch
                {
                    //TODO: Log
                }
                finally
                {
                    IsLoading = false;
                }

            });
        }

        private void LoadImage(AirlineBindable airline)
        {
            Task.Run(() =>
            {
                _imageRepository.LoadImage(airline.ImageUrl, (imageResult) =>
                {
                    if (imageResult == null)
                    {
                        LogsRepository.AddError("Error in ImageAdapter.LoadCardImage()", "The result is null");
                        return;
                    }

                    if (imageResult.IsSucceeded)
                    {
                        var bytes = imageResult.ReturnParam;

                        if (bytes == null || bytes.Length == 0)
                            return;

                        Device.BeginInvokeOnMainThread(() =>
                        {
                            try
                            {
                                airline.Image = ImageSource.FromStream(() => new MemoryStream(bytes));
                            }
                            catch (Exception eex)
                            {
                                var m = eex.Message; //TODO: log
                            }
                        });
                    }

                });
            });
        }

        private int GetSeparatorIndex(List<AIMS.Models.Airline> items)
        {
            if (!items.Any(x => x.Name.Equals("######") && x.Code.Equals("######")))
                return -1;

            var separatorIndex = 0;
            while (!items[separatorIndex].Name.Equals("######") || !items[separatorIndex].Code.Equals("######"))
            {
                separatorIndex++;
            }
            return separatorIndex;
        }
        #endregion
    }
}
