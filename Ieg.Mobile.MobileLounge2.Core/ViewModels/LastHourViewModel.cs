﻿using AIMS;
using AIMS.Models;
using Ieg.Mobile.MobileLounge2.Core.Common.BindableModels;
using Ieg.Mobile.MobileLounge2.Core.Views.ContentView;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace Ieg.Mobile.MobileLounge2.Core.ViewModels
{
    public class LastHourViewModel : ViewModelBase
    {
        private bool failMode = false;
        PassengersRepository passengerRepository = new PassengersRepository();

        private ObservableCollection<PassengerBindable> _passengers = new ObservableCollection<PassengerBindable>();

        public ObservableCollection<PassengerBindable> Passengers
        {
            get { return _passengers; }
            set
            {
                _passengers = value;
                NotifyPropertyChange("Passengers");
            }
        }

        private PassengerBindable _selectedPassenger;


        public PassengerBindable SelectedPassenger
        {
            get { return _selectedPassenger; }
            set
            {
                if (value == null)
                    return;

                _selectedPassenger = value;
                NotifyPropertyChange("SelectedPassenger");

                if (!_selectedPassenger.Fail)
                {
                    MembershipProvider.Current.SelectedPassenger = _selectedPassenger.Passenger;

                    Navigation.Push(new ValidationResultContentView());
                }
                else
                {
                    string title = "", text = "";
                    if (string.IsNullOrEmpty(_selectedPassenger.Passenger.FailedReason))
                    {
                        title = App.TextProvider.GetText(2045);
                        text = _selectedPassenger.Passenger.BarcodeString;
                    }
                    else
                    {
                        var newLineIndex = _selectedPassenger.Passenger.FailedReason.IndexOf("\n");
                        if (newLineIndex == -1)
                        {
                            title = _selectedPassenger.Passenger.FailedReason;
                            text = _selectedPassenger.Passenger.BarcodeString;
                        }
                        else
                        {
                            title = _selectedPassenger.Passenger.FailedReason.Substring(0, newLineIndex);
                            text = string.Format("{0}\n\n{1}\n", _selectedPassenger.Passenger.FailedReason.Substring(newLineIndex + 1),
                                !string.IsNullOrWhiteSpace(_selectedPassenger.Passenger.BarcodeString) ?
                                string.Format("Barcode: {0}", _selectedPassenger.Passenger.BarcodeString) :
                                string.Format("Manually entered\nFull Name: {0}\nFlight: [{1}-{2}-{3}]\nFFN: {4}",
                                _selectedPassenger.Passenger.FullName, _selectedPassenger.Passenger.FlightCarrier,
                                _selectedPassenger.Passenger.FlightNumber, _selectedPassenger.Passenger.TrackingClassOfService,
                                _selectedPassenger.Passenger.FFN));
                        }
                    }

                    MessageTitle = title;
                    MessageError = text;
                    ShowMessageError = true;

                    Navigation.RootMasterDetailPage.ShowOverlayPage();
                }
            }
        }



        public ICommand SearchCommand { get; private set; }

        public ICommand DeleteCommand { get; private set; }

        public ICommand ConfirmPopupCloseCommand { get; private set; }

        private string _keyWord;

        private CancellationTokenSource throttleCts = new CancellationTokenSource();

        public string Keyword
        {
            get { return _keyWord; }
            set
            {
                _keyWord = value;

                if (value == null)
                    return;

                NotifyPropertyChange("Keyword");

                if (string.IsNullOrWhiteSpace(Keyword))
                    LoadPassengers();
                else
                    SearchPassengers(_keyWord);
            }
        }

        public LastHourViewModel()
        {
            SearchCommand = new Command(() =>
            {
                if (string.IsNullOrWhiteSpace(Keyword))
                {
                    LoadPassengers();
                }
                else
                {
                    SearchPassengers(Keyword);
                }
            });

            DeleteCommand = new Command((model) =>
            {
                if (model != null)
                    _selectedPassenger = (model as PassengerBindable);

                ShowLastHourConfirmationPopup = true;
                Navigation.RootMasterDetailPage.ShowOverlayPage();
            });

            ConfirmPopupCloseCommand = new Command(() =>
            {
                if (_selectedPassenger != null)
                    Passengers.Remove(_selectedPassenger);

                ShowLastHourConfirmationPopup = false;
                Navigation.RootMasterDetailPage.HideOverlayPage();
            });

            Title = App.TextProvider.GetText(2010);

            LoadPassengers();
        }

        public void SearchPassengers(string keyword)
        {
            Interlocked.Exchange(ref throttleCts, new CancellationTokenSource()).Cancel();

            Task.Delay(TimeSpan.FromMilliseconds(1000), this.throttleCts.Token)
                .ContinueWith(delegate
                {
                    this.PerformSearch(keyword);
                },
                CancellationToken.None, TaskContinuationOptions.OnlyOnRanToCompletion,
                TaskScheduler.FromCurrentSynchronizationContext());
        }

        private void PerformSearch(string keyword)
        {
            IsLoading = true;
            passengerRepository.OnRepositoryChanged += OnSearchPassengers;
            passengerRepository.SearchPassengers(keyword);
        }

        private void LoadPassengers()
        {
            IsLoading = true;

            if (failMode)
            {
                passengerRepository.OnRepositoryChanged += OnLoadFailedPassengers;
                passengerRepository.LoadFailedPassengers();
            }
            else
            {
                passengerRepository.OnRepositoryChanged += OnLoadLastHourPassengers;
                passengerRepository.LoadLastHourPassengers();
            }
        }

        private void OnSearchPassengers(object repository)
        {
            passengerRepository.OnRepositoryChanged -= OnSearchPassengers;

            Device.BeginInvokeOnMainThread(() =>
            {
                Passengers.Clear();

                if (repository != null)
                    foreach (var passenger in ((IEnumerable<Passenger>)repository))
                        Passengers.Add(new PassengerBindable(passenger, false));

                IsLoading = false;
            });
        }

        private void OnLoadLastHourPassengers(object repository)
        {
            passengerRepository.OnRepositoryChanged -= OnLoadLastHourPassengers;

            Device.BeginInvokeOnMainThread(() =>
            {
                Passengers.Clear();

                if (repository != null)
                {
                    var passengers = PassengersRepository.GroupPassengers((IEnumerable<Passenger>)repository);

                    foreach (var passenger in passengers)
                    {
                        Passengers.Add(new PassengerBindable(passenger, false));
                    }
                }

                IsLoading = false;
            });
        }

        private void OnLoadFailedPassengers(object repository)
        {
            passengerRepository.OnRepositoryChanged -= OnLoadFailedPassengers;

            Device.BeginInvokeOnMainThread(() =>
            {
                Passengers.Clear();

                if (repository != null)
                {
                    foreach (var passenger in ((IEnumerable<Passenger>)repository))
                    {
                        Passengers.Add(new PassengerBindable(passenger, true));
                    }
                }

                IsLoading = false;
            });
        }

        private string _title = "My Profile";
        public string Title
        {
            set
            {
                _title = value;
                NotifyPropertyChange("Title");
            }
            get
            {
                return _title;
            }
        }
    }
}
