﻿using AIMS;
using AIMS.Models;
using Ieg.Mobile.MobileLounge2.Core.Common.BindableModels;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Windows.Input;
using Xamarin.Forms;

namespace Ieg.Mobile.MobileLounge2.Core.ViewModels
{
    public class MessagesViewModel : ViewModelBase
    {
        private ObservableCollection<MessageBindable> _messages = new ObservableCollection<MessageBindable>();

        private MessageRepository messagesRepository = new MessageRepository();

        public ObservableCollection<MessageBindable> Messages
        {
            get { return _messages; }
            set
            {
                _messages = value;
                NotifyPropertyChange("Messages");
            }
        }

        public string Title
        {
            get
            {
                return App.TextProvider.GetText(2041);
            }
        }


        public ICommand ShowMessageCommand { get; private set; }

        public MessagesViewModel()
        {
            ShowMessageCommand = new Command((model) =>
            {
                if (model != null && model is MessageBindable msg)
                {
                    MessageTitle = msg.Title;
                    MessageError = msg.Body;

                    ShowMessageError = true;
                    Navigation.RootMasterDetailPage.ShowOverlayPage();
                }
            });


            LoadMessages();
        }

        private void LoadMessages()
        {
            messagesRepository.OnRepositoryChanged += OnLoadMessages;

            messagesRepository.LoadAllMessages();
        }

        private void OnLoadMessages(object repository)
        {
            messagesRepository.OnRepositoryChanged -= OnLoadMessages;

            Device.BeginInvokeOnMainThread(() =>
            {
                if (repository == null)
                    return;

                else if (repository is IEnumerable<Message>)
                {
                    foreach (var item in ((IEnumerable<Message>)repository))
                    {
                        Messages.Add(new MessageBindable(item));
                    }

                    #region Mock Message
#if DEBUG
                    Messages.Add(new MessageBindable(new Message
                    {
                        Time = DateTime.Now,
                        Title = "Today's Long and biiiiiiiiiiiig titled Announcement",
                        Body = "Last descriptions, and more texts and more texts and more texts and more texts and more texts and more texts and more texts and more texts and more texts and more texts"
                    }));

                    Messages.Add(new MessageBindable(new Message
                    {
                        Time = DateTime.Now.AddMinutes(-30),
                        Title = "Today's Announcement",
                        Body = "Again descriptions, and more texts and more texts and more texts and more texts and more texts and more texts and more texts and more texts and more texts and more texts"
                    }));

                    Messages.Add(new MessageBindable(new Message
                    {
                        Time = DateTime.Now.AddDays(-1),
                        Title = "Yesterday's Announcement",
                        Body = "Descriptions, and more texts and more texts and more texts and more texts and more texts and more texts and more texts and more texts and more texts and more texts"
                    }));
#endif
                }
                #endregion


            });
        }
    }
}

