﻿using AIMS;
using Ieg.Mobile.MobileLounge2.Core.Common;
using Ieg.Mobile.MobileLounge2.Core.Common.Models;
using System;
using System.Collections.Generic;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;
using System.Linq;


namespace Ieg.Mobile.MobileLounge2.Core.ViewModels
{
    public class LoginViewModel : ViewModelBase
    {
        public ICommand LoginCommand { get; private set; }
        public ICommand ResetCommand { get; private set; }

        public event VoidEventHandler OnLoggedIn = null;

        //public event VoidEventHandler OnLoggedFail = null;

        public LoginViewModel()
        {
            IsLoading = true;
            Message = "Detecting your position. Please wait...";

            ((App)App.Current).OnLocationDetected += OnLocationDetected;

            LoginCommand = new Command(
                () =>
                {
                    IsLoading = true;
                    Message = string.Empty;
                    //Message = "Logging in..";
                    Task.Run(() => {
                        try
                        {
                            MembershipProvider.CreateMembershipProvider(Username, Password, GetProxy());
                            //HideLoading();
                            //ShowLoading(App.TextProvider.GetText(2511)/*"Authenticating"*/);
                            MembershipProvider.Current.Login(Username, Password, Current_OnLogin);
                        }
                        catch (Exception ex)
                        {
                            Message = ex.Message;
                            IsLoading = false;
                        }
                    });
                    //IsLoading = false;
                },
                () => { return !string.IsNullOrWhiteSpace(Username) && !string.IsNullOrWhiteSpace(Password) && !IsLoading; });

            ResetCommand = new Command(
                () =>
                {
                    Message = Username = Password = string.Empty;
                },
                () => { return !string.IsNullOrWhiteSpace(Username) && !string.IsNullOrWhiteSpace(Password) && !IsLoading; });

            PropertyChanged += OnPropertyChanged;

            if (App.AIMSApplication.Position != null)
            {
                OnLocationDetected();
            }
        }

        private void OnLocationDetected()
        {
            ((App)App.Current).OnLocationDetected -= OnLocationDetected;
            IsLoading = false;
            Message = string.Empty;
        }

        internal void Init()
        {
            Message = string.Empty;
            PreloadCredentials();
        }

        private IWebProxy GetProxy()
        {
            HttpWebRequest myWebRequest = (HttpWebRequest)WebRequest.Create("http://www.microsoft.com");
            IWebProxy proxy = myWebRequest.Proxy;
            return proxy;
        }

        private void Current_OnLogin(object sender, Tuple<bool, ErrorCode> e)
        {
            if (e.Item1)
            {
                LogsRepository.AddClientInfo("User logged in", MembershipProvider.Current.LoggedinUser.ToString());
                if (MembershipProvider.Current.UserLounges.Count() > 0)
                {
                    try
                    {
                        //App.TriggerApplicationStart();//TODO: Make it automatic

                        Message = "";
                        Username = Password = string.Empty;
                        IsLoading = false;

                        if (MembershipProvider.Current.SelectedLounge == null)
                            Device.BeginInvokeOnMainThread(() =>
                            {
                                OnLoggedIn?.Invoke();
                            });
                        else//go to airlines activity
                        {
                            LogsRepository.AddClientInfo("Lounge selected", string.Format("The user {0} selected the lounge", MembershipProvider.Current.LoggedinUser.Username));
                            LogsRepository.AddInfo("Lounge selected", string.Format("Lounge detail: {0} | WSID: {1}", MembershipProvider.Current.SelectedLounge.LoungeName, MembershipProvider.Current.SelectedLounge.WorkstationID));
                            //StartActivity(new Intent(this, typeof(MainActivity)));

                            Device.BeginInvokeOnMainThread(() =>
                            {
                                OnLoggedIn?.Invoke();
                            });
                        }
                    }
                    catch (Exception ex)
                    {
                        Message = ex.Message;
                        IsLoading = false;
                        LogsRepository.AddError("Error in staring activities", ex);

                        //Device.BeginInvokeOnMainThread(() =>
                        //{
                        //    OnLoggedFail?.Invoke();
                        //});
                    }
                }
                else
                {
                    LogsRepository.AddClientWarning("No lounge is available", string.Format("There were no lounge for the user {0}", MembershipProvider.Current.LoggedinUser.Username));
                    //AIMSMessage(App.TextProvider.GetText(2049), App.TextProvider.GetText(1005));
                    Message = App.TextProvider.GetText(1005);
                    IsLoading = false;
                    //RunOnUiThread(() =>
                    //{
                    //    Toast.MakeText(this, App.TextProvider.GetText(1005)/*"No lounges are assigned to this user"*/, ToastLength.Short).Show();
                    //});

                    //Device.BeginInvokeOnMainThread(() =>
                    //{
                    //    OnLoggedFail?.Invoke();
                    //});
                }
            }
            else
            {
                string errorMessage = App.TextProvider.GetText((int)e.Item2);
                LogsRepository.AddError("User login failed", string.Format("A user with username '{0}' failed to login into AIMS,\nError Code: {1}\nError Message: {2}", Username, e.Item2, errorMessage));
                //AIMSMessage(App.TextProvider.GetText(2047), errorMessage);
                //Message = string.Format("A user with username '{0}' failed to login into AIMS,\nError Code: {1}\nError Message: {2}", Username, e.Item2, errorMessage);
                Message = errorMessage;
                IsLoading = false;

                //Device.BeginInvokeOnMainThread(() =>
                //{
                //    OnLoggedFail?.Invoke();
                //});
            }
            //HideLoading();
        }

        public void PreloadCredentials()
        {
#if DEBUG
            Username = "mobile";
            Password = "Password001";
#endif
        }

        private void OnPropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (LoginCommand != null)
                ((Command)LoginCommand).ChangeCanExecute();
            if (ResetCommand != null)
                ((Command)ResetCommand).ChangeCanExecute();
        }

        private string _username = string.Empty;
        public string Username
        {
            get { return _username; }
            set
            {
                _username = value;
                NotifyPropertyChange("Username");
            }
        }

        private string _password = string.Empty;
        public string Password
        {
            get { return _password; }
            set
            {
                _password = value;
                NotifyPropertyChange("Password");
            }
        }
    }
}
