﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using Xamarin.Forms;
using Xamarin.Forms.RadialMenu.Models;

namespace Ieg.Mobile.MobileLounge2.Core.ViewModels
{
    public class RootMasterDetailViewModel : ViewModelBase
    {
        public RootMasterDetailViewModel()
        {
            NotifyPropertyChange("BackgroundImage");
        }

        public string BackgroundImage
        {
            get
            {
                return Device.RuntimePlatform == Device.UWP ? "Images/backgroundImage.png" :  "backgroundImage.png";
            }
        }

        private ObservableCollection<RadialMenuItem> _menuItems;
        public ObservableCollection<RadialMenuItem> MenuItems
        {
            set
            {
                _menuItems = value;
                NotifyPropertyChange("MenuItems");
            }
            get
            {
                return _menuItems;
            }
        }

    }
}
