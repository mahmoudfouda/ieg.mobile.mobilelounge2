﻿using AIMS;
using Ieg.Mobile.MobileLounge2.Core.Common.BindableModels;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.Windows.Input;
using Xamarin.Forms;

namespace Ieg.Mobile.MobileLounge2.Core.ViewModels
{
    public class LogsViewModel : ViewModelBase
    {
        private ObservableCollection<LogBindable> _logs = new ObservableCollection<LogBindable>();

        private LogsRepository logsRepository = new LogsRepository();

        public ObservableCollection<LogBindable> Logs
        {
            get { return _logs; }
            set
            {
                _logs = value;
                NotifyPropertyChange("Logs");
            }
        }

        public string Title
        {
            get
            {
                return App.TextProvider.GetText(2040);
            }
        }

        public ICommand ShowLogCommand { get; private set; }

        public LogsViewModel()
        {
            ShowLogCommand = new Command((model) =>
            {
                if(model != null && model is LogBindable log)
                {
                    MessageTitle = log.Title;
                    MessageError = log.Description;

                    ShowMessageError = true;
                    Navigation.RootMasterDetailPage.ShowOverlayPage();
                }
            });

            LoadLogs();
        }

        private void LoadLogs()
        {
            logsRepository.OnRepositoryChanged += OnLoadLogs;

            logsRepository.LoadAllLogs();
        }

        private void OnLoadLogs(object repository)
        {
            Device.BeginInvokeOnMainThread(() =>
            {
                if (repository == null)
                    return;

                foreach (var item in ((List<AIMS.DAL.Log>)repository))
                {
                    Logs.Add(new LogBindable(item));
                }
            });
        }
    }
}
