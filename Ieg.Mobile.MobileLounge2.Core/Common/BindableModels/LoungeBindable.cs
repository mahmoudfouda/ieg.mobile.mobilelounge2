﻿using Ieg.Mobile.MobileLounge2.Core.Common.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Ieg.Mobile.MobileLounge2.Core.Common.BindableModels
{
    public class LoungeBindable : ModelBaseBindable
    {
        private LoungeMenuItem _loungeMenuItem;

        public LoungeBindable(LoungeMenuItem loungeMenuItem)
        {
            if (loungeMenuItem == null)
                throw new ArgumentNullException("The parameter loungeMenuItem is null");

            _loungeMenuItem = loungeMenuItem;
        }

        public LoungeMenuItem LoungeMenuItem
        {
            get
            {
                return _loungeMenuItem;
            }
        }

        public string Name
        {
            get
            {
                return _loungeMenuItem.Name;
            }
        }

        public string LoungeName
        {
            get
            {
                return _loungeMenuItem.LoungeName;
            }
        }
        public int OccupanyPercentage
        {
            get
            {
                return _loungeMenuItem.OccupanyPercentage;
            }
        }

        private bool isSelected;
        public bool IsSelected
        {
            get
            {
                return isSelected;
            }
            set
            {
                isSelected = value;
                OnPropertyChanged("IsSelected");
            }
        }
    }
}
