﻿using AIMS.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Ieg.Mobile.MobileLounge2.Core.Common.BindableModels
{
    public class AirportBindable : ModelBaseBindable
    {
        private Airport _airport;

        public AirportBindable(Airport airport)
        {
            if (airport == null)
                throw new ArgumentNullException("The parameter airport is null");

            _airport = airport;
        }

        public string Name
        {
            get
            {
                return _airport.Name;
            }
        }

        public string IATA_Code
        {
            get
            {
                return _airport.IATA_Code;
            }
        }

        public string Continent
        {
            get
            {
                return _airport.Continent;
            }
        }

        public string Country
        {
            get
            {
                return _airport.Country;
            }
        }

        public string Municipality
        {
            get
            {
                return _airport.Municipality;
            }
        }
        
        public double Latitude
        {
            get
            {
                return _airport.Latitude;
            }
        }

        public double Longitude
        {
            get
            {
                return _airport.Longitude;
            }
        }

        public Airport AimsAirport
        {
            get
            {
                return _airport;
            }
        }

    }
}
