﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace Ieg.Mobile.MobileLounge2.Core.Common.BindableModels
{
    public class PaxBindable :  ModelBaseBindable
    {
        private ImageSource _imageAirline;
        public ImageSource ImageAirline
        {
            get
            {
                return _imageAirline;
            }
            set
            {
                _imageAirline = value;
                OnPropertyChanged("ImageAirline");
            }
        }

        private ImageSource _imageCard;
        public ImageSource ImageCard
        {
            get
            {
                return _imageCard;
            }
            set
            {
                _imageCard = value;
                OnPropertyChanged("ImageCard");
            }
        }
    }
}
