﻿using Ieg.Mobile.MobileLounge2.Core.Library;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace Ieg.Mobile.MobileLounge2.Core.Common.BindableModels
{
    public class PageBindable : ModelBaseBindable
    {

        private string _name;
        public string Name
        {
            get
            {
                return _name;
            }
            set
            {
                _name = value;
                OnPropertyChanged("Name");
            }
        }

        public string ArrowIcon
        {
            get
            {
                switch (BreadCrumberType)
                {
                    case BreadCrumberTypes.Common:
                        return Device.RuntimePlatform == Device.UWP ? "Images/normal2_Short.png" :  "normal2_Short.png";
                    case BreadCrumberTypes.BeforeLast:
                        return Device.RuntimePlatform == Device.UWP ? "Images/normalBeforeLast_short.png" : "normalBeforeLast_short.png";
                    case BreadCrumberTypes.Last:
                        return Device.RuntimePlatform == Device.UWP ? "Images/last_Short.png" : "last_Short.png";
                    default:
                        return Device.RuntimePlatform == Device.UWP ? "Images/normal2_Short.png" : "normal2_Short.png";
                }
            }
        }

        public Color BackGroundColor
        {
            get
            {
                switch (BreadCrumberType)
                {
                    case BreadCrumberTypes.Common:
                    case BreadCrumberTypes.BeforeLast:
                        return (Color)App.Current.Resources["BreadCrumbGray"];
                    case BreadCrumberTypes.Last:
                    default:
                        return (Color)App.Current.Resources["BreadCrumbBlue"];
                }
            }
        }

        public Color RightBackGroundColor
        {
            get
            {
                switch (BreadCrumberType)
                {
                    case BreadCrumberTypes.Common:
                        return (Color)App.Current.Resources["BreadCrumbGray"];
                    case BreadCrumberTypes.BeforeLast:
                        return (Color)App.Current.Resources["BreadCrumbBlue"];
                    case BreadCrumberTypes.Last:
                    default:
                        return Color.Transparent;
                }
            }
        }

        private BreadCrumberTypes _breadCrumberType;
        public BreadCrumberTypes BreadCrumberType
        {
            get
            {
                return _breadCrumberType;
            }
            set
            {
                _breadCrumberType = value;
                OnPropertyChanged("BreadCrumberType");
                OnPropertyChanged("BackGroundColor");
                OnPropertyChanged("RightBackGroundColor");
                OnPropertyChanged("ArrowIcon"); 
            }
        }

        public ICommand SelectPageCommand { get; private set; }
        public int Position { get; internal set; }

        public PageBindable()
        {
            SelectPageCommand = new Command(async () =>
            {
                try
                {
                    if (BreadCrumberType != BreadCrumberTypes.Last)
                    {
                        while (NavigatorService.Current.History.Count > Position)
                        {
                            await NavigatorService.Current.Pop();
                            break; //TODO: it needs to Pop all pages 
                        }


                        //for (int i = NavigatorService.Current.History.Count; i > Position; i--)
                        //{
                        //    await NavigatorService.Current.Pop();
                        //    await Task.Delay(500);
                        //}
                    }
                }
                catch(Exception ef)
                {
#if DEBUG
                    Debug.WriteLine("Closing pages {0}", ef.Message);
#endif
                    //TODO: Log
                }

            });
        }
    }
}
