﻿using AIMS.Models;
using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace Ieg.Mobile.MobileLounge2.Core.Common.BindableModels
{
    public class PassengerBindable : ModelBaseBindable
    {
        private Passenger _passenger;
        private bool _fail;

        private string failImg = Device.RuntimePlatform == Device.UWP ? "Images/passenger_red.png" : "passenger_red.png";
        private string normalImg = Device.RuntimePlatform == Device.UWP ? "Images/passenger_blue.png" : "passenger_blue.png";
        private string guestImg = Device.RuntimePlatform == Device.UWP ? "Images/passenger_guest.png" : "passenger_guest.png";

        private string _airportTo;
        private string _fromAirportName;
        private string _toAirportName;
        private string _airportFrom;
        private ImageSource _otherCard;
        private string _boardingTime;
        private string _terminal;
        private string _gate;
        private string _time;


        public PassengerBindable(Passenger passenger, bool? isFailed = null)
        {
            if (passenger == null)
                throw new ArgumentNullException("The parameter passenger is null");

            _passenger = passenger;

            if (string.IsNullOrEmpty(passenger.FailedReason))
                this._fail = isFailed.HasValue ? isFailed.Value : false;
            else
                this._fail = isFailed.HasValue ? isFailed.Value : true;
        }


        public string FullName
        {
            get
            {
                if (!string.IsNullOrEmpty(_passenger.FailedReason))
                {
                    if (_passenger.FailedReason.Contains("\n"))
                        return _passenger.FailedReason.Substring(0, _passenger.FailedReason.IndexOf("\n"));
                    else
                        return _passenger.FailedReason;
                }
                return _passenger.FullName;
            }
            set
            {
                _passenger.FullName = value;
                OnPropertyChanged("FullName");
            }
        }

        public string MainPaxName
        {
            get
            {
                return string.IsNullOrWhiteSpace(_passenger.HostName) ? "Main PAX" : _passenger.HostName;
            }
        }

        public bool IsGuest
        {
            get
            {
                return _passenger.IsGuest;
            }
        }

        public string TrackingTimestamp
        {
            get
            {
                return _passenger.TrackingTimestamp.ToShortTimeString();
            }
        }

        public string Image
        {
            get
            {
                if (_fail)
                    return failImg;
                else if (_passenger.IsGuest)
                    return guestImg;
                else
                    return normalImg;
            }
        }

        public string FFN
        {
            get
            {
                return _passenger.FFN;
            }
            set
            {
                _passenger.FFN = value;
                OnPropertyChanged("FFN");
            }
        }

        public string Notes
        {
            get
            {
                return _passenger.Notes;
            }
            set
            {
                _passenger.Notes = value;
                OnPropertyChanged("Notes");
            }
        }

        public string FlightCarrier
        {
            get
            {
                return _passenger.FlightCarrier;
            }
            set
            {
                _passenger.FlightCarrier = value;
                OnPropertyChanged("FlightCarrier");
            }
        }

        public string FlightNumber
        {
            get
            {
                return _passenger.FlightNumber;
            }
            set
            {
                _passenger.FlightNumber = value;
                OnPropertyChanged("FlightNumber");
            }
        }

        public string TrackingClassOfService
        {
            get
            {
                return _passenger.TrackingClassOfService;
            }
            set
            {
                _passenger.TrackingClassOfService = value;
                OnPropertyChanged("TrackingClassOfService");
            }
        }

        public string FromAirport
        {
            get
            {
                return _passenger.FromAirport;
            }
            set
            {
                _passenger.FromAirport = value;
                OnPropertyChanged("FromAirport");
            }
        }

        public string ToAirport
        {
            get
            {
                return _passenger.ToAirport;
            }
            set
            {
                _passenger.ToAirport = value;
                OnPropertyChanged("ToAirport");
            }
        }

        public string SeatNumber
        {
            get
            {
                return _passenger.SeatNumber;
            }
            set
            {
                _passenger.SeatNumber = value;
                OnPropertyChanged("SeatNumber");
            }
        }

        public string PNR
        {
            get
            {
                return _passenger.PNR;
            }
            set
            {
                _passenger.PNR = value;
                OnPropertyChanged("PNR");
            }
        }

        public string TrackingPassCombinedFlightNumber
        {
            get
            {
                return _passenger.TrackingPassCombinedFlightNumber;
            }
            set
            {
                _passenger.TrackingPassCombinedFlightNumber = value;
                OnPropertyChanged("TrackingPassCombinedFlightNumber");
            }
        }

        public Passenger Passenger
        {
            get
            {
                return _passenger;
            }
            set
            {
                _passenger = value;
            }
        }

        private ImageSource _imageAirline;
        public ImageSource ImageAirline
        {
            get
            {
                return _imageAirline;
            }
            set
            {
                _imageAirline = value;
                OnPropertyChanged("ImageAirline");
            }
        }

        private ImageSource _imageCard;
        public ImageSource ImageCard
        {
            get
            {
                return _imageCard;
            }
            set
            {
                _imageCard = value;
                OnPropertyChanged("ImageCard");
            }
        }

        private AirportBindable _selectedAirportFrom;

        private AirportBindable _selectedAirportTo;
        private string _seat;

        public AirportBindable SelectedAirportFrom
        {
            get { return _selectedAirportFrom; }
            set
            {
                if (value == null)
                    return;

                _selectedAirportFrom = value;
                OnPropertyChanged("SelectedAirportFrom");
                OnPropertyChanged("FromAirportName");
                _airportFrom = value.IATA_Code;
                FromAirportName = value.Name;
                OnPropertyChanged("AirportFrom");
            }
        }

        public AirportBindable SelectedAirportTo
        {
            get { return _selectedAirportTo; }
            set
            {
                if (value == null)
                    return;

                _selectedAirportTo = value;
                OnPropertyChanged("SelectedAirportTo");
                OnPropertyChanged("ToAirportName");
                _airportTo = value.IATA_Code;
                ToAirportName = value.Name;
                OnPropertyChanged("AirportTo");
            }
        }

        public string AirportFrom
        {
            get { return _airportFrom; }
            set
            {
                if (_airportFrom == value)
                    return;

                _airportFrom = value;
                OnPropertyChanged("AirportFrom");
            }
        }

        public string AirportTo
        {
            get { return _airportTo; }
            set
            {
                if (_airportTo == value)
                    return;

                _airportTo = value;

                OnPropertyChanged("AirportTo");
            }
        }

        public string FromAirportName
        {
            get
            {
                //if (_selectedAirportFrom == null)
                //    return string.Empty;

                //return _selectedAirportFrom.Name;

                return _fromAirportName;
            }
            set
            {
                _fromAirportName = value;
                OnPropertyChanged("FromAirportName");
            }
        }

        public string ToAirportName
        {
            get
            {
                //if (_selectedAirportTo == null)
                //    return string.Empty;

                //return _selectedAirportTo.Name;

                return _toAirportName;
            }
            set
            {
                _toAirportName = value;
                OnPropertyChanged("ToAirportName");
            }
        }

        public ImageSource OtherCard
        {
            get
            {
                return _otherCard;
            }
            set
            {
                _otherCard = value;
                OnPropertyChanged("OtherCard");
            }
        }

        public string BoardingTime
        {
            get
            {
                return _boardingTime;
            }
            set
            {
                _boardingTime = value;
                OnPropertyChanged("BoardingTime");
            }
        }

        public string Terminal
        {
            get
            {
                return _terminal;
            }
            set
            {
                _terminal = value;
                OnPropertyChanged("Terminal");
            }
        }

        public string Gate
        {
            get
            {
                return _gate;
            }
            set
            {
                _gate = value;
                OnPropertyChanged("Gate");
            }
        }

        public string Time
        {
            get
            {
                return _time;
            }
            set
            {
                _time = value;
                OnPropertyChanged("Time");
            }
        }

        public string Seat
        {
            get
            {
                return _seat;
            }
            set
            {
                _seat = value;
                OnPropertyChanged("Seat");
            }
        }

        public bool Fail
        {
            get
            {
                return _fail;
            }
        }
    }
}
