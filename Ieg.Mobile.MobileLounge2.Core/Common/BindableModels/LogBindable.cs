﻿using AIMS.DAL;
using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace Ieg.Mobile.MobileLounge2.Core.Common.BindableModels
{
    public class LogBindable : ModelBaseBindable
    {
        private Log _log;

        public LogBindable(Log log)
        {
            if (log == null)
                throw new ArgumentNullException("The parameter log is null");

            _log = log;
        }


        public string Title
        {
            get
            {
                return _log.Title;
            }
        }

        public string Description
        {
            get
            {
                return _log.Description;
            }
        }

        public DateTime Time
        {
            get
            {
                return _log.Time;
            }
        }

        public DateTime LocalTime
        {
            get
            {
                return _log.LocalTime;
            }
        }

        public string Image
        {
            get
            {
                string pathImg = Device.RuntimePlatform == Device.UWP ? "Images/" : string.Empty;

                pathImg += _log.LogLevel == (int)AIMS.LogLevel.Developer ? "profilelogdev.png" : "profilelogclient.png";

                return pathImg;
            }
        }
    }
}
