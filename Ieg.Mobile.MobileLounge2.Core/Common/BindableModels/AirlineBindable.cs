﻿using Ieg.Mobile.MobileLounge2.Core.Common.Models;
using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace Ieg.Mobile.MobileLounge2.Core.Common.BindableModels
{
    public class AirlineBindable : ModelBaseBindable
    {
        private Airline _airline;

        public AirlineBindable(Airline airline)
        {
            if (airline == null)
                throw new ArgumentNullException("The parameter airline is null");

            _airline = airline;
        }

        public string Name
        {
            get
            {
                return _airline.Name;
            }
        }

        public string ImageUrl
        {
            get
            {
                return _airline.ImageUrl;
            }
        }

        public ImageSource Image
        {
            get
            {
                return _airline.Image;
            }
            set
            {
                _airline.Image = value;
                OnPropertyChanged("Image");
            }
        }

        private int numberOfPassenger = 100;
        public int NumberOfPassenger
        {
            get
            {
#if DEBUG
                return new Random().Next(-100, 100);
#endif

                return numberOfPassenger;
            }
            set
            {
                numberOfPassenger = value;
                OnPropertyChanged("NumberOfPassenger");
            }
        }

        public Airline Airline
        {
            get
            {
                return _airline;
            }
        }

    }
}
