﻿using System;
using System.Collections.Generic;
using System.Text;
using AIMS.Models;
using Xamarin.Forms;

namespace Ieg.Mobile.MobileLounge2.Core.Common.BindableModels
{
    public class CardBindable : ModelBaseBindable
    {
        private Card _card;

        public CardBindable(Card card)
        {
            if (card == null)
                throw new ArgumentNullException("The parameter card is null");

            this._card = card;
        }

        public Card AimsCard
        {
            get
            {
                return _card;
            }
        }

        public string Name
        {
            get
            {
                return _card.Name;
            }
        }

        public string ImageUrl
        {
            get
            {
                return _card.ImageHandle;
            }
        }

        private ImageSource _image;
        public ImageSource Image
        {
            get
            {
                return _image;
            }
            set
            {
                _image = value;
                OnPropertyChanged("Image");
            }
        }
    }
}
