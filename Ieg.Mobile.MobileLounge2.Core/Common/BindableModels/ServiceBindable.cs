﻿using AIMS.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Text;
using Xamarin.Forms;

namespace Ieg.Mobile.MobileLounge2.Core.Common.BindableModels
{
    public class ServiceBindable : ObservableCollection<Passenger>, INotifyPropertyChanged
    {
        private ImageSource _icon;
        private string _name;
        private PassengerService _passengerService;

        public ServiceBindable(PassengerService passengerService)
        {
            if (passengerService == null)
                throw new ArgumentNullException("The parameter passengerService is null");

            _passengerService = passengerService;
        }

        public string DisplayText
        {
            get
            {
                return _passengerService.DisplayText;
            }
        }


        public ImageSource Icon
        {
            get
            {
                return _icon;
            }
            set
            {
                _icon = value;
                OnPropertyChanged(new PropertyChangedEventArgs("ImageCard"));
            }
        }

    }
}
