﻿using AIMS.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Ieg.Mobile.MobileLounge2.Core.Common.BindableModels
{
    public class MessageBindable : ModelBaseBindable
    {
        private Message _message;

        public MessageBindable(Message message)
        {
            if (message == null)
                throw new ArgumentNullException("The parameter message is null");

            _message = message;
        }

        public string Title
        {
            get
            {
                return _message.Title;
            }
        }

        public string Body
        {
            get
            {
                return _message.Body;
            }
        }

        public DateTime Time
        {
            get
            {
                return _message.Time;
            }
        }
    }
}
