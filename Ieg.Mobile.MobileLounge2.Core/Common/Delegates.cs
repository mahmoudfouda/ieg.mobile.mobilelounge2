﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ieg.Mobile.MobileLounge2.Core.Common
{
    public delegate void VoidEventHandler();
    public delegate void LogoutEventHandler(string reason);
    public delegate void ConnectionChangeEventHandler(bool isConnected);
    public delegate void ActionEventHandler(Action action);
}
