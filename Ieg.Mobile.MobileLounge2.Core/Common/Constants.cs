﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ieg.Mobile.MobileLounge2.Core.Common
{
    public static class Constants
    {
        public const string MenuOpened_Message = "MenuOpened";
        public const string MenuOpening_Message = "MenuOpening";
        public const string MenuClosed_Message = "MenuClosed";
        public const string MenuClosing_Message = "MenuClosing";
    }
}
