﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ieg.Mobile.MobileLounge2.Core.Common
{
    public static class Util
    {
        public static string RemoveLeadingZeros(this string number)
        {
            if (string.IsNullOrEmpty(number))
                return string.Empty;
            return number.Trim().TrimStart('0');
        }

        public static string ToLongString(this Exception ex)
        {
            if (ex == null) return string.Empty;

            var sb = new StringBuilder();
            sb.Append("Exception: ");
            sb.Append(ex.Message);
            Exception temp = ex.InnerException;
            while (temp != null)
            {
                sb.AppendLine();
                sb.Append(temp.Message);
                temp = temp.InnerException;
            }
            sb.AppendLine();
            sb.Append("Stack Trace: ");
            sb.Append(ex.StackTrace);

            return sb.ToString();
        }

        private static Random rand = new Random();
        public static int GetWaitRandomMilliseconds()
        {
            return rand.Next(300, 1000);
        }
    }
}
