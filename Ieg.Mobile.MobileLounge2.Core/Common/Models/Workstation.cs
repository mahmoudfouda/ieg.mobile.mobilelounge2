﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ieg.Mobile.MobileLounge2.Core.Common.Models
{
    public class Workstation : TileItem
    {
        public Workstation()
        {

        }

        public Workstation(AIMS.Models.Workstation workstation)
        {
            AimsWorkstation = workstation;
            Descriptions = string.Format("{0} | {1}", workstation.AirportCode, workstation.LoungeName);
            LoungeName = workstation.LoungeName;
            LoungeId = workstation.LoungeID;
            Name = workstation.WorkstationName;
            Id = workstation.WorkstationID;
        }
        public decimal Id { get; set; }

        public decimal LoungeId { get; set; }
        
        public string LoungeName { get; set; }

        public float Occupancy { get; set; }

        public AIMS.Models.Workstation AimsWorkstation { get; protected set; }
    }
}
