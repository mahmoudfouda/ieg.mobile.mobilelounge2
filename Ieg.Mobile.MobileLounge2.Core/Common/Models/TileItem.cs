﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ieg.Mobile.MobileLounge2.Core.Common.Models
{
    public class TileItem
    {
        public string Name { get; set; }

        public string ImageUrl { get; set; }

        public string Descriptions { get; set; }
    }
}
