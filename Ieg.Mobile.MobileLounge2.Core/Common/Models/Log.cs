﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ieg.Mobile.MobileLounge2.Core.Common.Models
{
    public class Log
    {
        public Log()
        {
            //Id = Guid.NewGuid();
            UTCTime = DateTime.UtcNow;
            LocalTime = DateTimeOffset.Now;
        }

        //[PrimaryKey]
        public Guid Id { get; set; }

        public DateTime UTCTime
        {
            get;
            set;
        }

        public DateTimeOffset LocalTime
        {
            get;
            set;
        }

        //[MaxLength(50)]
        public string Username { get; set; }

        //[MaxLength(200)]
        public string DeviceId { get; set; }

        //[MaxLength(128)]
        public string Title { get; set; }

        //[MaxLength(128)]
        public string Section { get; set; }

        public LogTypes LogType { get; set; }

        public int LogLevel { get; set; }

        public bool IsShortLog { get; set; }

        #region Position
        public double Latitude { get; set; }

        public double Longitude { get; set; }

        public double Altitude { get; set; }

        public float Accuracy { get; set; }
        #endregion

        public string Description { get; set; }

        #region Convertors
        public override string ToString()
        {
            if (IsShortLog)
                return string.Format("LT:{0:MM/dd/yyyy HH:mm:ss fff}\tUT:{1:MM/dd/yyyy HH:mm:ss fff}\t[{2}\t ({3})]\t- [Level:{4}\tType:{5}\tUser:{6}]\tD:{7}\t#ENDOFLOG#\r\n",
                LocalTime.ToLocalTime(), UTCTime, Title, Section, LogLevel, (int)LogType, Username, Description);
            return string.Format("LocalTime:{0:MM/dd/yyyy HH:mm:ss fff}\tUtcTime:{1:MM/dd/yyyy HH:mm:ss fff}\tLogTitle:{2}\t(AppSection:{3})\t\t\t- [LogLevel:{4}\tLogType:{5}\tUser:{6}\tDevice:{7}\tPosition:{8}, {9}\t({10})]\t\t\n----------\n{11}\n----------\n#ENDOFLOG#\r\n",
                LocalTime.ToLocalTime(), UTCTime, Title, Section, LogLevel, (int)LogType, Username, DeviceId, Latitude, Longitude, Accuracy, Description);
        }

        public static Log Parse(string text)
        {
            if (string.IsNullOrWhiteSpace(text))
                return null;

            int index, endIndex;
            var res = new Log();

            if (text.StartsWith("LocalTime:"))
            {
                index = text.IndexOf("\t");
                if (index == -1) return null;
                res.LocalTime = DateTime.ParseExact(text.Substring(10, index - 10), "MM/dd/yyyy HH:mm:ss fff", null);

                if (text.Contains("\tUtcTime:"))
                {
                    index = text.IndexOf("\tUtcTime:") + 9;
                    endIndex = text.IndexOf("\t", index);
                    if (endIndex == -1)
                        res.UTCTime = DateTime.ParseExact(text.Substring(index), "MM/dd/yyyy HH:mm:ss fff", null);
                    else
                        res.UTCTime = DateTime.ParseExact(text.Substring(index, endIndex - index), "MM/dd/yyyy HH:mm:ss fff", null);
                }

                if (text.Contains("\tLogTitle:"))
                {
                    index = text.IndexOf("\tLogTitle:") + 10;
                    endIndex = text.IndexOf("\t", index);
                    if (endIndex == -1)
                        res.Title = text.Substring(index);
                    else
                        res.Title = text.Substring(index, endIndex - index);
                }

                if (text.Contains("\t(AppSection:"))
                {
                    index = text.IndexOf("\t(AppSection:") + 13;
                    endIndex = text.IndexOf(")\t\t\t-", index);
                    if (endIndex == -1)
                        res.Section = text.Substring(index);
                    else
                        res.Section = text.Substring(index, endIndex - index);
                }

                if (text.Contains(" [LogLevel:"))
                {
                    index = text.IndexOf(" [LogLevel:") + 11;
                    endIndex = text.IndexOf("\t", index);
                    try
                    {
                        if (endIndex == -1)
                            res.LogLevel = int.Parse(text.Substring(index).Trim());
                        else
                            res.LogLevel = int.Parse(text.Substring(index, endIndex - index).Trim());
                    }
                    catch { }
                }

                if (text.Contains("\tLogType:"))
                {
                    index = text.IndexOf("\tLogType:") + 9;
                    endIndex = text.IndexOf("\t", index);
                    try
                    {
                        if (endIndex == -1)
                            res.LogType = (LogTypes)int.Parse(text.Substring(index).Trim());
                        else
                            res.LogType = (LogTypes)int.Parse(text.Substring(index, endIndex - index).Trim());
                    }
                    catch { }
                }

                if (text.Contains("\tUser:"))
                {
                    index = text.IndexOf("\tUser:") + 6;
                    endIndex = text.IndexOf("\t", index);
                    if (endIndex == -1)
                        res.Username = text.Substring(index);
                    else
                        res.Username = text.Substring(index, endIndex - index);
                }

                if (text.Contains("\tDevice:"))
                {
                    index = text.IndexOf("\tDevice:") + 8;
                    endIndex = text.IndexOf("\t", index);
                    if (endIndex == -1)
                        res.DeviceId = text.Substring(index);
                    else
                        res.DeviceId = text.Substring(index, endIndex - index);
                }

                if (text.Contains("\tPosition:"))
                {
                    index = text.IndexOf("\tPosition:") + 10;
                    endIndex = text.IndexOf(",", index);
                    if (endIndex > -1)
                        try
                        {
                            res.Latitude = double.Parse(text.Substring(index, endIndex - index).Trim());
                        }
                        catch { }

                    index = endIndex + 1;
                    endIndex = text.IndexOf("\t(", index);
                    if (endIndex > -1)
                        try
                        {
                            res.Longitude = double.Parse(text.Substring(index, endIndex - index).Trim());
                        }
                        catch { }

                    index = endIndex + 2;
                    endIndex = text.IndexOf(")]\t", index);
                    if (endIndex > -1)
                        try
                        {
                            res.Accuracy = float.Parse(text.Substring(index, endIndex - index).Trim());
                        }
                        catch { }
                }

                if (text.Contains(")]\t\t\n----------\n"))
                {
                    index = text.IndexOf(")]\t\t\n----------\n") + 16;
                    endIndex = text.IndexOf("\n----------\n#ENDOFLOG#\r\n", index);
                    if (endIndex == -1)
                        res.Description = text.Substring(index);
                    else
                        res.Description = text.Substring(index, endIndex - index);
                }
            }
            else if (text.StartsWith("LT:"))
            {
                index = text.IndexOf("\t");
                if (index == -1) return null;
                res.LocalTime = DateTime.ParseExact(text.Substring(3, index - 3), "MM/dd/yyyy HH:mm:ss fff", null);

                if (text.Contains("\tUT:"))
                {
                    index = text.IndexOf("\tUT:") + 4;
                    endIndex = text.IndexOf("\t", index);
                    if (endIndex == -1)
                        res.UTCTime = DateTime.ParseExact(text.Substring(index), "MM/dd/yyyy HH:mm:ss fff", null);
                    else
                        res.UTCTime = DateTime.ParseExact(text.Substring(index, endIndex - index), "MM/dd/yyyy HH:mm:ss fff", null);
                }

                if (text.Contains("\t["))
                {
                    index = text.IndexOf("\t[") + 2;
                    endIndex = text.IndexOf("\t (", index);
                    if (endIndex == -1)
                        res.Title = text.Substring(index);
                    else
                        res.Title = text.Substring(index, endIndex - index);
                }

                if (text.Contains("\t ("))
                {
                    index = text.IndexOf("\t (") + 3;
                    endIndex = text.IndexOf(")]\t- [Level:", index);
                    if (endIndex == -1)
                        res.Section = text.Substring(index);
                    else
                        res.Section = text.Substring(index, endIndex - index);
                }

                if (text.Contains(")]\t- [Level:"))
                {
                    index = text.IndexOf(")]\t- [Level:") + 12;
                    endIndex = text.IndexOf("\t", index);
                    try
                    {
                        if (endIndex == -1)
                            res.LogLevel = int.Parse(text.Substring(index).Trim());
                        else
                            res.LogLevel = int.Parse(text.Substring(index, endIndex - index).Trim());
                    }
                    catch { }
                }

                if (text.Contains("\tType:"))
                {
                    index = text.IndexOf("\tType:") + 6;
                    endIndex = text.IndexOf("\t", index);
                    try
                    {
                        if (endIndex == -1)
                            res.LogType = (LogTypes)int.Parse(text.Substring(index).Trim());
                        else
                            res.LogType = (LogTypes)int.Parse(text.Substring(index, endIndex - index).Trim());
                    }
                    catch { }
                }

                if (text.Contains("\tUser:"))
                {
                    index = text.IndexOf("\tUser:") + 6;
                    endIndex = text.IndexOf("]\tD:", index);
                    if (endIndex == -1)
                        res.Username = text.Substring(index);
                    else
                        res.Username = text.Substring(index, endIndex - index);
                }

                if (text.Contains("]\tD:"))
                {
                    index = text.IndexOf("]\tD:") + 4;
                    endIndex = text.IndexOf("\t#ENDOFLOG#\r\n", index);
                    if (endIndex == -1)
                        res.Description = text.Substring(index);
                    else
                        res.Description = text.Substring(index, endIndex - index);
                }
                res.IsShortLog = true;
            }
            else return null;

            return res;
        }
        #endregion
    }
}
