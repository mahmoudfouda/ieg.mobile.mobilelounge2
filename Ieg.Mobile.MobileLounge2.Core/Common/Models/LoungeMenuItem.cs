﻿using AIMS;
using System;
using System.Collections.Generic;
using System.Text;

namespace Ieg.Mobile.MobileLounge2.Core.Common.Models
{
    public class LoungeMenuItem : Workstation
    {
        public LoungeMenuItem()
        {
           // TargetType = typeof(Views.AirlinesPage);

#if DEBUG
            OccupanyPercentage = new Random().Next(25, 100);
#endif
        }

        public LoungeMenuItem(Workstation workstation) : this()
        {
            if (workstation == null) throw new ArgumentNullException("workstation");

            this.Id = workstation.Id;
            this.Name = workstation.Name;
            this.LoungeId = workstation.LoungeId;
            this.LoungeName = workstation.LoungeName;
            this.Occupancy = workstation.Occupancy;
            this.Descriptions = workstation.Descriptions;
            this.AimsWorkstation = workstation.AimsWorkstation;
        }

        public Type TargetType { get; set; }

        public int OccupanyPercentage { get; set; }

        public bool IsSelected { get; set; }
    }
}
