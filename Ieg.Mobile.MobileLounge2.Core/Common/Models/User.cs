﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ieg.Mobile.MobileLounge2.Core.Common.Models
{
    public class User
    {
        public string Username { get; set; }

        public string Password { get; set; }
        
        public string FullName { get; set; }
    }
}
