﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace Ieg.Mobile.MobileLounge2.Core.Common.Models
{
    //TODO: to be removed
    public class ResultPack<T>
    {
        public ResultPack()
        {
            GeneratedDate = DateTime.Now;
        }

        [DataMember]
        public DateTime GeneratedDate { get; set; }

        [DataMember]
        public bool IsSucceeded { get; set; }

        [DataMember]
        public string Message { get; set; }

        [DataMember]
        public T ReturnParam { get; set; }

        [DataMember]
        public int? ErrorCode { get; set; }

        [DataMember]
        public string ErrorMetadata { get; set; }

        [DataMember]
        public Guid? UID { get; set; }

        //[DataMember]
        //public Error Error { get; set; }

        public string RequestUrl { get; set; }
    }
}
