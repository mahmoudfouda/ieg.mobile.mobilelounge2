﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ieg.Mobile.MobileLounge2.Core.Common.Models
{
    public class Card : TileItem
    {
        public int Id { get; set; }

        public Airline Airline { get; set; }

        public string ValidationExpression { get; set; }
    }
}
