﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace Ieg.Mobile.MobileLounge2.Core.Common.Models
{
    public class Airline : TileItem
    {
        private AIMS.Models.Airline _aimsAirline;

        public Airline(AIMS.Models.Airline aimsAirline)
        {
            _aimsAirline = aimsAirline;
        }

        public decimal Id { get; set; }

        public int WorkstationId { get; set; }
        
        public string IATACode { get; set; }

        public List<Card> Cards { get; set; } = new List<Card>();

        public ImageSource Image { get; set; }

        public AIMS.Models.Airline AimsAirline
        {
            get
            {
                return _aimsAirline;
            }
        }
    }
}
