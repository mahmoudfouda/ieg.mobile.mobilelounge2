﻿using Ieg.Mobile.MobileLounge2.Core.Common.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Ieg.Mobile.MobileLounge2.Core.Common.Services
{
    public interface IWorkstationService
    {
        Task<List<Workstation>> GetAllWorkstations();

        Task<float> GetLoungeOccupancy(int LoungeId);
    }
}
