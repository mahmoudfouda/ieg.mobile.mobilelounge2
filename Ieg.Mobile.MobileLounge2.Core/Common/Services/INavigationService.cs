﻿using Ieg.Mobile.Common.Controls;
using Ieg.Mobile.MobileLounge2.Core.Views;
using Ieg.Mobile.MobileLounge2.Core.Views.ContentView;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Ieg.Mobile.MobileLounge2.Core.Common.Services
{
    public interface INavigationService
    {
        IReadOnlyList<CustomContentView> History { get; }

        event EventHandler<CustomContentView> OnPageOpenned;
        event EventHandler<CustomContentView> OnPageClosing;
        event EventHandler<IReadOnlyList<CustomContentView>> OnHistoryUpdated;

        Task Push(CustomContentView contentView);

        Task Pop();
        void InsertOnHistory(CustomContentView contentView);
        Task PopAll();

        void OpenMenu();

        void CloseMenu();

        bool IsMenuOpenned { get; }

        RootMasterDetailPage RootMasterDetailPage { get; }

        //IReadOnlyList<Page> History { get; }

        //event EventHandler<Page> OnPageOpenned;
        //event EventHandler<Page> OnPageClosing;
        //event EventHandler<IReadOnlyList<Page>> OnHistoryUpdated;

        //Task Push(Type pageType);

        //Task Pop();

        //Task PopAll();
    }
}
