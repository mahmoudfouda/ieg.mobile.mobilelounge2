﻿using Ieg.Mobile.MobileLounge2.Core.Common.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Ieg.Mobile.MobileLounge2.Core.Common.Services
{
    public interface IAirlinesService
    {
        Task<List<Airline>> GetAirlines(int workstationId, int? pageIndex = null, int? pageSize = null);

        Task<List<Card>> GetCards(int airlineId, int? pageIndex = null, int? pageSize = null);
    }
}
