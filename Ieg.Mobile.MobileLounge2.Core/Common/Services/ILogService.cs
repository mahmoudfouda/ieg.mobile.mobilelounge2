﻿using Ieg.Mobile.MobileLounge2.Core.Common.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Ieg.Mobile.MobileLounge2.Core.Common.Services
{
    public interface ILogService
    {
        Task<ResultPack<List<Log>>> GetAllLogs(int pageIndex = 0, int? pageSize = null);

        Task<ResultPack<Log>> GetLog(Guid id);

        Task<ResultPack<int>> AddLog(Log log);

        Task<ResultPack<int>> AddLog(Exception ex);

        Task<ResultPack<int>> AddLog(string title, string section, string description, string username = "");

        Task<ResultPack<int>> SendLogs();

        Task ClearLogs();
    }
}
