﻿using AIMS.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Ieg.Mobile.MobileLounge2.Core.Common.Services
{
    public interface IDataService
    {
        PassengerValidation ValidationResult { get; set; }

        bool IsGuestMode { get; set; }
    }
}
