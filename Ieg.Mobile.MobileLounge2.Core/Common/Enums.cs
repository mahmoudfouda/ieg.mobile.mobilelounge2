﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ieg.Mobile.MobileLounge2.Core.Common
{
    public enum LogTypes : int
    {
        Info = 0,
        Error = 1,
        Warning = 2,
    }


    public enum BreadCrumberTypes : int
    {
        Common = 0,
        BeforeLast = 1,
        Last = 2,
    }
}
