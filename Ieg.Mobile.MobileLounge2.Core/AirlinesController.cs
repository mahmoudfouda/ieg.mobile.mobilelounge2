﻿using Ieg.Mobile.MobileLounge2.Core.Common;
using Ieg.Mobile.MobileLounge2.Core.Common.Models;
using Ieg.Mobile.MobileLounge2.Core.Common.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ieg.Mobile.MobileLounge2.Core
{
    internal class AirlinesController : IAirlinesService
    {
        #region Singleton
        private static object loginLocker = new object();

        private static AirlinesController _current;
        public static AirlinesController Current
        {
            get
            {
                lock (loginLocker)
                {
                    if (_current == null)
                        _current = new AirlinesController();
                }
                return _current;
            }
        }

        private AirlinesController()
        {
            SeedMock();
        }
        #endregion

        #region Private Members
        private List<Airline> MockAirlines { get; set; } = new List<Airline>();
        private List<Card> MockCards { get; set; } = new List<Card>();
        private void SeedMock()
        {
            for (int i = 0; i < 35; i++)
            {
                MockAirlines.Add(new Airline(null) {
                    Id = i,
                    Name = $"Airline {i}",
                    IATACode = $"Airline {i}"
                });
            }
            var rand = new Random();
            for (int i = 0; i < 200; i++)
            {
                var airline = MockAirlines[rand.Next(0, MockAirlines.Count - 1)];

                var card = new Card
                {
                    Id = i,
                    Name = $"Card {i}",
                    Airline = airline
                };

                MockCards.Add(card);
                airline.Cards.Add(card);
            }
        }
        #endregion

        #region Airlines and Cards
        public async Task<List<Airline>> GetAirlines(int workstationId, int? pageIndex = null, int? pageSize = null)
        {
            await Task.Delay(Util.GetWaitRandomMilliseconds());
            if(pageIndex.HasValue && pageSize.HasValue)
            {
                return MockAirlines.Skip(pageIndex.Value * pageSize.Value).Take(pageSize.Value).ToList();
            }
            return MockAirlines;
        }

        public async Task<List<Card>> GetCards(int airlineId, int? pageIndex = null, int? pageSize = null)
        {
            await Task.Delay(Util.GetWaitRandomMilliseconds());

            var airline = MockAirlines.FirstOrDefault(x => x.Id == airlineId);
            if (airline == null) return new List<Card>();

            if (pageIndex.HasValue && pageSize.HasValue)
            {
                return airline.Cards.Skip(pageIndex.Value * pageSize.Value).Take(pageSize.Value).ToList();
            }
            return airline.Cards;
        }
        #endregion
    }
}
