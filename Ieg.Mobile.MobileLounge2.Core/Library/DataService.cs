﻿using AIMS.Models;
using Ieg.Mobile.MobileLounge2.Core.Common.Services;
using System;
using System.Collections.Generic;
using System.Text;

namespace Ieg.Mobile.MobileLounge2.Core.Library
{
    public class DataService : IDataService
    {
        #region Singleton Implementations
        private static object currentLocker = new object();

        private static Lazy<IDataService> _current = new Lazy<IDataService>(() => new DataService(), true);
        public static IDataService Current
        {
            get
            {
                IDataService instance = null;
                lock (currentLocker)
                {
                    instance = _current.Value;
                }
                return instance;
            }
        }
        #endregion

        private DataService() { }

        public PassengerValidation ValidationResult { get; set; }

        public bool IsGuestMode { get; set; } = false;

    }
}
