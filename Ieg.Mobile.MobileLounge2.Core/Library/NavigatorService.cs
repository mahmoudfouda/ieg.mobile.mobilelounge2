﻿using Ieg.Mobile.MobileLounge2.Core.Common.Services;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using System.Linq;
using Ieg.Mobile.MobileLounge2.Core.Views;
using Ieg.Mobile.Common.Controls;

namespace Ieg.Mobile.MobileLounge2.Core.Library
{
    public class NavigatorService : INavigationService
    {
        #region Singleton Implementations
        private static object currentLocker = new object();

        private static Lazy<INavigationService> _current = new Lazy<INavigationService>(() => new NavigatorService(), true);
        public static INavigationService Current
        {
            get
            {
                INavigationService instance = null;
                lock (currentLocker)
                {
                    instance = _current.Value;
                }
                return instance;
            }
        }
        #endregion

        private Stack<CustomContentView> _history = new Stack<CustomContentView>();

        public IReadOnlyList<CustomContentView> History
        {
            get
            {
                return _history.Reverse().ToList();
                //var reverseStack = _history.Reverse().ToList();
                //return reverseStack.Take(reverseStack.Count - 1).ToList();
            }
        }

        public bool IsMenuOpenned
        {
            get
            {
                if (RootMasterDetailPage == null)
                    return false;

                return RootMasterDetailPage.IsMenuOpen;
            }
        }

        public event EventHandler<CustomContentView> OnPageOpenned;
        public event EventHandler<CustomContentView> OnPageClosing;
        public event EventHandler<IReadOnlyList<CustomContentView>> OnHistoryUpdated;

        public async Task Pop()
        {
            if (_history != null && _history.Count > 0)
            {
                var pageToPop = _history.Reverse().LastOrDefault();
                if (pageToPop != null)
                {
                    Device.BeginInvokeOnMainThread(() =>
                    {
                        if (OnPageClosing != null)
                            OnPageClosing.Invoke(this, pageToPop);
                    });

                    var toDispose = _history.Pop();

                    if (toDispose != null)
                        toDispose.Dispose();

                    await Navigate(_history.Peek(), true);

                    Device.BeginInvokeOnMainThread(() =>
                    {
                        if (OnHistoryUpdated != null)
                            OnHistoryUpdated.Invoke(this, History);
                    });
                }
            }
        }

        public async Task PopAll()
        {
            if (_history != null)
            {
                //for (int i = 0; i < _history.Count; i++)
                for (int i = _history.Count; i > 0; i--)
                {
                    var p = _history.Peek();
                    Device.BeginInvokeOnMainThread(() =>
                    {
                        if (OnPageClosing != null && p != null)
                            OnPageClosing.Invoke(this, p);
                    });

                    var toDispose = _history.Pop();

                    if (toDispose != null)
                        toDispose.Dispose();
                }

                Device.BeginInvokeOnMainThread(() =>
                {
                    if (OnHistoryUpdated != null)
                        OnHistoryUpdated.Invoke(this, History);
                });
            }
        }

        public async Task Push(CustomContentView contentView)
        {
            if (_history.Count == 0)
                _history.Push(contentView);
            else
            {
                if (_history.Peek().KeepInHistory)
                {
                    _history.Push(contentView);
                }
                else
                {
                    var toDispose = _history.Pop();

                    if (toDispose != null)
                        toDispose.Dispose();

                    _history.Push(contentView);
                }
            }

            await Navigate(contentView);

            Device.BeginInvokeOnMainThread(() =>
            {
                if (OnPageOpenned != null)
                    OnPageOpenned.Invoke(this, contentView);
            });

            Device.BeginInvokeOnMainThread(() =>
            {
                if (OnHistoryUpdated != null)
                    OnHistoryUpdated.Invoke(this, History);
            });
        }

        private async Task Navigate(CustomContentView newPage, bool isPopping = false)
        {
            if (App.Current?.MainPage == null ||
                App.Current.MainPage.Navigation.ModalStack == null ||
                !App.Current.MainPage.Navigation.ModalStack.Any(c => c is RootMasterDetailPage))
                return;

            try
            {
                Device.BeginInvokeOnMainThread(async () =>
                {
                    var mdp = (RootMasterDetailPage)App.Current.MainPage.Navigation.ModalStack.FirstOrDefault(c => c is RootMasterDetailPage);
                    //mdp.MainPage.BindingContext = newPage.BindingContext;
                    //mdp.MainPage.Content = newPage.Content;

                    await mdp.ChangeContent(newPage, isPopping);
                });
            }
            catch
            {
                //TODO: log
            }
        }

        public void OpenMenu()
        {
            try
            {
                RootMasterDetailPage.OpenMenu();
            }
            catch 
            {
                //TODO: log
            }
        }

        public void CloseMenu()
        {
            try
            {
                RootMasterDetailPage.CloseMenu();
            }
            catch
            {
                //TODO: log
            }
        }

        public void InsertOnHistory(CustomContentView contentView)
        {
            if (this._history.Count == 0)
            {
                _history.Push(contentView);

                if (RootMasterDetailPage?.OverlayPage != null)
                    RootMasterDetailPage.OverlayPage.BindingContext = contentView.BindingContext;

                if (RootMasterDetailPage?.TitleBar != null)
                    RootMasterDetailPage.TitleBar.BindingContext = contentView.BindingContext;

                Device.BeginInvokeOnMainThread(() =>
                {
                    if (OnHistoryUpdated != null)
                        OnHistoryUpdated.Invoke(this, History);
                });
            }
        }

        public RootMasterDetailPage RootMasterDetailPage
        {
            get
            {
                try
                {
                    if (App.Current?.MainPage == null ||
                        App.Current.MainPage.Navigation.ModalStack == null ||
                        !App.Current.MainPage.Navigation.ModalStack.Any(c => c is RootMasterDetailPage))
                        return null;

                    return (RootMasterDetailPage)App.Current.MainPage.Navigation.ModalStack.FirstOrDefault(c => c is RootMasterDetailPage);
                }
                catch
                {
                    throw new ArgumentNullException("RootMasterDetailPage is null");
                }
            }
        }
    }
}
