﻿using Ieg.Mobile.MobileLounge2.Core.Common.BindableModels;
using Ieg.Mobile.MobileLounge2.Core.Views;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using System.Linq;
using AIMS;
using Ieg.Mobile.MobileLounge2.Core.Common.Services;

namespace Ieg.Mobile.MobileLounge2.Core.Library
{
    //public class NavigationService : INavigationService
    //{
    //    #region Singleton Implementations
    //    private static object currentLocker = new object();

    //    private static Lazy<INavigationService> _current = new Lazy<INavigationService>(() => new NavigationService(), true);
    //    public static INavigationService Current
    //    {
    //        get
    //        {
    //            INavigationService instance = null;
    //            lock (currentLocker)
    //            {
    //                instance = _current.Value;
    //            }
    //            return instance;
    //        }
    //    }

    //    private NavigationService()
    //    {
    //        //TODO instansiate lists


    //    }
    //    #endregion

    //    #region Events
    //    public event EventHandler<Page> OnPageOpenned;
    //    public event EventHandler<Page> OnPageClosing;
    //    public event EventHandler<IReadOnlyList<Page>> OnHistoryUpdated;
    //    #endregion

    //    private MasterDetailPage _masterDetailPage;
    //    private Stack<Page> _history = new Stack<Page>();

    //    public IReadOnlyList<Page> History
    //    {
    //        get
    //        {
    //            return _masterDetailPage.Detail.Navigation.NavigationStack.Take(_masterDetailPage.Detail.Navigation.NavigationStack.Count - 1).ToList();
    //        }
    //    }
    //    //public List<PageBindable> OpennedPages
    //    //{
    //    //    get
    //    //    {
    //    //        var _pages = new List<PageBindable>();

    //    //        if (MembershipProvider.Current != null && MembershipProvider.Current.SelectedLounge != null)
    //    //            _pages.Add(new PageBindable() { Name = MembershipProvider.Current.SelectedLounge.LoungeName });


    //    //        for (int i = 0; i < _masterDetailPage.Detail.Navigation.NavigationStack.Count-1; i++)
    //    //        {
    //    //            _pages.Add(new PageBindable() { Name = _masterDetailPage.Detail.Navigation.NavigationStack[i].Title });
    //    //        }

    //    //        //foreach (var item in _masterDetailPage.Detail.Navigation.NavigationStack)
    //    //        //{
    //    //        //    _pages.Add(new PageBindable() { Name = item.Title });
    //    //        //}

    //    //        _pages.Last().IsLastPage = true;
    //    //        return _pages;
    //    //    }
    //    //}

    //    public async Task Push(Type pageType)
    //    {
    //        await InternalNavigateToAsync(pageType);
    //    }

    //    //public async Task DetailNavigateToAsync(Type pageType)
    //    //{
    //    //    Page page = CreatePage(pageType);

    //    //    if (_masterDetailPage != null)
    //    //    {
    //    //        await _masterDetailPage.Detail.Navigation.PushAsync(page);

    //    //        if (OnNavigated != null)
    //    //            OnNavigated.Invoke(this, page);
    //    //    }
    //    //}

    //    //public async Task InitializeNavigation(Type pageType)
    //    //{
    //    //    Page page = CreatePage(pageType);

    //    //    if (_masterDetailPage != null)
    //    //    {
    //    //        await PopAll();

    //    //        _masterDetailPage.Detail = new NavigationPage(page) { BarBackgroundColor = (Color)App.Current.Resources["NavigationBarColor"] };

    //    //        Device.BeginInvokeOnMainThread(() =>
    //    //        {
    //    //            if (OnPageOpenned != null)
    //    //                OnPageOpenned.Invoke(this, page);
    //    //        });

    //    //        _history.Push(page);

    //    //        Device.BeginInvokeOnMainThread(() =>
    //    //        {
    //    //            if (OnHistoryUpdated != null)
    //    //                OnHistoryUpdated.Invoke(this, History);
    //    //        });
    //    //    }
    //    //}

    //    private async Task InternalNavigateToAsync(Type pageType)
    //    {
    //        var page = CreatePage(pageType);

    //        if (page is RootPage)
    //        {
    //            _masterDetailPage = page as MasterDetailPage;

    //            //It's beeing called before _history.Push because it is the root and we are not showing it on BreadCrumb
    //            Device.BeginInvokeOnMainThread(() =>
    //            {
    //                if (OnHistoryUpdated != null)
    //                    OnHistoryUpdated.Invoke(this, History);
    //            });

    //            _history.Push((_masterDetailPage.Detail as NavigationPage).CurrentPage);

    //            var navigationPage = Application.Current.MainPage;
    //            if (navigationPage != null)
    //            {
    //                await navigationPage.Navigation.PushModalAsync(page);
    //            }
    //        }
    //        else if (page is AirlinesPage)
    //        {
    //            await PopAll();
    //            _masterDetailPage.Detail = new NavigationPage(page) { BarBackgroundColor = (Color)App.Current.Resources["NavigationBarColor"] };
    //        }

    //        else
    //        {
    //            //var navigationPage = Application.Current.MainPage as NavigationPage;

    //            if (_masterDetailPage != null)
    //            {
    //                //await navigationPage.Navigation.PushAsync(page);

    //                await _masterDetailPage.Detail.Navigation.PushAsync(page);
    //                _history.Push(page);

    //                Device.BeginInvokeOnMainThread(() =>
    //                {
    //                    if (OnPageOpenned != null)
    //                        OnPageOpenned.Invoke(this, page);
    //                });

    //                Device.BeginInvokeOnMainThread(() =>
    //                {
    //                    if (OnHistoryUpdated != null)
    //                        OnHistoryUpdated.Invoke(this, History);
    //                });
    //            }
    //        }
    //    }

    //    public Task RemoveLastFromBackStackAsync()
    //    {
    //        var mainPage = Application.Current.MainPage as NavigationPage;

    //        if (mainPage != null)
    //        {
    //            mainPage.Navigation.RemovePage(
    //                mainPage.Navigation.NavigationStack[mainPage.Navigation.NavigationStack.Count - 2]);
    //        }

    //        return Task.FromResult(true);
    //    }

    //    public async Task Pop()
    //    {
    //        var mainPage = _masterDetailPage.Detail as NavigationPage;

    //        if (mainPage != null && mainPage.Navigation.NavigationStack.Count >= 2)
    //        {
    //            var pageToPop = mainPage.Navigation.NavigationStack[mainPage.Navigation.NavigationStack.Count - 2];
    //            if (pageToPop != null)
    //            {

    //                Device.BeginInvokeOnMainThread(() =>
    //                {
    //                    if (OnPageClosing != null)
    //                        OnPageClosing.Invoke(this, pageToPop);
    //                });

    //                _history.Pop();
    //                await mainPage.Navigation.PopAsync();

    //                Device.BeginInvokeOnMainThread(() =>
    //                {
    //                    if (OnHistoryUpdated != null)
    //                        OnHistoryUpdated.Invoke(this, History);
    //                });
    //            }
    //        }
    //    }

    //    private Page CreatePage(Type pageType)
    //    {
    //        if (pageType == null)
    //        {
    //            throw new Exception($"Cannot locate page type for {pageType}");
    //        }

    //        if (!(Activator.CreateInstance(pageType) is Page page))
    //        {
    //            throw new Exception($"Cannot create a instance of page for {pageType}");
    //        }

    //        return page;
    //    }

    //    public async Task PopAll()
    //    {
    //        var mainPage = _masterDetailPage.Detail as NavigationPage;
    //        if (mainPage != null)
    //        {
    //            for (int i = 0; i < _history.Count; i++)
    //            {
    //                var p = _history.Peek();
    //                Device.BeginInvokeOnMainThread(() =>
    //                {
    //                    if (OnPageClosing != null && p != null)
    //                        OnPageClosing.Invoke(this, p);
    //                });
    //                _history.Pop();
    //            }

    //            await mainPage.Navigation.PopToRootAsync();

    //            Device.BeginInvokeOnMainThread(() =>
    //            {
    //                if (OnHistoryUpdated != null)
    //                    OnHistoryUpdated.Invoke(this, History);
    //            });
    //        }
    //    }
    //}
}
