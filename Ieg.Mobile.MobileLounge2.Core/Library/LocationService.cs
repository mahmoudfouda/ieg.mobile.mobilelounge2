﻿using Ieg.Mobile.DataContracts.MobileLounge.ServiceModel;
using Ieg.Mobile.DataContracts.Models;
using Xamarin.Essentials;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Ieg.Mobile.MobileLounge2.Core.Library
{
    public class LocationService : IGpsAdapter
    {
        #region Singleton Implementations
        private static object currentLocker = new object();

        private static Lazy<IGpsAdapter> _current = new Lazy<IGpsAdapter>(() => new LocationService(), true);

        public static IGpsAdapter Current
        {
            get
            {
                IGpsAdapter instance = null;
                lock (currentLocker)
                {
                    instance = _current.Value;
                }
                return instance;
            }
        }
        private LocationService()
        {
            //GetLastLocationAsync();
        }
        #endregion

        #region IGpsAdapter implementations
        public event EventHandler<Position> GpsAdapter_OnLocationChanged;
        public event EventHandler<Exception> OnLog;

        public void Start()
        {
            if (_hasStarted) return;

            GetLastLocationAsync();

            _hasStarted = true;
            Xamarin.Forms.Device.StartTimer(TimeSpan.FromSeconds(REFRESH_INTERVAL_SECONDS), () => {
                GetActualLocationAsync();
                //TODO: Stop timer when its faulted (Maybe?)
                return _hasStarted;
            });
        }

        public void Stop()
        {
            _hasStarted = false;
        }
        #endregion

        #region Private Members
        private Location _lastLocation;
        private bool _isInitiated = false;
        private bool _isFaulted = false;
        private bool _hasStarted = false;

        private const int REFRESH_INTERVAL_SECONDS = 120;

        private async Task<Location> GetLastLocationAsync()
        {
            try
            {
                if (_lastLocation != null)
                    return _lastLocation;

                _lastLocation = await Geolocation.GetLastKnownLocationAsync();

                if (_lastLocation != null)
                {
                    if (_lastLocation.IsFromMockProvider)
                    {
                        _lastLocation = null;
                        throw new Exception("Mock location detected. Please turn off or disable the mock location provider application on your device.");
                    }
                    else
                    {
                        NotifyLocationChanged(_lastLocation);
                        _isInitiated = true;
                        _isFaulted = false;
                        Log($"Latitude: {_lastLocation.Latitude}, Longitude: {_lastLocation.Longitude}, Altitude: {_lastLocation.Altitude}");
                    }
                }
            }
            catch (FeatureNotSupportedException fnsEx)
            {
                _isFaulted = true;
                // Handle not supported on device exception
                Log("GPS is not supported on this device", fnsEx);
            }
            catch (FeatureNotEnabledException fneEx)
            {
                _isFaulted = true;
                // Handle not enabled on device exception
                Log("GPS is disabled on this device", fneEx);
            }
            catch (PermissionException pEx)
            {
                _isFaulted = true;
                // Handle permission exception
                Log("Permission to use GPS is not allowed yet", pEx);
            }
            catch (Exception ex)
            {
                _isFaulted = true;
                // Unable to get location
                Log("Failed to get location", ex);
            }

            return _lastLocation;
        }

        private async Task<Location> GetActualLocationAsync(System.Threading.CancellationToken cancellationToken = default)
        {
            try
            {
                var request = new GeolocationRequest(GeolocationAccuracy.Medium);
                _lastLocation = await Geolocation.GetLocationAsync(request, cancellationToken);

                if (_lastLocation != null)
                {
                    if (_lastLocation.IsFromMockProvider)
                    {
                        _lastLocation = null;
                        throw new Exception("Mock location detected. Please turn off or disable the mock location provider application on your device.");
                    }
                    else
                    {
                        NotifyLocationChanged(_lastLocation);
                        _isInitiated = true;
                        _isFaulted = false;
                        Log($"Latitude: {_lastLocation.Latitude}, Longitude: {_lastLocation.Longitude}, Altitude: {_lastLocation.Altitude}");
                    }
                }
            }
            catch (FeatureNotSupportedException fnsEx)
            {
                _isFaulted = true;
                // Handle not supported on device exception
                Log("GPS is not supported on this device", fnsEx);
            }
            catch (FeatureNotEnabledException fneEx)
            {
                _isFaulted = true;
                // Handle not enabled on device exception
                Log("GPS is disabled on this device", fneEx);
            }
            catch (PermissionException pEx)
            {
                _isFaulted = true;
                // Handle permission exception
                Log("Permission to use GPS is not allowed yet", pEx);
            }
            catch (Exception ex)
            {
                _isFaulted = true;
                // Unable to get location
                Log("Failed to get location", ex);
            }

            return _lastLocation;
        }

        private void NotifyLocationChanged(Location location)
        {
            if (GpsAdapter_OnLocationChanged != null && location != null)
            {
                GpsAdapter_OnLocationChanged.Invoke(this, new Position
                {
                    Latitude = location.Latitude,
                    Longitude = location.Longitude,
                    Accuracy = location.Accuracy.HasValue ? (float)location.Accuracy.Value : (float?)null,
                    Altitude = location.Altitude
                });
            }
        }

        private void Log(string logText, Exception innerException = null)
        {
            if (string.IsNullOrWhiteSpace(logText)) return;

            OnLog?.Invoke(this, new Exception($"##LocationService: {logText}", innerException));
        } 
        #endregion
    }
}
