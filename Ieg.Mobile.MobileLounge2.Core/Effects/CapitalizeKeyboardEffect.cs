﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace Ieg.Mobile.MobileLounge2.Core.Effects
{
    public class CapitalizeKeyboardEffect : RoutingEffect
    {
        public CapitalizeKeyboardEffect() : base(nameof(Ieg.Mobile.MobileLounge2) + "." + nameof(CapitalizeKeyboardEffect))
        {
        }
    }
}
