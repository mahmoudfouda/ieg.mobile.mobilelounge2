﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace Ieg.Mobile.MobileLounge2.Core.Effects
{
    public class EntryMoveNextEffect : RoutingEffect
    {
        public EntryMoveNextEffect() : base(nameof(Ieg.Mobile.MobileLounge2) + "." + nameof(EntryMoveNextEffect))
        {
        }
    }
}
