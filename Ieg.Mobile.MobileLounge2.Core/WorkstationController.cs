﻿using AIMS;
using Ieg.Mobile.MobileLounge2.Core.Common;
using Ieg.Mobile.MobileLounge2.Core.Common.Models;
using Ieg.Mobile.MobileLounge2.Core.Common.Services;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Linq;

namespace Ieg.Mobile.MobileLounge2.Core
{
    public class WorkstationController : IWorkstationService
    {
        #region Singleton
        private static object loginLocker = new object();

        private static WorkstationController _current;
        public static WorkstationController Current
        {
            get
            {
                lock (loginLocker)
                {
                    if (_current == null)
                        _current = new WorkstationController();
                }
                return _current;
            }
        }

        private WorkstationController()
        {
            SeedMock();
        }
        #endregion

        #region Private Members
        private List<Workstation> MockWorkstations { get; set; } = new List<Workstation>();
        private void SeedMock()
        {
            for (int i = 0; i < 5; i++)
            {
                MockWorkstations.Add(new Workstation
                {
                    Id = i,
                    Name = $"Workstation {i}",
                    LoungeId = i,
                    LoungeName = $"Lounge {i}",
                });
            }
        }
        #endregion

        public async Task<List<Workstation>> GetAllWorkstations()
        {
            await Task.Delay(Util.GetWaitRandomMilliseconds());

            MembershipProvider.Current.UserLounges.Select(x => new Workstation()
            {
                Descriptions = string.Format("{0} | {1}", x.AirportCode, x.LoungeName),
                LoungeName = x.LoungeName,
                Name = x.WorkstationName,
                Id = x.WorkstationID
            }).ToList();

            return MockWorkstations;
        }

        private Random rand = new Random();
        public async Task<float> GetLoungeOccupancy(int LoungeId)
        {
            await Task.Delay(Util.GetWaitRandomMilliseconds());

            return (float)rand.Next(0, 1000) / 10;
        }
    }
}
