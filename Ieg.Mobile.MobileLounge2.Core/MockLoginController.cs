﻿using Ieg.Mobile.MobileLounge2.Core.Common;
using Ieg.Mobile.MobileLounge2.Core.Common.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Ieg.Mobile.MobileLounge2.Core
{
    internal class MockLoginController
    {
        public event VoidEventHandler OnLoggedOut;

        public User LoggedInUser { get; set; }

        public static bool IsLoggedIn { get; private set; }

        private static object loginLocker = new object();

        private static MockLoginController _current;
        public static MockLoginController Current
        {
            get
            {
                lock (loginLocker)
                {
                    if (_current == null)
                        _current = new MockLoginController();
                }
                return _current;
            }
        }

        private MockLoginController()
        {
        }

        public async Task<ResultPack<string>> Login(string username, string password)
        {
            if (string.IsNullOrWhiteSpace(username) || string.IsNullOrEmpty(password))
                return new ResultPack<string>
                {
                    IsSucceeded = false,
                    Message = "Please enter username and/or password"
                };

            this.LoggedInUser = new User{
                FullName = "John Smith",
                Username = username
            };
            await Task.Delay(1000);
            IsLoggedIn = true;
            return new ResultPack<string>
            {
                IsSucceeded = true,
            };
        }

        public async Task<ResultPack<string>> Logout(bool forceLogout = false)
        {
            await Task.Delay(500);
            if (forceLogout)
            {
                IsLoggedIn = false;
                Xamarin.Forms.Device.BeginInvokeOnMainThread(()=> {
                    try
                    {
                        if (OnLoggedOut != null)
                            OnLoggedOut.Invoke();
                    }
                    catch
                    {
                    }
                });
                
                return new ResultPack<string>
                {
                    IsSucceeded = true,
                };
            }

            return new ResultPack<string>
            {
                IsSucceeded = false,
                Message = "Incomplete tasks left"
            };
        }
    }
}
