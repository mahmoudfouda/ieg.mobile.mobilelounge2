﻿using Ieg.Mobile.MobileLounge2.Core.Controls;
using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace Ieg.Mobile.MobileLounge2.Core.Behavior
{
    public class EmptyEntryValidatorBehavior : Behavior<CustomEntry>
    {
        CustomEntry control;
        string _placeHolder;
        Xamarin.Forms.Color _placeHolderColor;

        protected override void OnAttachedTo(CustomEntry bindable)
        {
            bindable.TextChanged += HandleTextChanged;
            bindable.Unfocused += HandleUnFocused;
            bindable.PropertyChanged += OnPropertyChanged;
            control = bindable;
            _placeHolder = bindable.Placeholder;
            _placeHolderColor = bindable.PlaceholderColor;
        }

        private void HandleUnFocused(object sender, FocusEventArgs e)
        {
            if (((CustomEntry)sender).IsRequired)
            {
                ((CustomEntry)sender).IsBorderErrorVisible = string.IsNullOrEmpty((sender as CustomEntry).Text);
                OnPropertyChanged(sender, new System.ComponentModel.PropertyChangedEventArgs(CustomEntry.IsBorderErrorVisibleProperty.PropertyName));
            }

        }

        void HandleTextChanged(object sender, TextChangedEventArgs e)
        {
            if (!string.IsNullOrEmpty(e.NewTextValue))
            {
                ((CustomEntry)sender).IsBorderErrorVisible = false;
            }
        }

        protected override void OnDetachingFrom(CustomEntry bindable)
        {
            bindable.TextChanged -= HandleTextChanged;
            bindable.Focused -= HandleUnFocused;
            bindable.PropertyChanged -= OnPropertyChanged;
        }

        void OnPropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == CustomEntry.IsBorderErrorVisibleProperty.PropertyName && control != null)
            {
                if (control.IsBorderErrorVisible)
                {
                    control.Placeholder = control.ErrorText;
                    control.PlaceholderColor = (Color)App.Current.Resources["TextColorRequired"];
                    control.Text = string.Empty;
                }
                else
                {
                    control.Placeholder = _placeHolder;
                    control.PlaceholderColor = _placeHolderColor;
                }
            }
        }
    }
}