﻿using Ieg.Mobile.MobileLounge2.Core.Common;
using Ieg.Mobile.MobileLounge2.Core.ViewModels;
using Ieg.Mobile.MobileLounge2.Core.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Ieg.Mobile.MobileLounge2.Core.Controls
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class PanelLeftControl : Grid
    {
        private bool isTapped = false;
        private SwipeGestureRecognizer _swipeLeft;

        public PanelLeftControl()
        {
            InitializeComponent();

            MessagingCenter.Subscribe<RootMasterDetailPage>(this, Constants.MenuOpened_Message, async (sender) => {
                await UpdatePositionOnfArrow();
            });

            MessagingCenter.Subscribe<RootMasterDetailPage>(this, Constants.MenuClosed_Message, async (sender) => {
                await UpdatePositionOnfArrow();
            });
        }

        private async void OnTapped(object sender, EventArgs e)
        {
            isTapped = !isTapped;

            if(this.BindingContext != null && this.BindingContext is ViewModelBase vm)
            {
                if (isTapped)
                    vm.OpenMenu();
                else
                    vm.CloseMenu();
            }

            await imgArrow.RotateTo(isTapped ? 180 : 360, 1000, Easing.CubicInOut);
        }

        protected async override void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            base.OnPropertyChanged(propertyName);
            await UpdatePositionOnfArrow();
        }

        private async Task UpdatePositionOnfArrow()
        {
            if (this.BindingContext != null && this.BindingContext is ViewModelBase vm)
            {
                if (vm.IsMenuOpenned ^ isTapped)
                {
                    isTapped = !isTapped;
                    await imgArrow.RotateTo(isTapped ? 180 : 360, 250, Easing.CubicInOut);
                }
            }
        }
    }
}