﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace Ieg.Mobile.MobileLounge2.Core.Controls
{
    public class AbsoluteLayoutGradient : AbsoluteLayout
    {
        public Color StartColor { get; set; }
        public Color EndColor { get; set; }
    }
}
