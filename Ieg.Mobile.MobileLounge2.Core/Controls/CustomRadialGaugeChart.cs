﻿using System;
using System.Threading.Tasks;
using Xamarin.Forms;
using SkiaSharp;
using SkiaSharp.Views.Forms;
using System.Diagnostics;
using Xamarin.Forms.Xaml;

namespace Ieg.Mobile.MobileLounge2.Core.Controls
{
    public class CustomRadialGaugeChart : SKCanvasView
    {
        private SKPaintSurfaceEventArgs argument;
        private ProgressUtils progressUtils = new ProgressUtils();
        private int valueAnimation = 1;

        public int Value { get; set; }


        public static BindableProperty ValueProperty = BindableProperty.Create(nameof(Value), typeof(int), typeof(CustomRadialGaugeChart), 0, BindingMode.TwoWay, null,
                                                                    (bindable, oldValue, newValue) =>
                                                                    {
                                                                        (bindable as CustomRadialGaugeChart).Value = (int)newValue;
                                                                        var angleValue = (bindable as CustomRadialGaugeChart).ConvertValueToAngle((int)newValue);
                                                                        (bindable as CustomRadialGaugeChart).AnimateProgress(angleValue);
                                                                    });


        public string TextColor { get; set; }


        public static BindableProperty TextColorProperty = BindableProperty.Create(nameof(TextColor), typeof(string), typeof(CustomRadialGaugeChart), string.Empty, BindingMode.TwoWay, null,
                                                                    (bindable, oldValue, newValue) =>
                                                                    {
                                                                        (bindable as CustomRadialGaugeChart).TextColor = (string)newValue;
                                                                    });

        public CustomRadialGaugeChart()
        {
            this.PaintSurface += CustomRadialGaugeChart_PaintSurface;
        }

        private int ConvertValueToAngle(int valueToConvert)
        {
            return (valueToConvert * 360) / 100;
        }

        void AnimateProgress(int progress)
        {
            Device.BeginInvokeOnMainThread(async () =>
            {
                valueAnimation = 1;

                // Looping at data interval of 5
                for (int i = 0; i < progress; i++)
                {
                    valueAnimation = i;
                    this.InvalidateSurface();
                    await Task.Delay(1);
                }

                valueAnimation = progress;
            });

        }

        private void CustomRadialGaugeChart_PaintSurface(object sender, SKPaintSurfaceEventArgs e)
        {
            argument = e;
            DrawGauge();
        }

        public void DrawGauge()
        {
            // Radial Gauge Constants
            int uPadding = 20;
            int side = 500;
            int radialGaugeWidth = 25;

            // Line TextSize inside Radial Gauge
            int lineSize1 = 220;

            // Line Y Coordinate inside Radial Gauge
            int lineHeight1 = 100;

            // Start & End Angle for Radial Gauge
            float startAngle = 0;
            float sweepAngle = 360;

            try
            {

                // Getting Canvas Info 
                SKImageInfo info = argument.Info;
                SKSurface surface = argument.Surface;
                SKCanvas canvas = surface.Canvas;
                progressUtils.SetDevice(info.Height, info.Width);
                canvas.Clear();

                // Getting Device Specific Screen Values
                // -------------------------------------------------

                // Top Padding for Radial Gauge
                float upperPading = progressUtils.GetFactoredHeight(uPadding);

                /* Coordinate Plotting for Radial Gauge
                *
                *    (X1,Y1) ------------
                *           |   (XC,YC)  |
                *           |      .     |
                *         Y |            |
                *           |            |
                *            ------------ (X2,Y2))
                *                  X
                *   
                *To fit a perfect Circle inside --> X==Y
                *       i.e It should be a Square
                */

                // Xc & Yc are center of the Circle
                int Xc = info.Width / 2;
                float Yc = progressUtils.GetFactoredHeight(side);

                // X1 Y1 are lefttop cordiates of rectange
                int X1 = (int)(Xc - Yc);
                int Y1 = (int)(Yc - Yc + upperPading);

                // X2 Y2 are rightbottom cordiates of rectange
                int X2 = (int)(Xc + Yc);
                int Y2 = (int)(Yc + Yc + upperPading);

                //  Empty Gauge Styling
                SKPaint paint1 = new SKPaint
                {
                    Style = SKPaintStyle.Stroke,
                    Color = Color.FromHex("#e0dfdf").ToSKColor(),                   // Colour of Radial Gauge
                    StrokeWidth = progressUtils.GetFactoredWidth(radialGaugeWidth), // Width of Radial Gauge
                    StrokeCap = SKStrokeCap.Round                                   // Round Corners for Radial Gauge
                };

                string circleColor = Value >= 90 ? "#eb5656" : Value >= 50 ? "#f2c84b" : "#6ecf97";

                // Filled Gauge Styling
                SKPaint paint2 = new SKPaint
                {
                    Style = SKPaintStyle.Stroke,
                    //Color = Color.FromHex("#05c782").ToSKColor(),                   // Overlay Colour of Radial Gauge
                    Color = Color.FromHex(circleColor).ToSKColor(),                   // Overlay Colour of Radial Gauge
                    StrokeWidth = progressUtils.GetFactoredWidth(radialGaugeWidth), // Overlay Width of Radial Gauge
                    StrokeCap = SKStrokeCap.Round                                   // Round Corners for Radial Gauge
                };

                // Defining boundaries for Gauge
                SKRect rect = new SKRect(X1, Y1, X2, Y2);


                //canvas.DrawRect(rect, paint1);
                //canvas.DrawOval(rect, paint1);

                // Rendering Empty Gauge
                SKPath path1 = new SKPath();
                path1.AddArc(rect, startAngle, sweepAngle);
                canvas.DrawPath(path1, paint1);

                // Rendering Filled Gauge
                SKPath path2 = new SKPath();
                path2.AddArc(rect, startAngle, (float)valueAnimation);
                canvas.DrawPath(path2, paint2);

                //---------------- Drawing Text Over Gauge ---------------------------

                // Achieved Minutes
                using (SKPaint skPaint = new SKPaint())
                {
                    skPaint.Style = SKPaintStyle.Fill;
                    skPaint.IsAntialias = true;
                    skPaint.Color = SKColor.Parse(TextColor); //"#ffffff"
                    skPaint.TextAlign = SKTextAlign.Center;
                    skPaint.TextSize = progressUtils.GetFactoredHeight(lineSize1);
                    skPaint.Typeface = SKTypeface.FromFamilyName(
                                        "Arial",
                                        SKFontStyleWeight.Bold,
                                        SKFontStyleWidth.Normal,
                                        SKFontStyleSlant.Upright);

                    // Drawing Achieved Minutes Over Radial Gauge
                    canvas.DrawText(Value + "%", Xc, Yc + progressUtils.GetFactoredHeight(lineHeight1), skPaint);
                }
            }
            catch 
            {
                //TODO: log
            }
        }
    }

    public class ProgressUtils
    {
        // Reference Values(Standard Pixel 1 Device)
        private const float refHeight = 1080;//1677;
        private const float refWidth = 632;//940;

        // Derived Proportinate Values
        private float deviceHeight = 1; // Initializing to 1
        private float deviceWidth = 1;  // Initializing to 1

        // Empty Constructor
        public ProgressUtils() { }

        // Setting Device Specific Values
        public void SetDevice(int deviceHeight, int deviceWidth)
        {
            this.deviceHeight = deviceHeight;
            this.deviceWidth = deviceWidth;
        }

        // Getting Device Specific Values
        public float GetFactoredValue(int value)
        {

            float refRatio = refHeight / refWidth;
            float devRatio = deviceHeight / deviceWidth;

            float factoredValue = value * (refRatio / devRatio);

            Debug.WriteLine("RR:" + refRatio + "  DR: " + devRatio + " DIV:" + (refRatio / devRatio));
            Debug.WriteLine("Calculated Value for " + value + "  : " + factoredValue);

            return factoredValue;
        }

        // Deriving Proportinate Height
        public float GetFactoredHeight(int value)
        {
            return (float)((value / refHeight) * deviceHeight);
        }

        // Deriving Proportinate Width
        public float GetFactoredWidth(int value)
        {
            return (float)((value / refWidth) * deviceWidth);
        }

        // Deriving Sweep Angle
        public int GetSweepAngle(int goal, int achieved)
        {
            int SweepAngle = 360;
            float factor = (float)achieved / goal;
            Debug.WriteLine("SWEEP ANGLE : " + (int)(SweepAngle * factor));

            return (int)(SweepAngle * factor);
        }

    }
}
