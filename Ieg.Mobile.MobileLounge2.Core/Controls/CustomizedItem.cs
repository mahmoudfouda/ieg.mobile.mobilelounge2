﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;
using Xamarin.Forms.RadialMenu.Models;

namespace Ieg.Mobile.MobileLounge2.Core.Controls
{
    public class CustomizedItem : RadialMenuItem
    {
        public override void Draw()
        {
            var itemGrid = new StackLayout() { Spacing = 0 };
            if (Source != null)
            {
                itemGrid.Children.Add(new Image() { Source = Source });
                var label = new Label() { FontSize = 10, HorizontalOptions = LayoutOptions.Center, VerticalOptions = LayoutOptions.Center, TextColor = Color.White, HorizontalTextAlignment = TextAlignment.Center, Text = Title, Margin = new Thickness(0, 1, 0, 0) };
                itemGrid.Children.Add(label);
                Content = itemGrid;
            }
        }
    }
}
