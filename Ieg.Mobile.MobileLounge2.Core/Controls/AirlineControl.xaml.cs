﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Ieg.Mobile.MobileLounge2.Core.Controls
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class AirlineControl : Grid
    {
        public AirlineControl()
        {
            InitializeComponent();
        }


        public static BindableProperty SelectedCommandProperty = BindableProperty.Create(nameof(SelectedCommand), typeof(Command), typeof(AirlineControl), null, BindingMode.TwoWay, null,
                                                                   (bindable, oldValue, newValue) => { (bindable as AirlineControl).SelectedCommand = (Command)newValue; });

        public Command SelectedCommand
        {
            get
            {
                return (Command)GetValue(SelectedCommandProperty);
            }
            set
            {
                tapGesture.Command = value;
                SetValue(SelectedCommandProperty, value);
            }
        }


        public static BindableProperty SelectedCommandParameterProperty = BindableProperty.Create(nameof(SelectedCommandParameter), typeof(object), typeof(AirlineControl), null, BindingMode.TwoWay, null,
                                                                   (bindable, oldValue, newValue) => { (bindable as AirlineControl).SelectedCommandParameter = newValue; });

        public object SelectedCommandParameter
        {
            get
            {
                return (object)GetValue(SelectedCommandParameterProperty);
            }
            set
            {
                tapGesture.CommandParameter = value;
                SetValue(SelectedCommandParameterProperty, value);
            }
        }
    }
}