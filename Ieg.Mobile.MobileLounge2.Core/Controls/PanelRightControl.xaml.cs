﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Ieg.Mobile.MobileLounge2.Core.Controls
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class PanelRightControl : Grid
    {
        private bool isTapped = false;

        public PanelRightControl()
        {
            InitializeComponent();
        }

        private async void OnTapped(object sender, EventArgs e)
        {
            isTapped = !isTapped;

            //if (this.BindingContext != null && this.BindingContext is ViewModelBase vm)
            //{
            //    if (isTapped)
            //        vm.Navigation.OpenMenu();
            //    else
            //        vm.Navigation.CloseMenu();
            //}

            await imgArrow.RotateTo(isTapped ? 180 : 360, 1000, Easing.CubicInOut);

        }
    }
}