﻿using Ieg.Mobile.MobileLounge2.Core.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Ieg.Mobile.MobileLounge2.Core.Controls
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class SearchCustomControl : Grid
    {

        public SearchCustomControl()
        {
            InitializeComponent();
        }

        private void OnSearchTapped(object sender, EventArgs e)
        {
            Animation animation = new Animation
            {
                { 0, 1, new Animation((d) => this.ColumnDefinitions[1].Width = new GridLength(d, GridUnitType.Star), 0, 1) },
                { 0, 1, new Animation((d) => this.ColumnDefinitions[0].Width = new GridLength(d, GridUnitType.Star), 1, 0) }
            };

            animation.Commit(this, "Animation", 16, 750, Easing.CubicInOut, (d, b) =>
            {
                searchBar.Focus();
                layoutBreadCrumb.IsVisible = false;
            }, null);

        }

        private void OnCancelSearch(object sender, EventArgs e)
        {
            var vm = BindingContext as ViewModelBase;

            if (vm == null)
                return;

            layoutBreadCrumb.IsVisible = true;
            Animation animation = new Animation
            {
                { 0, 1, new Animation((d) => this.ColumnDefinitions[0].Width = new GridLength(d, GridUnitType.Star), 0, 1) },
                { 0, 1, new Animation((d) => this.ColumnDefinitions[1].Width = new GridLength(d, GridUnitType.Star), 1, 0) }
            };

            animation.Commit(this, "Animation", 16, 750, Easing.CubicInOut, (d,b) =>
            {
                searchBar.Text = string.Empty;
                vm.LoadData();
            });
        }
    }
}