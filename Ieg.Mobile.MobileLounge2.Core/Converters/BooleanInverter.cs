﻿using System;
using System.Globalization;
using Xamarin.Forms;

namespace Ieg.Mobile.MobileLounge2.Core.Converters
{
    public class BooleanInverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value != null && value is bool)
                return !(bool)value;
            return false;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value != null && value is bool)
                return !(bool)value;
            return false;
        }
    }
}
