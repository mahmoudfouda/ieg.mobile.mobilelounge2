﻿using AIMS;
using Ieg.Mobile.MobileLounge2.Core.Common.Models;
using Ieg.Mobile.MobileLounge2.Core.Controls;
using Ieg.Mobile.MobileLounge2.Core.ViewModels;
using Ieg.Mobile.MobileLounge2.Core.Views.ContentView;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Ieg.Mobile.MobileLounge2.Core.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class RootPage : MasterDetailPage
    {
        public RootPage()
        {
            InitializeComponent();

            //MasterBehavior = MasterBehavior.SplitOnLandscape;
            //MasterPage.ListView.ItemSelected += ListView_ItemSelected;
            MockLoginController.Current.OnLoggedOut += ApplicationLoggedOut;

            (MasterPage.BindingContext as LoungesViewModel).PropertyChanged += RootPage_PropertyChanged;
        }

        private async void RootPage_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "SelectedLounge" && (MasterPage.BindingContext as LoungesViewModel).SelectedLounge != null)
            {
                var vm = (MasterPage.BindingContext as LoungesViewModel);

                //var page = (Page)Activator.CreateInstance(vm.SelectedLounge.LoungeMenuItem.TargetType);
                //page.Title = vm.SelectedLounge.Name;
                //Detail = new NavigationPage(page) { BarBackgroundColor = (Color)App.Current.Resources["NavigationBarColor"] };

                //await vm.NavigationService.DetailNavigateToAsync(vm.SelectedLounge.LoungeMenuItem.TargetType);

                vm.Pages.Clear();
                await vm.Navigation.Push(new AirlineContentView());

                IsPresented = false;

                MasterPage.ListView.SelectedItem = null;
            };
        }


        private void ApplicationLoggedOut()
        {
            this.Navigation.PopModalAsync(true);
        }

    }
}