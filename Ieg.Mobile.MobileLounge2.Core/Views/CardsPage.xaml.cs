﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Ieg.Mobile.MobileLounge2.Core.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class CardsPage : ContentPage
	{
		public CardsPage ()
		{
			InitializeComponent ();

            this.Title = "Cards";

            NavigationPage.SetHasBackButton(this, false);

            mnMenu.ChildShrinkEasing = Easing.CubicInOut;
            mnMenu.ChildGrowEasing = Easing.CubicInOut;
            mnMenu.MenuOpenEasing = Easing.CubicInOut;
            mnMenu.MenuCloseEasing = Easing.CubicOut;
        }
	}
}