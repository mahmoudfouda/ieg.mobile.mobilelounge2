﻿using Ieg.Mobile.MobileLounge2.Core.Controls;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Ieg.Mobile.MobileLounge2.Core.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class AirlinesPage : ContentPage
    {
        private Animation _animation;

        public AirlinesPage()
        {
            InitializeComponent();

            NavigationPage.SetHasBackButton(this, false);

            mnMenu.ChildShrinkEasing = Easing.CubicInOut;
            mnMenu.ChildGrowEasing = Easing.CubicInOut;
            mnMenu.MenuOpenEasing = Easing.CubicInOut;
            mnMenu.MenuCloseEasing = Easing.CubicOut;
        }
    }
}