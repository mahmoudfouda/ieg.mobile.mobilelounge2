﻿using Ieg.Mobile.Common.Controls;
using Ieg.Mobile.MobileLounge2.Core.Common;
using Ieg.Mobile.MobileLounge2.Core.Library;
using Ieg.Mobile.MobileLounge2.Core.ViewModels;
using Ieg.Mobile.MobileLounge2.Core.Views.ContentView;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.RadialMenu.Enumerations;
using Xamarin.Forms.RadialMenu.Models;
using Xamarin.Forms.Xaml;

namespace Ieg.Mobile.MobileLounge2.Core.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class RootMasterDetailPage : CustomMasterPageDetail
    {
        private RootMasterDetailViewModel _vm;

        public RootMasterDetailPage()
        {
            InitializeComponent();

            NavigationPage.SetHasBackButton(this, false);

            MenuBehavior = MasterBehavior.Split;

            MenuOpened += OnMenuOpened;
            MenuOpening += OnMenuOpening;
            MenuClosed += OnMenuClosed;
            MenuClosing += OnMenuClosing;

            MockLoginController.Current.OnLoggedOut += ApplicationLoggedOut;

            _vm = (BindingContext as RootMasterDetailViewModel);

            LoadItemsMenu(); 
        }
   

        private void OnMenuClosed(object sender, EventArgs e)
        {
            MessagingCenter.Send<RootMasterDetailPage>(this, Constants.MenuClosed_Message);
        }

        private bool OnMenuOpening()
        {
            MessagingCenter.Send<RootMasterDetailPage>(this, Constants.MenuOpening_Message);
            return true;
        }

        private bool OnMenuClosing()
        {
            MessagingCenter.Send<RootMasterDetailPage>(this, Constants.MenuClosing_Message);
            return true;
        }

        private void OnMenuOpened(object sender, EventArgs e)
        {
            MessagingCenter.Send<RootMasterDetailPage>(this, Constants.MenuOpened_Message);
        }

        private async void ApplicationLoggedOut()
        {
            await (this.BindingContext as RootMasterDetailViewModel).Navigation.PopAll();
            await this.Navigation.PopModalAsync(true);
        }

        private void LoadItemsMenu()
        {
            _vm.MenuItems = new ObservableCollection<RadialMenuItem>()
                {
                    new RadialMenuItem()
                    {
                        Source = "radial_btn__home.png",
                        WidthRequest = 31,
                        HeightRequest = 31,
                        VerticalOptions = LayoutOptions.Center,
                        HorizontalOptions = LayoutOptions.Center,
                        Location = Enumerations.RadialMenuLocation.N,
                    }
                };

            _vm.MenuItems.Add(new RadialMenuItem()
            {
                Source = "radial_btn_message.png",
                WidthRequest = 31,
                HeightRequest = 31,
                VerticalOptions = LayoutOptions.Center,
                HorizontalOptions = LayoutOptions.Center,
                Location = Enumerations.RadialMenuLocation.Ne,
            });

            _vm.MenuItems.Add(new RadialMenuItem()
            {
                Source = "radial_btn_lasthour.png",
                WidthRequest = 31,
                HeightRequest = 31,
                VerticalOptions = LayoutOptions.Center,
                HorizontalOptions = LayoutOptions.Center,
                Location = Enumerations.RadialMenuLocation.E
            });

            _vm.MenuItems.Add(new RadialMenuItem()
            {
                Source = "radial_btn_logs.png",
                WidthRequest = 31,
                HeightRequest = 31,
                VerticalOptions = LayoutOptions.Center,
                HorizontalOptions = LayoutOptions.Center,
                Location = Enumerations.RadialMenuLocation.Se,
            });

            _vm.MenuItems.Add(new RadialMenuItem()
            {
                Source = "radial_btn_statistics.png",
                WidthRequest = 31,
                HeightRequest = 31,
                VerticalOptions = LayoutOptions.Center,
                HorizontalOptions = LayoutOptions.Center,
                Location = Enumerations.RadialMenuLocation.S
            });

            _vm.MenuItems.Add(new RadialMenuItem()
            {
                Source = "radial_btn_settings.png",
                WidthRequest = 31,
                HeightRequest = 31,
                VerticalOptions = LayoutOptions.Center,
                HorizontalOptions = LayoutOptions.Center,
                Location = Enumerations.RadialMenuLocation.Sw
            });
        }

        private async void OnCallMenu(object sender, Xamarin.Forms.RadialMenu.Enumerations.Enumerations.RadialMenuLocation e)
        {
            switch (e)
            {
                case Enumerations.RadialMenuLocation.N:
                    await NavigatorService.Current.Push(new ProfileContentView());
                    break;
                case Enumerations.RadialMenuLocation.Ne:
                    await NavigatorService.Current.Push(new MessagesContentView());
                    break;
                case Enumerations.RadialMenuLocation.Nw:
                    break;
                case Enumerations.RadialMenuLocation.S:
                    break;
                case Enumerations.RadialMenuLocation.Se:
                    await NavigatorService.Current.Push(new LogsContentView());
                    break;
                case Enumerations.RadialMenuLocation.Sw:
                    break;
                case Enumerations.RadialMenuLocation.W:
                    break;
                case Enumerations.RadialMenuLocation.E:
                    await NavigatorService.Current.Push(new LastHourContentView());
                    break;
                default:
                    break;
            }

        }
    }
}