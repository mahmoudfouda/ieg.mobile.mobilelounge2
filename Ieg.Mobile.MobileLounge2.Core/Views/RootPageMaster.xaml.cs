﻿using Ieg.Mobile.MobileLounge2.Core.Controls;
using Ieg.Mobile.MobileLounge2.Core.ViewModels;
using Microcharts;
using Microcharts.Forms;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Ieg.Mobile.MobileLounge2.Core.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class RootPageMaster : ContentPage
    {
        public ListView ListView => MenuItemsListView;

        public RootPageMaster()
        {
            InitializeComponent();
        }
    }
}