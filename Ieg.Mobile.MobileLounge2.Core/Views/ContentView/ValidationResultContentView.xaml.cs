﻿using Ieg.Mobile.Common.Controls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Ieg.Mobile.MobileLounge2.Core.Views.ContentView
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ValidationResultContentView : CustomContentView
    {
        public ValidationResultContentView()
        {
            InitializeComponent();

            KeepInHistory = false;
        }

        private async void OnShowServices(object sender, EventArgs e)
        {
            if (scrollView.ScrollY > 0)
                await scrollView.ScrollToAsync(0, 0, true);


            await gridServices.TranslateTo(-gridServices.Width, 0, 0);
            await Task.WhenAll(
                gridServices.FadeTo(1, 500, Easing.Linear),
                gridServices.TranslateTo(0, gridServices.Y, 500, Easing.CubicInOut));
        }
    }
}