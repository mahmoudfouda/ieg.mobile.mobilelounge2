﻿using Ieg.Mobile.Common.Controls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Ieg.Mobile.MobileLounge2.Core.Views.ContentView
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class LogsContentView : CustomContentView
    {
        public LogsContentView()
        {
            InitializeComponent();
            KeepInHistory = false;
        }
    }
}