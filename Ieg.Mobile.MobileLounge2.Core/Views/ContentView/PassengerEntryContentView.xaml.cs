﻿using Ieg.Mobile.Common.Controls;
using Ieg.Mobile.MobileLounge2.Core.Common.BindableModels;
using Ieg.Mobile.MobileLounge2.Core.Controls;
using Ieg.Mobile.MobileLounge2.Core.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Ieg.Mobile.MobileLounge2.Core.Views.ContentView
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class PassengerEntryContentView : CustomContentView
    {
        private PassengerEntryViewModel _vm;
        private CustomEntry _txtEditingEntry;

        public PassengerEntryContentView()
        {
            InitializeComponent();
            KeepInHistory = false;

            lstFrom.ItemSelected += LstFrom_ItemSelected;
            lstFrom.ItemDisappearing += LstFrom_ItemDisappearing;
            
            txtFrom.TextChanged += TxtAirportTextChange;
            txtTo.TextChanged += TxtAirportTextChange;

            txtFrom.Focused += TxtFrom_Focused;
            txtTo.Focused += TxtTo_Focused;

            txtFrom.Completed += TxtFrom_Completed;
            txtTo.Completed += TxtFrom_Completed;

            txtFlightCarrier.NextEntry = txtFlightNumber;
            txtFlightNumber.NextEntry = txtTrackingClassOfService;
            txtTrackingClassOfService.NextEntry = txtPNR;
            txtPNR.NextEntry = txtFullName;
            txtFullName.NextEntry = txtFrom;
            txtFrom.NextEntry = txtTo;
            txtTo.NextEntry = txtLoyalty;
            txtLoyalty.NextEntry = txtSeat;
            txtSeat.NextEntry = txtNotes;

            txtFlightNumber.Focus();

            //var touchToClose = new TapGestureRecognizer();
            //touchToClose.Tapped += OnAutoCompleteClose;
            //touchToClose.NumberOfTapsRequired = 1;
            //formOverlay.GestureRecognizers.Add(touchToClose);

            _vm = this.BindingContext as PassengerEntryViewModel;
        }

        private void TxtFrom_Completed(object sender, EventArgs e)
        {
            CustomEntry txt = sender as CustomEntry;

            if (txt != null)
            {
                OnAutoCompleteClose(sender, e);
                _vm.LoadFullAirportName(txt.Text, txt == txtFrom);
            }
        }

        private async void TxtFrom_Focused(object sender, FocusEventArgs e)
        {
            await scrollView.ScrollToAsync(0, 100, true);
        }

        private void LstFrom_ItemDisappearing(object sender, ItemVisibilityEventArgs e)
        {
            if (Device.RuntimePlatform == Device.Android)
            {
                if (_txtEditingEntry != null && _txtEditingEntry.IsFocused)
                {
                    _txtEditingEntry.Unfocus();
                }
            }
        }

        private async void TxtTo_Focused(object sender, FocusEventArgs e)
        {
            if (Device.RuntimePlatform != Device.UWP)
            {
                var location = GetLocation(txtFrom);
                await scrollView.ScrollToAsync(0, location.Y - txtTo.Height - lblFromAirport.Height, true);
            }
        }

        private void OnAutoCompleteClose(object sender, EventArgs e)
        {
            var anim = new Animation((d) => myFloater.Opacity = d, 1, 0, Easing.CubicInOut, null);

            anim.Commit(myFloater, "AutoCompleteClosingAnim", 16, 350, Easing.CubicInOut, (d, b) =>
            {
                absHolder.IsVisible = false;
            });
        }

        private void TxtAirportTextChange(object sender, TextChangedEventArgs e)
        {
            if (sender is CustomEntry txtBox)
            {
                _txtEditingEntry = txtBox;

                var textBoxLocation = GetLocation(txtBox);
                var formLocation = GetLocation(absHolder);

                if (string.IsNullOrWhiteSpace(txtBox.Text) || txtBox.Text.Length <= 1)
                {
                    myFloater.Opacity = 0;
                    absHolder.IsVisible = false;
                    return;
                }

                InitSearch(txtBox.Text, () =>
                {
                    Device.BeginInvokeOnMainThread(() =>
                    {
                        if (myFloater.Opacity == 0 || !absHolder.IsVisible)
                        {
                            _vm.IsListVisible = false;
                            absHolder.IsVisible = true;
                            myFloater.Opacity = 0;

                            AbsoluteLayout.SetLayoutFlags(myFloater, AbsoluteLayoutFlags.SizeProportional);
                            //AbsoluteLayout.SetLayoutBounds(myFloater, new Rectangle(textBoxLocation.X - formLocation.X, txtBox.Height + textBoxLocation.Y - formLocation.Y, 0.85, 0.75));

                            var locationY = txtBox.Bounds.Bottom;

                            if (Device.RuntimePlatform != Device.UWP)
                            {
                                if (txtBox == txtTo)
                                    locationY = lblFromAirport.Bounds.Bottom + txtBox.Height + 10; //txtFrom.Bounds.Bottom - lblFromAirport.Height;
                                if (txtBox == txtFrom)
                                    locationY = lblFromAirport.Bounds.Top + txtBox.Height + 10;  //txtBox.Bounds.Bottom + txtBox.Height - 10;
                            }
                            else
                                locationY += txtBox.Height + formLocation.Y + 80;

                            AbsoluteLayout.SetLayoutBounds(myFloater, new Rectangle(textBoxLocation.X - formLocation.X, locationY, 0.43, 0.35));

                            var anim = new Animation((d) => myFloater.Opacity = d, 0, 1, Easing.CubicInOut, null);
                            anim.Commit(myFloater, "AutoCompleteOpeningAnim", 16, 600, Easing.CubicInOut, null);
                        }
                    });
                });
            }
        }

        private Point GetLocation(VisualElement element)
        {
            if (element == null) return Point.Zero;

            var y = element.Y;
            var x = element.X;
            var parent = (VisualElement)element.Parent;
            while (parent != null)
            {
                y += parent.Y;
                x += parent.X;
                if (parent.Parent is VisualElement)
                    parent = (VisualElement)parent.Parent;
                else break;
            }

            return new Point(x, y);
        }

        private void InitSearch(string keyword, Action callback)
        {
            _vm.SearchAirports(keyword);
            callback?.Invoke();
        }

        private void LstFrom_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            if (e.SelectedItem != null && sender is ListView lst)
            {
                if (_txtEditingEntry == txtFrom)
                    _vm.SelectedPassenger.SelectedAirportFrom = lst.SelectedItem as AirportBindable;
                else if (_txtEditingEntry == txtTo)
                    _vm.SelectedPassenger.SelectedAirportTo = lst.SelectedItem as AirportBindable;

                _txtEditingEntry.TextChanged -= TxtAirportTextChange;
                _txtEditingEntry.Text = (lst.SelectedItem as AirportBindable).IATA_Code;
                _txtEditingEntry.TextChanged += TxtAirportTextChange;

                myFloater.Opacity = 0;
                absHolder.IsVisible = false;

                (sender as ListView).SelectedItem = null;
            }
        }
    }
}