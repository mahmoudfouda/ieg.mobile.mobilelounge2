﻿using Ieg.Mobile.Common.Controls;
using Ieg.Mobile.MobileLounge2.Core.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Ieg.Mobile.MobileLounge2.Core.Views.ContentView
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class GenericOverlayPageContentView : CustomContentView
    {
        ViewModelBase _vm;
        private TapGestureRecognizer _tappedOverlayPage;

        public GenericOverlayPageContentView()
        {
            InitializeComponent();

            _tappedOverlayPage = new TapGestureRecognizer();
            _tappedOverlayPage.Tapped += OnTapOverlyPage;
            _tappedOverlayPage.NumberOfTapsRequired = 1;

            this.GestureRecognizers.Add(_tappedOverlayPage);
        }

        private void OnTapOverlyPage(object sender, EventArgs e)
        {
            this.Focus();
            loader.Hide();
            (this.BindingContext as ViewModelBase).HideAllPopup();
        }

        protected override void OnBindingContextChanged()
        {
            base.OnBindingContextChanged();

            if (this.BindingContext != null)
            {
                _vm = (this.BindingContext as ViewModelBase);
                _vm.OnStartProcess -= OnStartProcess;
                _vm.OnStartProcess += OnStartProcess;

                _vm.OnEndProcess -= OnEndProcess;
                _vm.OnEndProcess += OnEndProcess;
            }
        }

        private void OnEndProcess()
        {
            loader.Hide();
            _vm.HideAllPopup();
        }

        private void OnStartProcess(Action action)
        {
            loader.Show();
            Task.Delay(1000).ContinueWith((t) =>
            {
                if (action != null)
                    action.Invoke();

            });
        }
    }
}