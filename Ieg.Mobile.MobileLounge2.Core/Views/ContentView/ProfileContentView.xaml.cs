﻿using AIMS;
using Ieg.Mobile.Common.Controls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.GoogleMaps;
using Xamarin.Forms.Xaml;

namespace Ieg.Mobile.MobileLounge2.Core.Views.ContentView
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ProfileContentView : CustomContentView
    {
        public ProfileContentView()
        {
            InitializeComponent();
            KeepInHistory = false;

            double lat = DefaultMobileApplication.Current.Position.Latitude;
            double lon = DefaultMobileApplication.Current.Position.Latitude;

#if DEBUG
            lat = 45.511021;
            lon = -73.668443;

            map.MoveToRegion(MapSpan.FromCenterAndRadius(new Position(lat, lon), Distance.FromMeters(100)).WithZoom(0.5));
#endif

            map.Pins.Add(new Pin() { Label = "My Position", Position = new Position(lat, lon) });

            var positions = new System.Collections.Generic.List<AIMS.Models.ReadOnlyPosition>();
            #region Filling up positions with white fences
            var fenceList = DefaultMobileApplication.Current.Fences;
            if (fenceList != null)
            {
                foreach (var fence in fenceList)
                {
                    if (fence.Positions != null)
                        positions.AddRange(fence.Positions);
                }
            }

            foreach (var position in positions)
            {
                //map.Circle.Add(new Controls.CustomCircleMap() { Position = new Position(position.Latitude, position.Longitude), Radius = position.Radius });

                map.Circles.Add(new Circle()
                {
                    Center = new Position(position.Latitude, position.Longitude),
                    Radius = Distance.FromMeters(100),
                    FillColor = Color.FromHex("#660099ee"),
                    StrokeColor = Color.FromHex("#FFFFFFFF"),
                    StrokeWidth = 0.5f
                }); 
            }
            #endregion
        }

        private void OnMapTypeClicked(object sender, EventArgs e)
        {
            var b = sender as Button;
            switch (b.Text)
            {
                case "Street":
                    map.MapType = MapType.Street;
                    break;
                case "Hybrid":
                    map.MapType = MapType.Hybrid;
                    break;
                case "Satellite":
                    map.MapType = MapType.Satellite;
                    break;
            }
        }

        void AddMapStyle()
        {
            var assembly = typeof(LoginPage).GetTypeInfo().Assembly;

            var stream = assembly.GetManifestResourceStream($"Ieg.Mobile.MobileLounge2.Core.Controls.DarkMapStyle.json");

            string styleFile;

            using (var reader = new System.IO.StreamReader(stream))
            {
                styleFile = reader.ReadToEnd();
            }

            map.MapStyle = MapStyle.FromJson(styleFile);
        }
    }
}