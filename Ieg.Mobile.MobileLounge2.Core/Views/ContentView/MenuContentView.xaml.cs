﻿using Ieg.Mobile.Common.Controls;
using Ieg.Mobile.MobileLounge2.Core.Common;
using Ieg.Mobile.MobileLounge2.Core.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Ieg.Mobile.MobileLounge2.Core.Views.ContentView
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MenuContentView : CustomContentView
    {
        private LoungesViewModel _vm;
        private SwipeGestureRecognizer _swipeLeft;

        public MenuContentView()
        {
            InitializeComponent();

            DoAnimation(0);

            MessagingCenter.Subscribe<RootMasterDetailPage>(this, Constants.MenuOpened_Message, (sender) =>
            {
                DoAnimation(1);
            });

            MessagingCenter.Subscribe<RootMasterDetailPage>(this, Constants.MenuClosing_Message, (sender) =>
            {
                DoAnimation(0);
            });

            _vm = this.BindingContext as LoungesViewModel;

            _vm.PropertyChanged += MenuContentView_PropertyChanged;


            
            //var _swipeLeft = new SwipeGestureRecognizer();
            //_swipeLeft.Direction = SwipeDirection.Left;
            //_swipeLeft.Swiped += OnSwipedLeft;

            //menuRoot.GestureRecognizers.Add(_swipeLeft);
        }

        //private void ListView_ItemAppearing(object sender, ItemVisibilityEventArgs e)
        //{
        //    if (menuRoot.GestureRecognizers != null && menuRoot.GestureRecognizers.Count > 0 && e.Item is View itemView)
        //    {
        //        if (itemView.GestureRecognizers != null && itemView.GestureRecognizers.Count == 0)
        //        {

        //        }

        //    }
        //}

        //private void ListView_ItemDisappearing(object sender, ItemVisibilityEventArgs e)
        //{
        //    if(e.Item is View itemView)
        //    {
        //        if (itemView.GestureRecognizers != null && itemView.GestureRecognizers.Count > 0)
        //        {
        //            itemView.GestureRecognizers.Clear();
        //        }
        //    }
        //}

        //private void ListView_ChildAdded(object sender, ElementEventArgs e)
        //{
        //    if (menuRoot.GestureRecognizers != null && menuRoot.GestureRecognizers.Count > 0 && e.Element is View itemView)
        //    {
        //        if(itemView.GestureRecognizers != null && itemView.GestureRecognizers.Count == 0)
        //        {

        //        }
                
        //    }
        //}

        //private void ListView_ChildRemoved(object sender, ElementEventArgs e)
        //{
        //    if(e.Element is View itemView)
        //    {
        //        if (itemView.GestureRecognizers != null && itemView.GestureRecognizers.Count > 0)
        //        {
        //            itemView.GestureRecognizers.Clear();
        //        }
        //    }
        //}

        //private void OnSwipedLeft(object sender, SwipedEventArgs e)
        //{

        //}

        private void DoAnimation(double opacity)
        {
            menuRoot.FadeTo(opacity, 100, Easing.Linear);
        }

        private void MenuContentView_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "SelectedLounge" && (BindingContext as LoungesViewModel).SelectedLounge != null)
            {
                //MenuItemsListView.SelectedItem = null;

                Task.Run(async () =>
                {
                    var airLineContent = new AirlineContentView();
                    var avm = airLineContent.BindingContext as AirLinesViewModel;
                    await avm.Navigation.PopAll();
                    await avm.Navigation.Push(airLineContent);
                });
            }
            //else
            //{
            //    foreach (var item in MenuItemsListView.ItemsSource)
            //    {
            //        Cell cell = item as Cell;
            //    }
            //}
        }

        //private void SwipeGestureRecognizer_Swiped(object sender, SwipedEventArgs e)
        //{

        //}
    }
}