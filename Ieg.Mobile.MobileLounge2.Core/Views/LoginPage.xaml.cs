﻿using System;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Ieg.Mobile.DataContracts.MobileLounge.ServiceModel;
using System.Threading.Tasks;
using Ieg.Mobile.MobileLounge2.Core.ViewModels;
using Ieg.Mobile.MobileLounge2.Core.Views.ContentView;
using Ieg.Mobile.MobileLounge2.Core.Library;

namespace Ieg.Mobile.MobileLounge2.Core.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class LoginPage : ContentPage
    {
        public LoginViewModel ViewModel { get { return this.BindingContext as ViewModels.LoginViewModel; } }

        public LoginPage()
        {
            InitializeComponent();

            if (ViewModel != null)
            {
                ViewModel.PropertyChanged += Vm_PropertyChanged;
                ViewModel.OnLoggedIn += OnLoggedIn;
                if (ViewModel.IsLoading)
                    loader.Show();
            }

            this.txtUName.Completed += TexboxCompletedEditing;
            this.txtPass.Completed += TexboxCompletedEditing;

            NavigationPage.SetHasNavigationBar(this, false);
        }

        private void Vm_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if(!string.IsNullOrWhiteSpace(e.PropertyName))
            {
                if (e.PropertyName.Equals("IsLoading") && loader != null)
                {
                    if (ViewModel.IsLoading)
                        loader.Show();
                    else loader.Hide();
                }
            }
        }

        private void TexboxCompletedEditing(object sender, EventArgs e)
        {
            if (sender is Entry txt)
            {
                if (txt.Placeholder == "Username")
                    txtPass.Focus();
                else if (txt.Placeholder == "Password")
                    btnLogin.Focus();
            }
        }

        protected override void OnAppearing()
        {
            //if (this.BindingContext != null)
            //{
            //    if (this.BindingContext is ViewModels.LoginViewModel vm)
            //    {
            //        vm.OnLoggedIn -= OnLoggedIn;
            //        vm.OnLoggedIn += OnLoggedIn;

            //        //vm.OnLoggedFail -= OnLoggedFail;
            //        //vm.OnLoggedFail += OnLoggedFail;

            //        vm.Init();
            //    }
            //}

            if(ViewModel != null)
            {
                ViewModel.Init();
            }

            //System.Threading.Tasks.Task.Run(() => { 
            //    Device.BeginInvokeOnMainThread(async () =>
            //    {
            //        await System.Threading.Tasks.Task.Delay(500);
            //        txtUName.Focus();
            //    });
            //});

            //txtUName.Focus();
        }

        //private void OnLoggedFail()
        //{
        //    this.loader.Hide();
        //}

        private async void OnLoggedIn()
        {
            //Navigation.PushModalAsync(new RootPage());

            //this.loader.Hide();

            //App.Current.MainPage = new RootMasterDetailPage();

            var root = new RootMasterDetailPage();

            await App.Current.MainPage.Navigation.PushModalAsync(root);

            NavigatorService.Current.InsertOnHistory(root.MainPage);

            //await NavigatorService.Current.Push(new AirlineContentView());

            //await Navigation.PushAsync(new RootMasterDetailPage());

            //await (this.BindingContext as ViewModels.LoginViewModel).Navigation.Push(typeof(RootPage));


            //await Task.Delay(50).ContinueWith((t) =>
            //{
            //    Device.BeginInvokeOnMainThread(async () =>
            //    {
            //        await (this.BindingContext as LoginViewModel).Navigation.Push(new AirlineContentView());
            //    });
            //});

        }

        //private void OnStartLogin(object sender, EventArgs e)
        //{
        //    //loader.Show();
        //    Task.Delay(1000).ContinueWith((t) =>
        //    {
        //        (BindingContext as LoginViewModel).LoginCommand.Execute(null);
        //        //App.Current.MainPage = new MainPage();

        //        if (this.BindingContext is ViewModels.LoginViewModel vm)
        //            vm.Message = string.Empty;

        //    });
        //}
    }
}